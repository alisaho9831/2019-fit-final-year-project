--
-- Dumping data for table `productimages`
--

LOCK TABLES productimages WRITE;

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('3.jpg',' ',3);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('6.jpg',' ',6);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('7.jpg',' ',7);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('8.jpg',' ',8);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('9.jpg',' ',9);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('10.jpg',' ',10);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('11.jpg',' ',11);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('12.jpg',' ',12);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('33.jpg',' ',33);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('34.jpg',' ',34);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('35.jpg',' ',35);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('36.jpg',' ',36);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('38.jpg',' ',38);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('39.jpg',' ',39);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('41.jpg',' ',41);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('42.jpg',' ',42);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('43.jpg',' ',43);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('44.jpg',' ',44);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('45.jpg',' ',45);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('46.jpg',' ',46);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('47.jpg',' ',47);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('48.jpg',' ',48);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('49.jpg',' ',49);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('50.jpg',' ',50);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('54.jpg',' ',54);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('55.jpg',' ',55);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('56.jpg',' ',56);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('57.jpg',' ',57);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('58.jpg',' ',58);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('59.jpg',' ',59);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('60.jpg',' ',60);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('2 x Glad Wrap Refill 50m (EasyCut Dispenser).jpg',' ',5);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('Baseball Cap Black.jpg',' ',51);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('Baseball Cap Red.jpg',' ',52);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('Golden State Cap.jpg',' ',53);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('toyplush.jpg',' ',4);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('POY-Product-Shots-2014-45.jpg',' ',3);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('gilletterazor.jpg',' ',1);

INSERT INTO productimages (image,image_dir,product_id)
VALUES ('dovepers.jpg',' ',2);

UNLOCK TABLES;