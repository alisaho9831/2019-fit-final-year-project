--
-- Dumping data for table `items`
--
LOCK TABLES items WRITE;

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (4000539130007,1,'Gender','Men',20,100,12,24,5.00,6.00,200,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (5746000958362,2,'Gender','Men',30,200,12,24,3.00,4.00,234,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (1700094850004,3,'Gender','Women',120,100,12,24,6.00,7.00,500,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (3000394000392,4,'Gender','Women',220,1000,12,24,6.00,8.00,300,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (7283900049582,5,'Colour','Jungle red',200,1300,12,24,8.00,7.00,400,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (4000293400021,6,'Colour','Black',200,0,1,12,0,39.00,400,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9000293800002,7,'Colour','Smoking Charcoal',320,560,24,48,8.00,10.00,350,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (3000984320001,8,'Gender','Women',260,800,1,24,7.00,9.00,250,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8000029380002,9,'Gender','Men',560,800,2,24,8.00,10.00,230,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8000987000231,10,'Gender','Women',820,1000,1,48,7.00,9.00,230,1,1,1,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (5098090230021,11,'Type','Electric',420,0,1,24,0,13.00,250,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2009384002931,12,'Colour','Black',270,0,1,36,0,12.00,260,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (7490003980002,33,'Gender','Women',330,0,12,24,0,140.00,230,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (3800039820002,34,'Gender','Men',250,0,12,24,0,54.00,240,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2837090498329,35,'Colour','Black',320,0,12,24,0,334.00,400,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2910003948392,36,'Gender','Women',220,0,12,24,0,435.00,450,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9280003948029,38,'Character','Winnie The Pool',120,0,12,24,0,34.00,220,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2830002949802,39,'Character','Dumbo',520,0,12,24,0,34.00,540,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9400029384901,41,'Colour','Optic White',230,0,12,24,0,64.00,220,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9002930004982,42,'Colour','Navy',520,0,12,24,0,64.00,260,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9409900029835,43,'Colour','Black/White',230,0,12,24,0,84.00,640,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8490002900098,44,'Colour','Mystery Ruby/Cloud White',220,0,12,24,0,84.00,340,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2830004983023,45,'Colour','Core Black/White/Bright Cyan',120,0,12,24,0,74.00,230,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2830003984099,46,'Colour','Green',240,0,12,24,0,8.00,540,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8200093849023,47,'Colour','Green',620,0,12,24,0,6.00,260,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (3984092200002,48,'Colour','Navy',210,0,12,24,0,8.00,523,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8009870003403,49,'Colour','Black',240,0,12,24,0,24.00,543,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8900002938005,50,'Colour','Gravel',260,0,12,24,0,24.00,650,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9200009380004,51,'Colour','Black',320,0,12,24,0,36.00,430,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (7000983099002,52,'Colour','Red',260,0,12,24,0,36.00,235,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8000349380004,53,'Colour','Blue',720,0,12,24,0,30.00,120,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (6000984000986,54,'Colour','Black',320,0,12,24,0,140.00,500,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8000984572905,55,'Colour','Blue',260,0,12,24,0,80.00,260,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8000985898302,56,'Age','0-3',230,0,12,24,0,18.00,640,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (3009849002984,57,'Character','Batmen',270,0,12,24,0,9.00,150,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2980009849821,58,'Character','Star War',120,0,12,24,0,25.00,360,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (8700983749209,59,'Age','3+',240,0,12,24,0,20.00,360,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (7509994800093,60,'Age','3+',270,0,12,24,0,18.00,120,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2309849572839,61,'Colour','Titan Grey/Tomato',320,0,12,24,0,80.00,140,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (9009830000984,62,'Colour','White/Royal Blue',270,0,12,24,0,40.00,130,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (2398940009389,63,'Colour','Red/Smoke',280,0,12,24,0,22.00,120,0,1,0,1);

INSERT INTO items (barcode, product_id, option_name, option_value, retail_quantity, wholesale_quantity, inner_carton_size, outer_carton_size, wholesale_price, retail_price, weight, is_wholesale, is_retail, published_wholesale, published_retail)
VALUES (3000948500093,64,'Colour','Violet Quartz',230,0,12,24,0,9.00,200,0,1,0,1);



UNLOCK TABLES;
