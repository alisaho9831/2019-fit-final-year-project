LOCK TABLES order_lines WRITE;

# INSERT INTO order_lines (order_id, item_id, fulfillment_status, price, amount)
# VALUES (2,1,'Not fulfilled',4.00,1);
#
# INSERT INTO order_lines (order_id, item_id, fulfillment_status, price, amount)
# VALUES (2, 26, 'Not Fulfilled',84,1);

INSERT INTO order_lines (order_id, item_id, fulfillment_status, price, amount)
VALUES (1,18,'Not fulfilled',140.00,1);

# INSERT INTO order_lines (order_id, item_id, fulfillment_status, price, amount)
# VALUES (1, 26, 'Not Fulfilled',84,1);
#
# INSERT INTO order_lines (order_id, item_id, fulfillment_status, price, amount)
# VALUES (1, 10, 'Not Fulfilled',334,1);

UNLOCK TABLES;