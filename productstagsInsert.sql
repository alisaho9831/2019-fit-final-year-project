LOCK TABLES products_tags WRITE;

INSERT INTO products_tags (tag_id, product_id)
VALUES (2,3);

INSERT INTO products_tags (tag_id, product_id)
VALUES (22,3);

INSERT INTO products_tags (tag_id, product_id)
VALUES (22,6);

INSERT INTO products_tags (tag_id, product_id)
VALUES (31,6);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,7);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,8);

INSERT INTO products_tags (tag_id, product_id)
VALUES (20,10);

INSERT INTO products_tags (tag_id, product_id)
VALUES (20,11);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,33);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,34);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,35);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,46);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,47);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,48);

INSERT INTO products_tags (tag_id, product_id)
VALUES (7,50);

UNLOCK TABLES;