<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ContentManagementTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ContentManagementTable Test Case
 */
class ContentManagementTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ContentManagementTable
     */
    public $ContentManagement;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ContentManagement'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ContentManagement') ? [] : ['className' => ContentManagementTable::class];
        $this->ContentManagement = TableRegistry::get('ContentManagement', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ContentManagement);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
