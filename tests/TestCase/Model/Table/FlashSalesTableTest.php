<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FlashSalesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FlashSalesTable Test Case
 */
class FlashSalesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\FlashSalesTable
     */
    public $FlashSales;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.FlashSales',
        'app.Collections',
        'app.ProductCollections',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.PromoCodes',
        'app.ProductImages',
        'app.ProductTags',
        'app.WishlistProducts',
        'app.TagCollections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('FlashSales') ? [] : ['className' => FlashSalesTable::class];
        $this->FlashSales = TableRegistry::get('FlashSales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->FlashSales);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
