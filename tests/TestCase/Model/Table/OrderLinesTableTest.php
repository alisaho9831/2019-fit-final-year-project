<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderLinesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderLinesTable Test Case
 */
class OrderLinesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderLinesTable
     */
    public $OrderLines;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.Users',
        'app.Products',
        'app.Items',
        'app.Productimages',
        'app.Collections',
        'app.FlashSales',
        'app.ProductCollections',
        'app.TagCollections',
        'app.ProductTags',
        'app.CollectionsProducts',
        'app.CustomersProducts',
        'app.Tags',
        'app.ProductsTags',
        'app.PromoCodes',
        'app.Admins'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Orderlines') ? [] : ['className' => OrderLinesTable::class];
        $this->OrderLines = TableRegistry::get('Orderlines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderLines);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
