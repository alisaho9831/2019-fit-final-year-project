<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TagCollectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TagCollectionsTable Test Case
 */
class TagCollectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TagCollectionsTable
     */
    public $TagCollections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TagCollections',
        'app.ProductTags',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.PromoCodes',
        'app.ProductCollections',
        'app.Collections',
        'app.FlashSales',
        'app.ProductImages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TagCollections') ? [] : ['className' => TagCollectionsTable::class];
        $this->TagCollections = TableRegistry::get('TagCollections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TagCollections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
