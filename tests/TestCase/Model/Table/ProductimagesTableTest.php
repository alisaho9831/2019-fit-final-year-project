<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductimagesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductimagesTable Test Case
 */
class ProductimagesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductimagesTable
     */
    public $Productimages;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Productimages',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.Users',
        'app.CustomersProducts',
        'app.PromoCodes',
        'app.ProductCollections',
        'app.Collections',
        'app.FlashSales',
        'app.TagCollections',
        'app.ProductTags',
        'app.ProductImages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Productimages') ? [] : ['className' => ProductimagesTable::class];
        $this->Productimages = TableRegistry::get('Productimages', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Productimages);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
