<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProductCollectionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProductCollectionsTable Test Case
 */
class ProductCollectionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProductCollectionsTable
     */
    public $ProductCollections;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProductCollections',
        'app.Products',
        'app.Collections',
        'app.FlashSales',
        'app.TagCollections'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProductCollections') ? [] : ['className' => ProductCollectionsTable::class];
        $this->ProductCollections = TableRegistry::get('ProductCollections', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductCollections);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
