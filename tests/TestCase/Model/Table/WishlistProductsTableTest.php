<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WishlistProductsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WishlistProductsTable Test Case
 */
class WishlistProductsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\WishlistProductsTable
     */
    public $WishlistProducts;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.WishlistProducts',
        'app.Wishlists',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.PromoCodes',
        'app.ProductCollections',
        'app.Collections',
        'app.FlashSales',
        'app.TagCollections',
        'app.ProductTags',
        'app.ProductImages'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('WishlistProducts') ? [] : ['className' => WishlistProductsTable::class];
        $this->WishlistProducts = TableRegistry::get('WishlistProducts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->WishlistProducts);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
