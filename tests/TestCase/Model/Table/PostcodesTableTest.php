<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PostcodesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PostcodesTable Test Case
 */
class PostcodesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PostcodesTable
     */
    public $Postcodes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Postcodes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Postcodes') ? [] : ['className' => PostcodesTable::class];
        $this->Postcodes = TableRegistry::get('Postcodes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Postcodes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
