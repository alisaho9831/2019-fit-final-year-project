<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CollectionsTagsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CollectionsTagsTable Test Case
 */
class CollectionsTagsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CollectionsTagsTable
     */
    public $CollectionsTags;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CollectionsTags',
        'app.Tags',
        'app.Collections',
        'app.FlashSales',
        'app.ProductCollections',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.Users',
        'app.CustomersProducts',
        'app.PromoCodes',
        'app.Admins',
        'app.Productimages',
        'app.CollectionsProducts',
        'app.ProductsTags',
        'app.TagCollections',
        'app.ProductTags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('CollectionsTags') ? [] : ['className' => CollectionsTagsTable::class];
        $this->CollectionsTags = TableRegistry::get('CollectionsTags', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->CollectionsTags);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
