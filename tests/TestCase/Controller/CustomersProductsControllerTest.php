<?php
namespace App\Test\TestCase\Controller;

use App\Controller\CustomersProductsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\CustomersProductsController Test Case
 */
class CustomersProductsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.CustomersProducts',
        'app.Customers',
        'app.Users',
        'app.Addresses',
        'app.Orders',
        'app.PromoCodes',
        'app.Orderlines',
        'app.Items',
        'app.Products',
        'app.ProductCollections',
        'app.Collections',
        'app.FlashSales',
        'app.TagCollections',
        'app.ProductTags',
        'app.ProductImages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
