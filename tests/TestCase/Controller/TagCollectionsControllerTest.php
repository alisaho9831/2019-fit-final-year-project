<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TagCollectionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TagCollectionsController Test Case
 */
class TagCollectionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.TagCollections',
        'app.ProductTags',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.PromoCodes',
        'app.ProductCollections',
        'app.Collections',
        'app.FlashSales',
        'app.ProductImages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
