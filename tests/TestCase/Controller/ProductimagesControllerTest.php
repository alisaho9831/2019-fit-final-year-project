<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ProductimagesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ProductimagesController Test Case
 */
class ProductimagesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Productimages',
        'app.Products',
        'app.Items',
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.Users',
        'app.CustomersProducts',
        'app.PromoCodes',
        'app.ProductCollections',
        'app.Collections',
        'app.FlashSales',
        'app.TagCollections',
        'app.ProductTags',
        'app.ProductImages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
