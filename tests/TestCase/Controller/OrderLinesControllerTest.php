<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OrderlinesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OrderlinesController Test Case
 */
class OrderLinesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Orderlines',
        'app.Orders',
        'app.Addresses',
        'app.Customers',
        'app.Users',
        'app.Products',
        'app.Items',
        'app.Productimages',
        'app.Collections',
        'app.FlashSales',
        'app.ProductCollections',
        'app.TagCollections',
        'app.ProductTags',
        'app.CollectionsProducts',
        'app.CustomersProducts',
        'app.Tags',
        'app.ProductsTags',
        'app.PromoCodes',
        'app.Admins'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
