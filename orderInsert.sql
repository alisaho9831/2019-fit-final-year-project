--
-- Dumping data for table `addresses`
--
LOCK TABLES addresses WRITE;

INSERT INTO addresses (email, financial_status, paid_date, fulfillment_status, fulfillment_date, subtotal, taxes_cost, shipping_cost, total_cost, discount_code, discount_amount, shipping_method, order_date, shipping_address_id, billing_address_id, promo_code_id, payment_method, order_status, notes, customer_id, is_shopping_cart)
VALUES ('ho9831@gmail.com','unpaid',STR_TO_DATE('01/01/1990', '%m/%d/%Y'),'not completed',STR_TO_DATE('01/01/1990', '%m/%d/%Y'),0,0,0,0,'NA',0,'NA',STR_TO_DATE('01/01/1990', '%m/%d/%Y'),0,0,0,'NA','NOT AN ORDER','NA',12,1);


UNLOCK TABLES;