--
-- Dumping data for table `promo_codes`
--

LOCK TABLES promo_codes WRITE;

INSERT INTO promo_codes (promo_code, discount_type, discount_amount, start_date, end_date)
VALUES ('000000','NA',0,STR_TO_DATE('01/01/1990', '%m/%d/%Y'),STR_TO_DATE('01/01/1990', '%m/%d/%Y'));

UNLOCK TABLES;