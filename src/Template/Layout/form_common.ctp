<?= $this->Html->css("form.css") ?>
<?= $this->Flash->render(); ?>

<section class="content">
    <div class="row">
        <!--------------------- Content ------------------------------->
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="box-body row justify-content-center">
                <div  id="register-sub">
                    <h2 class="registration-title"><?= $this->fetch("formHeading"); ?></h2>
                    <?= $this->fetch("content") ?>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

    <?= $this->fetch("validation"); ?>
</section>

