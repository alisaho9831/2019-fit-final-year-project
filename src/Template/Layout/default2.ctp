<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $this->fetch('title', 'Welcome to Smooth Sales') ?>
    </title>

    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('query-ui.css') ?>
    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('custom.css') ?>
    <?= $this->Html->css('magnific-popup.css') ?>
    <?= $this->Html->css('animate.css') ?>
    <?= $this->Html->css('owl.carousel.min.css') ?>
    <?= $this->Html->css('themify-icons.css') ?>
    <?= $this->Html->css('pe-icon-7-stroke.css') ?>
    <?= $this->Html->css('icofont.css') ?>
    <?= $this->Html->css('meanmenu.min.css') ?>
    <?= $this->Html->css('bundle.css') ?>
    <?= $this->Html->css('jquery-ui.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('easyzoom.css') ?>
    <?= $this->Html->css('hamburgers.min.css') ?>
    <?= $this->Html->css('responsive.css') ?>
    <?= $this->Html->css('slinky.css') ?>
    <?= $this->Html->script('/vendor/modernizr-2.8.3.min.js') ?>


    <link rel="shortcut icon" type="image/png" href="img/logo/smooth_sales.png">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>

<?= $this->fetch('content') ?>


<?= $this->Html->script('/vendor/jquery-1.12.0.min.js') ?>
<?= $this->Html->script('popper.js') ?>
<?= $this->Html->script('bootstrap.min.js') ?>
<?= $this->Html->script('jquery.magnific-popup.min.js') ?>
<?= $this->Html->script('isotope.pkgd.min.js') ?>
<?= $this->Html->script('imagesloaded.pkgd.min.js') ?>
<?= $this->Html->script('jquery.counterup.min.js"') ?>
<?= $this->Html->script('waypoints.min.js"') ?>
<?= $this->Html->script('ajax-mail.js') ?>
<?= $this->Html->script('owl.carousel.min.js') ?>
<?= $this->Html->script('plugins.js') ?>
<?= $this->Html->script('main.js') ?>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
