<!-- Wishlist start -->
<div class="cart-main-area pt-95 pb-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h1 class="cart-heading">Wishlist <?php /*var_dump($email)*/?></h1>

                <form action="#">
                    <div class="table-content table-responsive">
                        <?= $this->Flash->render() ?>
                        <table>
                            <thead>
                            <!--<tr>
                                <th>images</th>
                                <th>Product</th>
                                <th>Price</th>
                                <th>Remove</th>
                            </tr>-->
                            </thead>
                            <tbody>
                            <?php
                                foreach ( $wishlist_products as $wishlist_product):
                                   //print_r($shopping_cart_item);

                            ?>
                            <tr>

                                <td class="product-thumbnail">
                                    <?= $this->Html->image($wishlist_product->image, ['url' => ['controller' => 'Products', 'action' => 'normal_detail', '?' => ['product' => $wishlist_product->products_id]]]);?>
                                </td>
                                <td class="product-name"><?php echo $this->Html->link($wishlist_product->title, ['controller' => 'Products', 'action' => 'normal_detail', '?' => ['product' => $wishlist_product->products_id]]); ?></td>
                                <td class="product-price-cart"><span class="amount">$<?= $wishlist_product->retail_price?></span></td>
                                <td>
                                    <a class="animate-right" data-toggle="modal" data-target="#exampleModal-delete-<?= $wishlist_product->products_id?>" href="#">
                                        REMOVE
                                    </a>
                                </td>
                            </tr>

                            <?php endforeach?>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Wishlist-area end -->

<!--Delete Confirm Model-->
<?php
    foreach ($wishlist_products as $wishlist_product):?>
       <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'customers_products',
            'action' => 'deleteProduct'
        ]
    ])?>
        <div class="modal fade" id="exampleModal-delete-<?= $wishlist_product->products_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="pe-7s-close" aria-hidden="true"></span>
            </button>
            <div class="modal-dialog delete-modal-quickview-width" role="document">
                <div class="modal-content">
                    <div class="remove-quick-view-content">
                        <h6>Do you want to remove the item from the cart?</h6><br>
                        <h4><?= $wishlist_product->title?></h4><br>
                        <?=$this->form->hidden('product_id',['value'=>$wishlist_product->products_id ]);?>
                        <?=$this->Form->submit(__('Confirm'),['id' => 'confirmbtn']);?>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Form->end() ?>
<?php endforeach ?>
