<!--footer-->
<div class="footer-top-area pt-15 pb-0" style="background-color:#F44344;">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-3">
                <div class="footer-widget mb-20">
                    <h3 class="footer-widget-title">About Us</h3>
                    <div class="footer-widget-content">
                        <ul>
                            <li><a <?= $this->Html->link('Privacy Policy', ['controller' => 'contentManagement', 'action' => 'PrivacyPolicy']) ?></a></li>
                            <li><a <?= $this->Html->link('Refund Policy', ['controller' => 'contentManagement', 'action' => 'RefundPolicy']) ?></a></li>
                            <li><a <?= $this->Html->link('Terms of Service', ['controller' => 'contentManagement', 'action' => 'TermsOfService']) ?></a></li>
                            <li><a <?= $this->Html->link('Sell Through Us', ['controller' => 'contentManagement', 'action' => 'sellThroughUs']) ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-3">
                <div class="footer-widget mb-40">
                    <h3 class="footer-widget-title">Enquiries</h3>
                    <div class="footer-widget-content">
                        <ul>
                            <li><a <?= $this->Html->link('FAQ', ['controller' => 'contentManagement', 'action' => 'FAQ']) ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="footer-widget mb-20">
                    <h3 class="footer-widget-title">Contact</h3>
                    <div class="footer-newsletter">
                        <p>Have a question?
                            Give us a call on: (03) 9013 6774. Open Monday-Friday (9am-5pm)</p>
                        <div id="mc_embed_signup" class="subscribe-form pr-40">

                        </div>
                        <div class="footer-contact">
                            <p><span><i class=" ti-headphone-alt "></i></span>9013&nbsp;6774 </p>
                            <p><span><i class="pe-7s-mail" ></i></span> <a href="mailto:info@smoothsales.com.au"> info@smoothsales.com.au </p>

                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
</div>
<div class="footer-bottom ptb-20" style="background-color:#EE6263"; >
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="copyright">
                    <p>
                        Copyright ©
                        <a href="https://smoothsales.com.au/">Smooth Sales</a>
                        2019. All Rights Reserved.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!--footer end-->
