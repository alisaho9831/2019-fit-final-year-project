<!--Header-->
<div class="header">
    <div class="header-bottom ptb-20 clearfix ">
        <div class="header-bottom-wrapper">
            <div class="logo-3">
                    <?= $this->Html->image('logo/smoothsales.png', ['alt' => 'CakePHP', 'url' => ['controller' => 'Articles', 'action' => 'wholesale_home']]) ?>

            </div>
            <div class="categories-search-wrapper categories-search-wrapper2">
                <!--<div class="all-categories">
                    <div class="select-wrapper">

                        <select class="select">
                            <option value="0">All Categories</option>
                            <?php /*foreach ($searchCategories as $searchCategory): */?>
                                <option value="<?/*= $searchCategory->id */?>"><?/*=$searchCategory->title */?></option>
                            <?php /*endforeach;*/?>
                        </select>
                    </div>
                </div>-->
                <div class="categories-wrapper">

                    <?=$this->Form->create(null, [
                            'url' => [
                            'controller' => 'Products',
                            'action' => 'searchWholesale'
                        ]
                    ])?>
                    <?=$this->Form->text('keyword', ['autocomplete' => 'off', 'placeholder' => 'Find a product'])?>
                    <?=$this->Form->button('Search', ['type' => 'submit'])?>
                    <?= $this->Form->end() ?>

                </div>
            </div>
            <div class="header-cart-3">

                <?= $this->Html->link($this->Html->tag("i",'', array('class' => 'pe-7s-users')).$text,$url,["escape"=>false]) ?>




                <!-- only show my account drop down with options if customer is logged in-->
<!--               <li> -->
                    <?php if($this->request->getSession()->read('Auth')){ ?>
                        <?= $this->Html->link($this->Html->tag("i",'', array('class' => 'pe-7s-cart')).'Shopping Cart',["controller"=>"Orders", "action"=>"wholesale_cart"],["escape"=>false]) ?>
                        <?= $this->Html->link($this->Html->tag("i",'', array('class' => 'pe-7s-like')).'Wishlist',["controller"=>"CustomersProducts", "action"=>"wholesale_wishlist" ],["escape"=>false]) ?>
                        <?= $this->Html->link($this->Html->tag("i",'', array('class' => 'pe-7s-users')).'Logout',['controller' => 'Users', 'action' => 'logout'],["escape"=>false]) ?>
                    <?php } ?>
<!--                    <ul class="dropdown">-->
<!--                        <li>  --><?php //if($this->request->getSession()->read('Auth')){
//                                echo  $this->Html->link( 'View account', [] );
//                            } ?>
<!--                        </li>-->
<!--                        <li>  --><?php //if($this->request->getSession()->read('Auth')){
//                                echo  $this->Html->link( 'My Orders', [] );
//                            } ?>
<!--                        </li>-->
<!--                        <li>  --><?php //if($this->request->getSession()->read('Auth')){
//                                echo  $this->Html->link( 'Logout', ['controller' => 'customers','action' => 'logout'] );
//                            } ?>
<!--                        </li>-->
<!--                    </ul>-->
<!--                </li>-->


            </div>
        </div>
    </div>
    <!--header end-->



    <!--navbar-->
    <div class="nav-bar">
        <ul class="nav-menu">
            <li><a href="#" class="selected">All Categories ▾</a>
                <ul>
                    <?php foreach ($mainMenu as $mainMenuItems):
                    $id = $mainMenuItems->id;
                    ?>
                    <li>
                        <?= $this->Html->link( $mainMenuItems->title, ['controller' => 'products', 'action' => 'wholesale_listing', $mainMenuItems->id]) ?>
                        <ul>
                            <?php foreach ($submenu as $submenuItems):
                            $child_id = $submenuItems->parent_collection;
                            if($id==$child_id){
                            ?>
                            <li><?= $this->Html->link( $submenuItems->title, ['controller' => 'products', 'action' => 'wholesale_listing', $submenuItems->id]) ?></li>
                                <?php
                            }
                                ?>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <?php endforeach ?>
                </ul>
            </li>
            <li>
                <?=$this->Html->link('Clothes', ['controller' => 'Products', 'action' => 'wholesaleListing', 2])?>
            </li>
            <li>
                <?=$this->Html->link('Tech', ['controller' => 'Products', 'action' => 'wholesaleListing', 8])?>
            </li>
            <li>
                <?= $this->Html->link('Beauty', ['controller' => 'Products', 'action' => 'wholesaleListing', 9])?>
            </li>
            <li>
                <?= $this->Html->link('Toys & Games', ['controller' => 'Products', 'action' => 'wholesaleListing', 17])?>
            </li>
            <li>
                <?= $this->Html->link('Entertainment', ['controller' => 'Products', 'action' => 'wholesaleListing', 22])?>
            </li>
            <li>
                <?= $this->Html->link('Cooking', ['controller' => 'Products', 'action' => 'wholesaleListing', 24])?>
            </li>
            <li>
                <?= $this->Html->link('Travel', ['controller' => 'Products', 'action' => 'wholesaleListing', 26])?>
            </li>
        </ul>
    </div>
</div>
