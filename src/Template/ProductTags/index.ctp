<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductTag[]|\Cake\Collection\CollectionInterface $productTags
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Product Tag'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productTags index large-9 medium-8 columns content">
    <h3><?= __('Product Tags') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tag_name') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productTags as $productTag): ?>
            <tr>
                <td><?= $this->Number->format($productTag->id) ?></td>
                <td><?= $productTag->has('product') ? $this->Html->link($productTag->product->title, ['controller' => 'Products', 'action' => 'view', $productTag->product->id]) : '' ?></td>
                <td><?= h($productTag->tag_name) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $productTag->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productTag->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productTag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productTag->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
