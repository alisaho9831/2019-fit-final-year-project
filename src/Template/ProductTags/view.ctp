<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductTag $productTag
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Product Tag'), ['action' => 'edit', $productTag->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product Tag'), ['action' => 'delete', $productTag->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productTag->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Product Tags'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Tag'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productTags view large-9 medium-8 columns content">
    <h3><?= h($productTag->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $productTag->has('product') ? $this->Html->link($productTag->product->title, ['controller' => 'Products', 'action' => 'view', $productTag->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tag Name') ?></th>
            <td><?= h($productTag->tag_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($productTag->id) ?></td>
        </tr>
    </table>
</div>
