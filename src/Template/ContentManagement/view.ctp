<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContentManagement $contentManagement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Content Management'), ['action' => 'edit', $contentManagement->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Content Management'), ['action' => 'delete', $contentManagement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contentManagement->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Content Management'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Content Management'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contentManagement view large-9 medium-8 columns content">
    <h3><?= h($contentManagement->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($contentManagement->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Content') ?></th>
            <td><?= h($contentManagement->content) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contentManagement->id) ?></td>
        </tr>
    </table>
</div>
