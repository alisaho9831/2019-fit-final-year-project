<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContentManagement $contentManagement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Content Management'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="contentManagement form large-9 medium-8 columns content">
    <?= $this->Form->create($contentManagement) ?>
    <fieldset>
        <legend><?= __('Add Content Management') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('content');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
