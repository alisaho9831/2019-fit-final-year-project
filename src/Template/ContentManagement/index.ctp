<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContentManagement[]|\Cake\Collection\CollectionInterface $contentManagement
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Content Management'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contentManagement index large-9 medium-8 columns content">
    <h3><?= __('Content Management') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                <th scope="col"><?= $this->Paginator->sort('content') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contentManagement as $contentManagement): ?>
            <tr>
                <td><?= $this->Number->format($contentManagement->id) ?></td>
                <td><?= h($contentManagement->title) ?></td>
                <td><?= h($contentManagement->content) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contentManagement->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contentManagement->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contentManagement->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contentManagement->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
