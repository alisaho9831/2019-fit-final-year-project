
<?php error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Registration Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="webroot/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
    <!-- STYLE CSS -->
    <?= $this->Html->css('signupstyle.css') ?>
</head>

<body>
<div class="wrapper">
    <div class="image-holder">
        <?= $this->Html->image('Items.jpg') ?>
    </div>
    <div class="form-inner">
        <?=$this->Form->create(null, [
            'url' => [
                'controller' => 'Users',
                'action' => 'signup'
            ]
        ])?>
            <div class="form-header">
                <h3>Sign up</h3>
            </div>
            <br>
            <div class="signup_error">
                <b><?= $this->Flash->render() ?></b>
            </div>
            <br>
            <div class="form-group">
                <?= $this->Form->control('First Name',['required' => True,'class'=>'form-control','name'=>'first_name']);?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('Last Name',['required' => True,'class'=>'form-control','name'=>'last_name']);?>
            </div>
            <div class="form-group">
                <?= $this->Form->control('Email',['required' => True,'class'=>'form-control','type'=>'email','name'=>'username']);?>
            </div>
        <div class="form-group">
            <?= $this->Form->control('Confirm Email',['required' => True,'class'=>'form-control','type'=>'email','name'=>'username2']);?>
        </div>
            <div class="form-group">
                <?=$this->Form->control('password',['required' => True,'class'=>'form-control','required pattern'=>"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}",
                    'title'=>"Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"]);?>
            </div>
            <div class="form-group" >
                <?=$this->Form->control('Confirm password',['required' => True,'class'=>'form-control', 'type'=>'password', 'name'=>'pwd2']); ?>
            </div>


<!--            --><?php
//            echo $this->Form->hidden('role',['default' => 'Customer']);
//            echo $this->Form->hidden('created',['default'=> date('Y-m-d H:i:s')]);
//            echo $this->Form->hidden('modified',['default'=> date('Y-m-d H:i:s')]);
//            ?>
            <?= $this->Form->button(__('create my account'),['id'=>'btn']) ?>

<!--            <div class="socials">-->
<!--                <p>Sign up with social platforms</p>-->
<!--                <a href="" class="socials-icon">-->
<!--                    <i class="zmdi zmdi-facebook"></i>-->
<!--                </a>-->
<!--                <a href="" class="socials-icon">-->
<!--                    <i class="zmdi zmdi-instagram"></i>-->
<!--                </a>-->
<!--                <a href="" class="socials-icon">-->
<!--                    <i class="zmdi zmdi-twitter"></i>-->
<!--                </a>-->
<!--                <a href="" class="socials-icon">-->
<!--                    <i class="zmdi zmdi-tumblr"></i>-->
<!--                </a>-->
<!--            </div>-->
            <?= $this->Form->end() ?>
    </div>

</div>

<?= $this->Html->script('jquery-3.3.1.min.js') ?>
<?= $this->Html->script('jquery.form-validator.min.js') ?>
<?= $this->Html->script('signup.js') ?>

</body>
</html>
