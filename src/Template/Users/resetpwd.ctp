
<?php $this->extend("/Layout/form_common") ?>
<?php $this->assign("formHeading", "Reset Password"); ?>

<?= $this->Form->create(); ?>
    <div style="width: 420px; margin: 0 auto">
        <label> Email </label><?php echo $this->Form->control('email', ['required', 'type'=>'email', 'label'=> false, 'class'=>'form-control']); ?>
        <div style="height:10px"></div>
        <label> Reset Code </label><?php echo $this->Form->control('verify_code', ['required', 'label'=> false, 'class'=>'form-control']); ?>
        <div style="height:10px"></div>
        <label>New Password</label><?php echo $this->Form->control('password', ['class'=>'form-control', 'required', 'label'=>false]) ?>
        <div style="height:10px"></div>
<!--        <label>Confirm Password</label>--><?php //echo $this->Form->control('password_confirm', ['class'=>'form-control', 'required', 'label'=>false]) ?>

        <div style="padding: 25px">
            <?= $this->Form->button("Save Changes", ['type' => 'submit','class'=>'button'])?>
        </div>

    </div>
<?= $this->Form->end(); ?>




