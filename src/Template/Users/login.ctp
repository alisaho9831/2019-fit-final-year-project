
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Registration Form</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- MATERIAL DESIGN ICONIC FONT -->
    <link rel="stylesheet" href="webroot/fonts/material-design-iconic-font/css/material-design-iconic-font.min.css">
    <!-- STYLE CSS -->
    <?= $this->Html->css('signupstyle.css') ?>
</head>


<body>


<div class="wrapper">
    <div class="image-holder">
        <?= $this->Html->image('Items.jpg') ?>
    </div>
    <div class="form-inner">
        <?= $this->Form->create();?>
        <div class="form-header">
            <h3>Sign In </h3>

        </div>
        <br>
        <b><?= $this->Flash->render() ?></b>
        <br>

        <div class="form-group">
            <?= $this->Form->control('Email',['required' => True,'class'=>'form-control','type'=>'email','name'=>'username']);?>
        </div>
        <div class="form-group">
            <?=$this->Form->control('password',['required' => True,'class'=>'form-control','required pattern'=>"(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}",
                'title'=>"Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"]);?>
        </div>
        <?= $this->Form->button(__('Login')) ?>

        <br>
        <br>
        <center><?=$this->Html->link('Forgot password?', ['controller' => 'Users', 'action' => 'forgetpwd'], ['class'=>'signUpButton']); ?></center><br>
        <center><?=$this->Html->link('NOT A MEMBER? JOIN NOW', ['controller' => 'Users', 'action' => 'signup'], ['class'=>'signUpButton']); ?></center><br>


        <!--                    <div class="socials">-->
        <!--                        <p>Sign up with social platforms</p>-->
        <!--                        <a href="" class="socials-icon">-->
        <!--                            <i class="zmdi zmdi-facebook"></i>-->
        <!--                        </a>-->
        <!--                        <a href="" class="socials-icon">-->
        <!--                            <i class="zmdi zmdi-instagram"></i>-->
        <!--                        </a>-->
        <!--                        <a href="" class="socials-icon">-->
        <!--                            <i class="zmdi zmdi-twitter"></i>-->
        <!--                        </a>-->
        <!--                        <a href="" class="socials-icon">-->
        <!--                            <i class="zmdi zmdi-tumblr"></i>-->
        <!--                        </a>-->
        <!--                    </div>-->
        <?= $this->Form->end() ?>

    </div>

</div>

<?= $this->Html->script('jquery-3.3.1.min.js') ?>
<?= $this->Html->script('jquery.form-validator.min.js') ?>
<?= $this->Html->script('signup.js') ?>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
