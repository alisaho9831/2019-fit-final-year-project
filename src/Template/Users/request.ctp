
<?php $this->extend("/Layout/form_common") ?>
<?php $this->assign("formHeading", "Wholesale Enquiries"); ?>



<?= $this->Form->create(); ?>
<div style="width: 420px; margin: 0 auto">
        <div class="form-group">
            <?= $this->Form->control('Name',['required' => True,'class'=>'form-control','name'=>'username']);?>
            <?= $this->Form->control('Email',['required' => True,'class'=>'form-control','type'=>'email']);?>
        </div>

        <div class="form-group">
            <?=$this->Form->control('Phone',['required' => True,'class'=>'form-control']);?>
            <?=$this->Form->control('ABN',['required' => false,'abnValidate'=> True,'class'=>'form-control']);?>
            <?=$this->Form->control('Description',['required' => True,'rows' => '5', 'cols' => '5','class'=>'form-control']);?>

        </div>
        <div style="padding: 25px">
        <?= $this->Form->button('Submit', ['type' => 'submit','class'=>'button']); ?>
</div>

<?= $this->Form->end(); ?>



<script>
      function abnValidate(value, element){
        if (value.length != 11 || isNaN(parseInt(value)))
            return false;
        var weighting =[10,1,3,5,7,9,11,13,15,17,19];
        var tally = (parseInt(value[0]) - 1) * weighting[0];
        for (var i = 1; i < value.length; i++){
            tally += (parseInt(value[i]) * weighting[i]);
        }
        return (tally % 89) == 0;
    }

    jQuery.validator.addMethod(
        'abnValidate',
        abnValidate, 'This ABN is not valid'
    );
</script>

<?= $this->Html->script("validations.js") ?>

