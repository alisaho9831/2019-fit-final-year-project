
<?php $this->extend("/Layout/form_common") ?>
<?php $this->assign("formHeading", "Reset Password"); ?>

        <h6>Please enter the email address for your account.</h6>
        <h6>You will be emailed a link to reset your password.</h6>
        <h6>Please check your email account’s Spam or Junk folder to ensure the message was not filtered.</h6>
        <div style="height:10px"></div>
        <?= $this->Form->create(); ?>
        <div style="width: 420px; margin: 0 auto">
            <?= $this->Form->control('email', ["placeholder" => "Email", "label" => false, "autofocus" => "autofocus",'name'=>'username']); ?>

        </div>

        <div style="padding: 25px">
            <?= $this->Form->button('Send Reset Email', ['type' => 'submit','class'=>'button']); ?>
        </div>
        <?= $this->Form->end(); ?>

        <div class="form-group">
            <?= $this->Html->link("< Go back to Login", ['action' => 'login']); ?>
        </div>


