<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item[]|\Cake\Collection\CollectionInterface $items
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Lines'), ['controller' => 'Orderlines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Line'), ['controller' => 'Orderlines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="items index large-9 medium-8 columns content">
    <h3><?= __('Items') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('barcode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('option_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('option_value') ?></th>
                <th scope="col"><?= $this->Paginator->sort('retail_quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wholesale_quantity') ?></th>
                <th scope="col"><?= $this->Paginator->sort('inner_carton_size') ?></th>
                <th scope="col"><?= $this->Paginator->sort('outer_carton_size') ?></th>
                <th scope="col"><?= $this->Paginator->sort('wholesale_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('retail_price') ?></th>
                <th scope="col"><?= $this->Paginator->sort('weight') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_wholesale') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_retail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('published_wholesale') ?></th>
                <th scope="col"><?= $this->Paginator->sort('published_retail') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_deactivated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($items as $item): ?>
            <tr>
                <td><?= $this->Number->format($item->id) ?></td>
                <td><?= h($item->barcode) ?></td>
                <td><?= $item->has('product') ? $this->Html->link($item->product->title, ['controller' => 'Products', 'action' => 'view', $item->product->id]) : '' ?></td>
                <td><?= h($item->option_name) ?></td>
                <td><?= h($item->option_value) ?></td>
                <td><?= $this->Number->format($item->retail_quantity) ?></td>
                <td><?= $this->Number->format($item->wholesale_quantity) ?></td>
                <td><?= $this->Number->format($item->inner_carton_size) ?></td>
                <td><?= $this->Number->format($item->outer_carton_size) ?></td>
                <td><?= $this->Number->format($item->wholesale_price) ?></td>
                <td><?= $this->Number->format($item->retail_price) ?></td>
                <td><?= $this->Number->format($item->weight) ?></td>
                <td><?= h($item->is_wholesale) ?></td>
                <td><?= h($item->is_retail) ?></td>
                <td><?= h($item->published_wholesale) ?></td>
                <td><?= h($item->published_retail) ?></td>
                <td><?= h($item->is_deactivated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $item->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $item->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
