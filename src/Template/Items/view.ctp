<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->id], ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Order Lines'), ['controller' => 'Orderlines', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order Line'), ['controller' => 'Orderlines', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="items view large-9 medium-8 columns content">
    <h3><?= h($item->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Barcode') ?></th>
            <td><?= h($item->barcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $item->has('product') ? $this->Html->link($item->product->title, ['controller' => 'Products', 'action' => 'view', $item->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Option Name') ?></th>
            <td><?= h($item->option_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Option Value') ?></th>
            <td><?= h($item->option_value) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($item->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Retail Quantity') ?></th>
            <td><?= $this->Number->format($item->retail_quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wholesale Quantity') ?></th>
            <td><?= $this->Number->format($item->wholesale_quantity) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Inner Carton Size') ?></th>
            <td><?= $this->Number->format($item->inner_carton_size) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Outer Carton Size') ?></th>
            <td><?= $this->Number->format($item->outer_carton_size) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wholesale Price') ?></th>
            <td><?= $this->Number->format($item->wholesale_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Retail Price') ?></th>
            <td><?= $this->Number->format($item->retail_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Weight') ?></th>
            <td><?= $this->Number->format($item->weight) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Wholesale') ?></th>
            <td><?= $item->is_wholesale ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Retail') ?></th>
            <td><?= $item->is_retail ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Published Wholesale') ?></th>
            <td><?= $item->published_wholesale ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Published Retail') ?></th>
            <td><?= $item->published_retail ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deactivated') ?></th>
            <td><?= $item->is_deactivated ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Order Lines') ?></h4>
        <?php if (!empty($item->order_lines)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Order Id') ?></th>
                <th scope="col"><?= __('Item Id') ?></th>
                <th scope="col"><?= __('Fulfillment Status') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Expiry Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($item->order_lines as $orderLines): ?>
            <tr>
                <td><?= h($orderLines->order_id) ?></td>
                <td><?= h($orderLines->item_id) ?></td>
                <td><?= h($orderLines->fulfillment_status) ?></td>
                <td><?= h($orderLines->price) ?></td>
                <td><?= h($orderLines->amount) ?></td>
                <td><?= h($orderLines->expiry_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orderlines', 'action' => 'view', $orderLines->order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orderlines', 'action' => 'edit', $orderLines->order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orderlines', 'action' => 'delete', $orderLines->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderLines->order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
