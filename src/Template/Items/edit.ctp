<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $item->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $item->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Lines'), ['controller' => 'Orderlines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Line'), ['controller' => 'Orderlines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="items form large-9 medium-8 columns content">
    <?= $this->Form->create($item) ?>
    <fieldset>
        <legend><?= __('Edit Item') ?></legend>
        <?php
            echo $this->Form->control('barcode');
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('option_name');
            echo $this->Form->control('option_value');
            echo $this->Form->control('retail_quantity');
            echo $this->Form->control('wholesale_quantity');
            echo $this->Form->control('inner_carton_size');
            echo $this->Form->control('outer_carton_size');
            echo $this->Form->control('wholesale_price');
            echo $this->Form->control('retail_price');
            echo $this->Form->control('weight');
            echo $this->Form->control('is_wholesale');
            echo $this->Form->control('is_retail');
            echo $this->Form->control('published_wholesale');
            echo $this->Form->control('published_retail');
            echo $this->Form->control('is_deactivated');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
