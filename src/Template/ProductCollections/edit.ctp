<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductCollection $productCollection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productCollection->product_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productCollection->product_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Product Collections'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productCollections form large-9 medium-8 columns content">
    <?= $this->Form->create($productCollection) ?>
    <fieldset>
        <legend><?= __('Edit Product Collection') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
