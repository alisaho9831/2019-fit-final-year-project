<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductCollection[]|\Cake\Collection\CollectionInterface $productCollections
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Product Collection'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productCollections index large-9 medium-8 columns content">
    <h3><?= __('Product Collections') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productCollections as $productCollection): ?>
            <tr>
                <td><?= $productCollection->has('product') ? $this->Html->link($productCollection->product->title, ['controller' => 'Products', 'action' => 'view', $productCollection->product->id]) : '' ?></td>
                <td><?= $productCollection->has('collection') ? $this->Html->link($productCollection->collection->title, ['controller' => 'Collections', 'action' => 'view', $productCollection->collection->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $productCollection->product_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productCollection->product_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productCollection->product_id], ['confirm' => __('Are you sure you want to delete # {0}?', $productCollection->product_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
