<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ProductCollection $productCollection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Product Collection'), ['action' => 'edit', $productCollection->product_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product Collection'), ['action' => 'delete', $productCollection->product_id], ['confirm' => __('Are you sure you want to delete # {0}?', $productCollection->product_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Product Collections'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Collection'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productCollections view large-9 medium-8 columns content">
    <h3><?= h($productCollection->product_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $productCollection->has('product') ? $this->Html->link($productCollection->product->title, ['controller' => 'Products', 'action' => 'view', $productCollection->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection') ?></th>
            <td><?= $productCollection->has('collection') ? $this->Html->link($productCollection->collection->title, ['controller' => 'Collections', 'action' => 'view', $productCollection->collection->id]) : '' ?></td>
        </tr>
    </table>
</div>
