<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CollectionsProduct[]|\Cake\Collection\CollectionInterface $collectionsProducts
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Collections Product'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="collectionsProducts index large-9 medium-8 columns content">
    <h3><?= __('Collections Products') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($collectionsProducts as $collectionsProduct): ?>
            <tr>
                <td><?= $collectionsProduct->has('product') ? $this->Html->link($collectionsProduct->product->title, ['controller' => 'Products', 'action' => 'view', $collectionsProduct->product->id]) : '' ?></td>
                <td><?= $collectionsProduct->has('collection') ? $this->Html->link($collectionsProduct->collection->title, ['controller' => 'Collections', 'action' => 'view', $collectionsProduct->collection->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $collectionsProduct->product_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $collectionsProduct->product_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $collectionsProduct->product_id], ['confirm' => __('Are you sure you want to delete # {0}?', $collectionsProduct->product_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
