<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CollectionsProduct $collectionsProduct
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Collections Product'), ['action' => 'edit', $collectionsProduct->product_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Collections Product'), ['action' => 'delete', $collectionsProduct->product_id], ['confirm' => __('Are you sure you want to delete # {0}?', $collectionsProduct->product_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Collections Products'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collections Product'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="collectionsProducts view large-9 medium-8 columns content">
    <h3><?= h($collectionsProduct->product_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $collectionsProduct->has('product') ? $this->Html->link($collectionsProduct->product->title, ['controller' => 'Products', 'action' => 'view', $collectionsProduct->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection') ?></th>
            <td><?= $collectionsProduct->has('collection') ? $this->Html->link($collectionsProduct->collection->title, ['controller' => 'Collections', 'action' => 'view', $collectionsProduct->collection->id]) : '' ?></td>
        </tr>
    </table>
</div>
