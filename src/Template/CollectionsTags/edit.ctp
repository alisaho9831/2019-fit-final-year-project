<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CollectionsTag $collectionsTag
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $collectionsTag->tag_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $collectionsTag->tag_id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Collections Tags'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="collectionsTags form large-9 medium-8 columns content">
    <?= $this->Form->create($collectionsTag) ?>
    <fieldset>
        <legend><?= __('Edit Collections Tag') ?></legend>
        <?php
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
