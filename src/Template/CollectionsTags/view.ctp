<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\CollectionsTag $collectionsTag
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Collections Tag'), ['action' => 'edit', $collectionsTag->tag_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Collections Tag'), ['action' => 'delete', $collectionsTag->tag_id], ['confirm' => __('Are you sure you want to delete # {0}?', $collectionsTag->tag_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Collections Tags'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collections Tag'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tags'), ['controller' => 'Tags', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tag'), ['controller' => 'Tags', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="collectionsTags view large-9 medium-8 columns content">
    <h3><?= h($collectionsTag->tag_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Tag') ?></th>
            <td><?= $collectionsTag->has('tag') ? $this->Html->link($collectionsTag->tag->id, ['controller' => 'Tags', 'action' => 'view', $collectionsTag->tag->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Collection') ?></th>
            <td><?= $collectionsTag->has('collection') ? $this->Html->link($collectionsTag->collection->title, ['controller' => 'Collections', 'action' => 'view', $collectionsTag->collection->id]) : '' ?></td>
        </tr>
    </table>
</div>
