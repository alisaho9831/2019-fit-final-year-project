
<!-- checkout-area start -->
<div class="checkout-area ptb-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Thank you for purchasing!</h2>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                    <div class="checkbox-form">
                        <h3>Shipping Address</h3>
                        <div class="row">
                            <div class="address">
                                <!--                            --><?php //print_r($shippingaddress)?>
                                <?php
                                foreach ($shippingaddress as $shipping){ ?>
                                    <h6><?= $shipping->address1?></h6>
                                    <h6><?= $shipping->address2?></h6>
                                    <h6><?= $shipping->city.' '.$shipping->state?></h6>
                                    <h6><?= $shipping->post_code?></h6>
                                    <h6><?= $shipping->country?></h6>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox-form">
                        <h3>Billing Address</h3>
                        <div class="row">
                            <div class="address">
                                <!--                            --><?php //print_r($shippingaddress)?>
                                <?php
                                foreach ($billingaddress as $billing){ ?>
                                    <h6><?= $billing->address1?></h6>
                                    <h6><?= $billing->address2?></h6>
                                    <h6><?= $billing->city.' '. $billing->state?></h6>
                                    <h6><?= $billing->post_code?></h6>
                                    <h6><?= $billing->country?></h6>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12">
                <div class="your-order">
                    <h3>Your order</h3>
                    <div class="your-order-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="product-name">Product</th>
                                <th class="product-total">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ( $shopping_cart_items as $shopping_cart_item):
                                //print_r($shopping_cart_item);

                                ?>
                                <tr class="cart_item">
                                    <td class="product-name">
                                        <?= $shopping_cart_item->title?><strong class="product-quantity"> × <?= $shopping_cart_item->amount?></strong>
                                    </td>
                                    <td class="product-total">
                                        <span class="amount"><?= $shopping_cart_item->price?></span>
                                    </td>
                                </tr>
                            <?php endforeach?>
                            </tbody>
                            <tfoot>
                            <?php
                            foreach ( $subtotal as $subtotal):
                                ?>
                                <tr class="cart-subtotal">
                                    <th>Shipping Method</th>
                                    <td><span class="amount"><?= $subtotal->shippingMethod?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>Cart Subtotal</th>
                                    <td><span class="amount">$<?= number_format($subtotal->sum,2)?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>GST (Included)</th>
                                    <td><span class="amount">$<?= number_format($subtotal->tax,2)?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>Shipping Fee</th>
                                    <td><span class="amount">$<?= number_format($subtotal->shippingCost,2)?></span></td>
                                </tr>
                                <tr class="order-total">
                                    <th>Order Total</th>
                                    <td><strong><span class="amount">$<?= number_format($subtotal->total,2)?></span></strong>
                                    </td>
                                </tr>
                            <?php endforeach?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="payment-method">
                        <div class="payment-accordion">
                            <div class="order-button-payment">

<!--                                <form action="/your-charge-code" method="POST">-->
<!--                                    <script-->
<!--                                        src="https://checkout.stripe.com/checkout.js" class="stripe-button"-->
<!--                                        data-key="pk_test_6pRNASCoBOKtIshFeQd4XMUh"-->
<!--                                        data-amount="999"-->
<!--                                        data-name="CheakOut"-->
<!--                                        data-description="Pay"-->
<!--                                        data-locale="auto"-->
<!--                                        data-zip-code="true">-->
<!--                                    </script>-->
<!--                                </form>-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout-area end -->

<!-- all js here -->
</body>
</html>
