<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $order->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Addresses'), ['controller' => 'Addresses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Address'), ['controller' => 'Addresses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Promo Codes'), ['controller' => 'PromoCodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Promo Code'), ['controller' => 'PromoCodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Lines'), ['controller' => 'Orderlines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Line'), ['controller' => 'Orderlines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders form large-9 medium-8 columns content">
    <?= $this->Form->create($order) ?>
    <fieldset>
        <legend><?= __('Edit Order') ?></legend>
        <?php
            echo $this->Form->control('email');
            echo $this->Form->control('financial_status');
            echo $this->Form->control('paid_date');
            echo $this->Form->control('fulfillment_status');
            echo $this->Form->control('fulfillment_date');
            echo $this->Form->control('subtotal');
            echo $this->Form->control('taxes_cost');
            echo $this->Form->control('shipping_cost');
            echo $this->Form->control('total_cost');
            echo $this->Form->control('discount_code');
            echo $this->Form->control('discount_amount');
            echo $this->Form->control('shipping_method');
            echo $this->Form->control('order_date');
            echo $this->Form->control('shipping_address_id');
            echo $this->Form->control('billing_address_id', ['options' => $addresses]);
            echo $this->Form->control('promo_code_id', ['options' => $promoCodes]);
            echo $this->Form->control('payment_method');
            echo $this->Form->control('order_status');
            echo $this->Form->control('tracking_number');
            echo $this->Form->control('shipping_company');
            echo $this->Form->control('notes');
            echo $this->Form->control('is_deactivated');
            echo $this->Form->control('customer_id', ['options' => $customers]);
            echo $this->Form->control('is_shopping_cart');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
