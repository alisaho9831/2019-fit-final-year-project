<?php $this->layout = 'default2';?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ezone - eCommerce HTML5 Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

</head>
<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!-- checkout-area start -->
<div class="checkout-area ptb-20">
    <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'orders',
            'action' => 'addBillingAddress'
        ]
    ])?>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Payment</h2><br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <form action="#">
                    <div class="checkbox-form">
                        <h3>Billing Address</h3>
                        <div class="row">
                            <div class="col-md-6">
                                <?= $this->Form->control('First Name*',['required' => True,'class'=>'checkout-form-list','name'=>'first_name']);?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('Last Name*',['required' => True,'class'=>'checkout-form-list','name'=>'last_name']);?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('Address*',['required' => True,'class'=>'checkout-form-list','name'=>'address1','placeholder'=>'Street address']);?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('',['class'=>'checkout-form-list','name'=>'address2','placeholder'=>'Apartment, suite, unit etc. (optional)']);?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('City*',['required' => True,'class'=>'checkout-form-list','name'=>'city']);?>
                            </div>
                            <div class="col-md-6">
                                <!-- State Select -->
                                <?= $this->Form->control('State/Territory*', [
                                    'options' => $states, 'required' => True,'class'=>'checkout-form-list','name'=>'state'
                                ]); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('Postcode*',['required' => True,'class'=>'checkout-form-list','name'=>'postcode']);?>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->control('Country*', [
                                    'options' => $country,'required' => True,'class'=>'checkout-form-list','name'=>'country'
                                ]); ?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('Phone*',['required' => True,'class'=>'checkout-form-list','name'=>'phone']);?>
                            </div>
                            <div class="col-md-6">
                                <?= $this->Form->control('Email*',['required' => True,'class'=>'checkout-form-list','name'=>'email']);?>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-6 col-md-12 col-12">
                <div class="your-order">
                    <h3>Your order</h3>
                    <div class="your-order-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="product-name">Product</th>
                                <th class="product-total">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ( $shopping_cart_items as $shopping_cart_item):
                                //print_r($shopping_cart_item);

                                ?>
                                <tr class="cart_item">
                                    <td class="product-name">
                                        <?= $shopping_cart_item->title?><strong class="product-quantity"> × <?= $shopping_cart_item->amount?></strong>
                                    </td>
                                    <td class="product-total">
                                        <span class="amount"><?= $shopping_cart_item->price?></span>
                                    </td>
                                </tr>
                                <?= $this->Form->hidden('order_id',['value'=>$shopping_cart_item->orders_id]);?>
                            <?php endforeach?>
                            </tbody>
                            <tfoot>
                            <?php
                            foreach ( $subtotal as $subtotal):
                                ?>
                                <tr class="cart-subtotal">
                                    <th>Shipping Method</th>
                                    <td><span class="amount"><?= $subtotal->shippingMethod?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>Cart Subtotal</th>
                                    <td><span class="amount">$<?= number_format($subtotal->sum,2)?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>GST (Included)</th>
                                    <td><span class="amount">$<?= number_format($subtotal->tax,2)?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>Shipping Fee</th>
                                    <td><span class="amount">$<?= number_format($subtotal->shippingCost,2)?></span></td>
                                </tr>
                                <tr class="order-total">
                                    <th>Order Total</th>
                                    <td><strong><span class="amount">$<?= number_format($subtotal->total,2)?></span></strong>
                                    </td>
                                </tr>
                            <?php endforeach?>
                            </tfoot>
                        </table>
                    </div>
                    <?= $this->Form->button(__('Proceed to payment'),['id' => 'updatebtn']); ?>
                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<!-- checkout-area end -->

<!-- all js here -->
</body>
</html>
