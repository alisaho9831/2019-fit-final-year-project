<?php $this->layout = 'wholesale_default';?>
<!-- checkout-area start -->
<div class="checkout-area ptb-20">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Thank you for purchasing!</h2>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                    <div class="checkbox-form">
                        <h3>Shipping Address</h3>
                        <div class="row">
                            <div class="address">
                                <?php
                                foreach ($shippingaddress as $shipping){ ?>
                                    <h6><?= $shipping->address1?></h6>
                                    <h6><?= $shipping->address2?></h6>
                                    <h6><?= $shipping->city.' '.$shipping->state?></h6>
                                    <h6><?= $shipping->post_code?></h6>
                                    <h6><?= $shipping->country?></h6>
                                    <?php
                                } ?>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox-form">
                        <h3>Billing Address</h3>
                        <div class="row">
                            <div class="address">
                                <?php
                                foreach ($billingaddress as $billing){ ?>
                                    <h6><?= $billing->address1?></h6>
                                    <h6><?= $billing->address2?></h6>
                                    <h6><?= $billing->city.' '. $billing->state?></h6>
                                    <h6><?= $billing->post_code?></h6>
                                    <h6><?= $billing->country?></h6>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="col-lg-6 col-md-12 col-12">
                <div class="your-order">
                    <h3>Your order</h3>
                    <div class="your-order-table table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="product-name">Product</th>
                                <th class="product-total">Quantity</th>
                                <th class="product-total">Inner/Outer</th>
                                <th class="product-total">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ( $shopping_cart_items as $shopping_cart_item): ?>
                                <tr class="cart_item">
                                    <td class="product-name">
                                        <?= $shopping_cart_item->title?><strong class="product-quantity"> × <?= $shopping_cart_item->amount?></strong>
                                    </td>
                                    <td class="product-name">
                                        <strong class="product-quantity"><?= $shopping_cart_item->carton_amount?></strong>
                                    </td>
                                    <?php if ($shopping_cart_item['is_inner_carton'] == 1){?>
                                        <td> Inner</td>
                                    <?php }elseif ($shopping_cart_item['is_inner_carton'] == 0){?>
                                        <td> Outer</td>
                                    <?php }?>
                                    <td class="product-total">
                                        <span class="amount">$<?= number_format($shopping_cart_item->price,2)?></span>
                                    </td>
                                </tr>
                            <?php endforeach?>
                            </tbody>

                            <tfoot>
                            <?php
                            foreach ( $subtotal as $subtotal):
                                ?>
                                <tr class="cart-subtotal">
                                    <th>Cart Subtotal</th>
                                    <td><span class="amount">$<?= number_format($subtotal->sum,2)?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>GST (Included)</th>
                                    <td><span class="amount">$<?= number_format(($subtotal->sum-($subtotal->sum/1.1)),2)?></span></td>
                                </tr>
                                <tr class="order-total">
                                    <th>Order Total</th>
                                    <td><strong><span class="amount">$<?= number_format($subtotal->total,2)?></span></strong>
                                    </td>
                                </tr>
                            <?php endforeach?>
                            </tfoot>
                        </table>
                    </div>
                    <div class="payment-method">
                        <div class="payment-accordion">
                            <div class="order-button-payment">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout-area end -->

<!-- all js here -->
</body>
</html>
