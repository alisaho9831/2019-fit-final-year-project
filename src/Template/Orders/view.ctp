<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order $order
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Order'), ['action' => 'edit', $order->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Order'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Addresses'), ['controller' => 'Addresses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Address'), ['controller' => 'Addresses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Promo Codes'), ['controller' => 'PromoCodes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Promo Code'), ['controller' => 'PromoCodes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Order Lines'), ['controller' => 'Orderlines', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order Line'), ['controller' => 'Orderlines', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="orders view large-9 medium-8 columns content">
    <h3><?= h($order->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($order->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Financial Status') ?></th>
            <td><?= h($order->financial_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fulfillment Status') ?></th>
            <td><?= h($order->fulfillment_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount Code') ?></th>
            <td><?= h($order->discount_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Shipping Method') ?></th>
            <td><?= h($order->shipping_method) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= $order->has('address') ? $this->Html->link($order->address->id, ['controller' => 'Addresses', 'action' => 'view', $order->address->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Promo Code') ?></th>
            <td><?= $order->has('promo_code') ? $this->Html->link($order->promo_code->id, ['controller' => 'PromoCodes', 'action' => 'view', $order->promo_code->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Payment Method') ?></th>
            <td><?= h($order->payment_method) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Status') ?></th>
            <td><?= h($order->order_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tracking Number') ?></th>
            <td><?= h($order->tracking_number) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Shipping Company') ?></th>
            <td><?= h($order->shipping_company) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Notes') ?></th>
            <td><?= h($order->notes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Customer') ?></th>
            <td><?= $order->has('customer') ? $this->Html->link($order->customer->id, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($order->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Subtotal') ?></th>
            <td><?= $this->Number->format($order->subtotal) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Taxes Cost') ?></th>
            <td><?= $this->Number->format($order->taxes_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Shipping Cost') ?></th>
            <td><?= $this->Number->format($order->shipping_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Cost') ?></th>
            <td><?= $this->Number->format($order->total_cost) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount Amount') ?></th>
            <td><?= $this->Number->format($order->discount_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Shipping Address Id') ?></th>
            <td><?= $this->Number->format($order->shipping_address_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Paid Date') ?></th>
            <td><?= h($order->paid_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fulfillment Date') ?></th>
            <td><?= h($order->fulfillment_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Order Date') ?></th>
            <td><?= h($order->order_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deactivated') ?></th>
            <td><?= $order->is_deactivated ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Shopping Cart') ?></th>
            <td><?= $order->is_shopping_cart ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Order Lines') ?></h4>
        <?php if (!empty($order->order_lines)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Order Id') ?></th>
                <th scope="col"><?= __('Item Id') ?></th>
                <th scope="col"><?= __('Fulfillment Status') ?></th>
                <th scope="col"><?= __('Price') ?></th>
                <th scope="col"><?= __('Amount') ?></th>
                <th scope="col"><?= __('Expiry Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($order->order_lines as $orderLines): ?>
            <tr>
                <td><?= h($orderLines->order_id) ?></td>
                <td><?= h($orderLines->item_id) ?></td>
                <td><?= h($orderLines->fulfillment_status) ?></td>
                <td><?= h($orderLines->price) ?></td>
                <td><?= h($orderLines->amount) ?></td>
                <td><?= h($orderLines->expiry_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orderlines', 'action' => 'view', $orderLines->order_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orderlines', 'action' => 'edit', $orderLines->order_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orderlines', 'action' => 'delete', $orderLines->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderLines->order_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
