<?php $this->layout = 'wholesale_default';?>
<!-- shopping-cart-area start -->
<div class="cart-main-area pt-95 pb-30">
    <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'orders',
            'action' => 'wholesaleShippingaddress'
        ]
    ])?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h1 class="cart-heading">Cart <?php /*var_dump($email)*/?></h1>

                <form action="#">
                    <div class="table-content table-responsive">
                        <?= $this->Flash->render() ?>
                        <table>
                            <thead>
                            <tr>
                                <!--                                <th>Remove</th>-->
                                <!--                                <th>images</th>-->
                                <!--                                <th>Product</th>-->
                                <!--                                <th>Price</th>-->
                                <!--                                <th>Quantity</th>-->
                                <!--                                <th>Total</th>-->
                                <!--                                <th>Modify</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ( $shopping_cart_items as $shopping_cart_item):
                                //print_r($shopping_cart_item);

                                echo $this->form->hidden('order_id', ['value' => $shopping_cart_item->orders_id]);

                                ?>
                                <tr>
                                    <td><a class="animate-right" data-toggle="modal" data-target="#exampleModal-delete-<?= $shopping_cart_item->items_id?>" href="#">
                                            <b><?=$this->Html->tag("i",'', array('class' => 'pe-7s-close', 'style' => 'font-size: 2em')); ?></b>
                                        </a>
                                    </td>


                                    <td class="product-thumbnail">
                                        <?= $this->Html->image($shopping_cart_item->image, ['url' => ['controller' => 'Products', 'action' => 'normal_detail', '?' => ['product' => $shopping_cart_item->products_id]]]);?>
                                    </td>
                                    <td class="product-name"><?php echo $this->Html->link($shopping_cart_item->title, ['controller' => 'Products', 'action' => 'normal_detail', '?' => ['product' => $shopping_cart_item->products_id]]); ?></td>
                                    <td class="product-price-cart"><span class="amount">$<?= number_format($shopping_cart_item->price/$shopping_cart_item->carton_amount,2)?></span></td>
                                    <td class="product-quantity"><?= $shopping_cart_item->carton_amount?></td>
                                    <?php if ($shopping_cart_item)?>
                                    <td class="product-subtotal">$<?= number_format($shopping_cart_item->price,2)?></td>

                                    <?php if ($shopping_cart_item['is_inner_carton'] == 1){?>
                                        <td> Inner</td>
                                    <?php }elseif ($shopping_cart_item['is_inner_carton'] == 0){?>
                                        <td> Outer</td>
                                    <?php }?>
                                    <td><a class="animate-right" data-toggle="modal" data-target="#exampleModal-<?= $shopping_cart_item->items_id?>" href="#">
                                            EDIT
                                        </a>
                                    </td>

                                </tr>

                            <?php endforeach?>
                            </tbody>
                        </table>


                    </div>

                    <div class="row">
                        <div class="col-md-5 ml-auto">
                            <div class="cart-page-total">
                                <h2>Cart totals</h2>
                                <ul>
                                    <?php
                                    foreach ( $subtotal as $subtotal):
                                        ?>
                                        <li>Subtotal(GST included)<span>$<?= number_format($subtotal->sum,2)?></span></li>
                                        <li>GST<span>$<?= number_format($subtotal->sum-($subtotal->sum/1.1),2)?></span></li>
                                        <li>Total<span>$<?= number_format($subtotal->sum,2)?></span></li>
                                    <?php endforeach?>
                                </ul>
                                <?= $this->form->hidden('tax_cost', ['value' => $subtotal->sum-($subtotal->sum/1.1)])?>
                                <?=$this->Form->submit(__('Proceed to checkout'),['id' => 'updatebtn']);?>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<!-- shopping-cart-area end -->

<!--Edit model-->
<?php
foreach ($shopping_cart_items as $shopping_cart_item):

    ?>
    <?=$this->Form->create(null, [
    'url' => [
        'controller' => 'order_lines',
        'action' => 'updateCartWholesale'
    ]
])?>
    <div class="modal fade" id="exampleModal-<?= $shopping_cart_item->items_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="pe-7s-close" aria-hidden="true"></span>
        </button>
        <div class="modal-dialog modal-quickview-width" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="qwick-view-left">
                        <div class="quick-view-learg-img">
                            <div class="quick-view-tab-content tab-content">
                                <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                    <?= $this->Html->image($shopping_cart_item->image, ['alt' => 'CakePHP']) ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="qwick-view-right">
                        <div class="qwick-view-content">
                            <h5><?= $shopping_cart_item->title?></h5>
                            <div class="price">
                                <span class="new">$ <?= $shopping_cart_item->wholesale_price?></span>
                            </div>
                            <p><?= $shopping_cart_item->body?></p>
                            <?php echo $this->Form->radio('is_inner_carton', [['value' => 1, 'text' => 'Inner','class' => 'form-control'],['value' => 0, 'text' => 'Outer','class' => 'form-control']],['value'=>$shopping_cart_item->is_inner_carton,'onClick'=>'checkInner()']); ?>
                            <br>
                            <div class="quickview-plus-minus">
                                <?= $this->form->hidden('order_id', ['value' => $shopping_cart_item->orders_id]);?>
                                <?=$this->form->hidden('item_id',['value'=>$shopping_cart_item->items_id ]);?>
                                <?=$this->form->hidden('wholesale_price',['value'=>$shopping_cart_item->price/$shopping_cart_item->amount]);?>
                                <?=$this->form->hidden('oldAmount',['value'=>$shopping_cart_item->amount]);?>
                                <?=$this->form->hidden('inner_carton_size',['value'=>$shopping_cart_item->inner_carton_size]);?>
                                <?=$this->form->hidden('outer_carton_size',['value'=>$shopping_cart_item->outer_carton_size]);?>

                                <?php $defaultMax=(int)($shopping_cart_item->wholesale_quantity / $shopping_cart_item->inner_carton_size); ?>


                                <?= $this->Form->control(null,['id'=>"checkMinMax",'value'=>$shopping_cart_item->carton_amount, 'name'=>'carton_amount', 'type'=>'number', 'min'=>1, 'max'=>$defaultMax]); ?>

                                <div class="quickview-btn-cart">
                                    <?php
                                    echo $this->Form->button(__('Update'),['id' => 'updatebtn']);
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->Form->end() ?>
<?php endforeach ?>

<!--Delete Confirm Model-->
<?php
foreach ($shopping_cart_items as $shopping_cart_item):?>
    <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'order_lines',
            'action' => 'deleteItemWholesale'
        ]
    ])?>
    <div class="modal fade" id="exampleModal-delete-<?= $shopping_cart_item->items_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="pe-7s-close" aria-hidden="true"></span>
        </button>
        <div class="modal-dialog delete-modal-quickview-width" role="document">
            <div class="modal-content">
                <div class="remove-quick-view-content">
                    <h6>Do you want to remove the item from the cart?</h6><br>
                    <h4><?= $shopping_cart_item->title?></h4><br>
                    <?=$this->form->hidden('item_id',['value'=>$shopping_cart_item->items_id ]);?>
                    <?= $this->form->hidden('order_id',['value'=>$shopping_cart_item->orders_id]);?>
                    <?=$this->form->hidden('amount',['value'=>$shopping_cart_item->amount]);?>
                    <?=$this->Form->submit(__('Confirm'),['id' => 'confirmbtn']);?>

                </div>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
<?php endforeach ?>
<script type="text/javascript" >

    function checkInner(){


        var wholesale_quantity="<?php echo $shopping_cart_item['wholesale_quantity'] ?>";
        var inner_carton_size="<?php echo $shopping_cart_item['inner_carton_size'] ?>";
        var outer_carton_size="<?php echo $shopping_cart_item['outer_carton_size'] ?>";
        var input = document.getElementById("checkMinMax");
        var isInner=document.querySelector('input[name="is_inner_carton"]:checked').value;

        var maxValue=wholesale_quantity / inner_carton_size;
        if(isInner==1){
            maxValue=Math.floor(wholesale_quantity / inner_carton_size);
        }
        else{
            maxValue=Math.floor(wholesale_quantity / outer_carton_size)
        }
        input.setAttribute("max",maxValue); // set a new value;
    }
    </script>
