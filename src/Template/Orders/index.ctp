<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Order[]|\Cake\Collection\CollectionInterface $orders
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Addresses'), ['controller' => 'Addresses', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Address'), ['controller' => 'Addresses', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Promo Codes'), ['controller' => 'PromoCodes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Promo Code'), ['controller' => 'PromoCodes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Order Lines'), ['controller' => 'Orderlines', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Order Line'), ['controller' => 'Orderlines', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="orders index large-9 medium-8 columns content">
    <h3><?= __('Orders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('financial_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paid_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fulfillment_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fulfillment_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('subtotal') ?></th>
                <th scope="col"><?= $this->Paginator->sort('taxes_cost') ?></th>
                <th scope="col"><?= $this->Paginator->sort('shipping_cost') ?></th>
                <th scope="col"><?= $this->Paginator->sort('total_cost') ?></th>
                <th scope="col"><?= $this->Paginator->sort('discount_code') ?></th>
                <th scope="col"><?= $this->Paginator->sort('discount_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('shipping_method') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('shipping_address_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('billing_address_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('promo_code_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('payment_method') ?></th>
                <th scope="col"><?= $this->Paginator->sort('order_status') ?></th>
                <th scope="col"><?= $this->Paginator->sort('tracking_number') ?></th>
                <th scope="col"><?= $this->Paginator->sort('shipping_company') ?></th>
                <th scope="col"><?= $this->Paginator->sort('notes') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_deactivated') ?></th>
                <th scope="col"><?= $this->Paginator->sort('customer_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_shopping_cart') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($orders as $order): ?>
            <tr>
                <td><?= $this->Number->format($order->id) ?></td>
                <td><?= h($order->email) ?></td>
                <td><?= h($order->financial_status) ?></td>
                <td><?= h($order->paid_date) ?></td>
                <td><?= h($order->fulfillment_status) ?></td>
                <td><?= h($order->fulfillment_date) ?></td>
                <td><?= $this->Number->format($order->subtotal) ?></td>
                <td><?= $this->Number->format($order->taxes_cost) ?></td>
                <td><?= $this->Number->format($order->shipping_cost) ?></td>
                <td><?= $this->Number->format($order->total_cost) ?></td>
                <td><?= h($order->discount_code) ?></td>
                <td><?= $this->Number->format($order->discount_amount) ?></td>
                <td><?= h($order->shipping_method) ?></td>
                <td><?= h($order->order_date) ?></td>
                <td><?= $this->Number->format($order->shipping_address_id) ?></td>
                <td><?= $order->has('address') ? $this->Html->link($order->address->id, ['controller' => 'Addresses', 'action' => 'view', $order->address->id]) : '' ?></td>
                <td><?= $order->has('promo_code') ? $this->Html->link($order->promo_code->id, ['controller' => 'PromoCodes', 'action' => 'view', $order->promo_code->id]) : '' ?></td>
                <td><?= h($order->payment_method) ?></td>
                <td><?= h($order->order_status) ?></td>
                <td><?= h($order->tracking_number) ?></td>
                <td><?= h($order->shipping_company) ?></td>
                <td><?= h($order->notes) ?></td>
                <td><?= h($order->is_deactivated) ?></td>
                <td><?= $order->has('customer') ? $this->Html->link($order->customer->id, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></td>
                <td><?= h($order->is_shopping_cart) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $order->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $order->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $order->id], ['confirm' => __('Are you sure you want to delete # {0}?', $order->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
