<!-- shopping-cart-area start -->
<div class="cart-main-area pt-95 pb-30">
    <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'orders',
            'action' => 'addShippingMethod'
        ]
    ])?>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <h1 class="cart-heading">Cart <?php /*var_dump($email)*/?></h1>

                <form action="#">
                    <div class="table-content table-responsive">
                        <?= $this->Flash->render() ?>
                        <table>
                            <thead>
                            <tr>
    <!--                                <th>Remove</th>-->
    <!--                                <th>images</th>-->
    <!--                                <th>Product</th>-->
    <!--                                <th>Price</th>-->
    <!--                                <th>Quantity</th>-->
    <!--                                <th>Total</th>-->
    <!--                                <th>Modify</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                                $totalWeight = 0;
                                foreach ( $shopping_cart_items as $shopping_cart_item):
                                   //print_r($shopping_cart_item);

                                    echo $this->form->hidden('order_id', ['value' => $shopping_cart_item->orders_id]);

                                    $totalWeight += $shopping_cart_item->weight*$shopping_cart_item->amount;



                            ?>
                            <tr>
                                <td><a class="animate-right" data-toggle="modal" data-target="#exampleModal-delete-<?= $shopping_cart_item->items_id?>" href="#">
                                            <b><?=$this->Html->tag("i",'', array('class' => 'pe-7s-close', 'style' => 'font-size: 2em')); ?></b>
                                    </a>
                                </td>


                                <td class="product-thumbnail">
                                    <?= $this->Html->image($shopping_cart_item->image, ['url' => ['controller' => 'Products', 'action' => 'normal_detail', '?' => ['product' => $shopping_cart_item->products_id]]]);?>
                                </td>
                                <td class="product-name"><?php echo $this->Html->link($shopping_cart_item->title, ['controller' => 'Products', 'action' => 'normal_detail', '?' => ['product' => $shopping_cart_item->products_id]]); ?></td>
                                <td class="product-price-cart"><span class="amount">$<?= $shopping_cart_item->price/$shopping_cart_item->amount?></span></td>
                                <td class="product-quantity"><?= $shopping_cart_item->amount?></td>
                                <td class="product-subtotal">$<?= $shopping_cart_item->price?></td>
                                <td><a class="animate-right" data-toggle="modal" data-target="#exampleModal-<?= $shopping_cart_item->items_id?>" href="#">
                                        EDIT
                                    </a>
                                </td>

                            </tr>

                            <?php endforeach?>

                            </tbody>
                        </table>


                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="coupon-all">
                                <!--<div class="coupon">
                                    <input id="coupon_code" class="input-text" name="coupon_code" value="" placeholder="Coupon code" type="text">
                                    <input class="button" name="apply_coupon" value="Apply coupon" type="submit">
                                </div>-->


                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="chooseShipping">
                            <h5>Postage Calculation</h5><br>
                            <p>Please enter your postcode</p>
                            <style>
                                #metro-shipping label {
                                    display: block;
                                }

                                #regional-shipping label {
                                    display: block;
                                }
                            </style>
                            <?=$this->form->control('',['class'=>'input-text', 'name'=> 'postcode','id'=>'postcode', 'placeholder'=>'Enter Your Postcode' ]);?><br>
                            <div id="metro-shipping">
                                <?php echo $this->Form->radio(
                                    'metroShipping',
                                    [
                                        ['value' => 'StandardMetro', 'text' => 'Standard Metro $6.95'],
                                        ['value' => 'ExpressMetro', 'text' => 'Express Metro $12.95 (<500g), $29.95 (>500g)'],
                                    ],
                                    [
                                        'legend'=>false, 'separator'=>'<br/>', 'checked'=>false
                                    ]
                                );?>
                            </div>
                            <div id="regional-shipping">
                                <?php echo $this->Form->radio(
                                    'regionalShipping',
                                    [
                                        ['value' => 'StandardRegional', 'text' => 'Standard Regional $9.95 (<500g), $19.95 (>500g)'],
                                        ['value' => 'ExpressRegional', 'text' => 'Express Regional $19.95 (<500g), $29.95 (>500g)'],
                                    ],
                                    [
                                        'legend'=>false, 'separator'=>'<br/>', 'checked'=>false
                                    ]
                                );?>

                            </div>
                            <div id="postcodeError">
                                <p>Please enter a valid postcode</p>
                            </div>
                        </div>
                        <div class="col-md-5 ml-auto">

                            <div class="cart-page-total">
                                <ul>
                                    <?php
                                    $standard_metro_shipping_fee = 6.95;
                                    if($totalWeight <= 500){
                                        $express_metro_shipping_fee = 12.95;
                                        $standard_regional_shipping_fee = 9.95;
                                        $express_regional_shipping_fee = 19.95;
                                    }elseif ($totalWeight >= 500){
                                        $express_metro_shipping_fee = 29.95;
                                        $standard_regional_shipping_fee = 19.95;
                                        $express_regional_shipping_fee =29.95;
                                    }
                                        foreach ( $subtotal as $subtotal):
                                    ?>
                                    <li>Subtotal(GST included)<span>$<?= number_format($subtotal->sum,2)?></span></li>
                                    <li>GST<span>$<?= number_format($subtotal->sum-($subtotal->sum/1.1),2)?></span></li>
                                    <li id="SM">Shipping Fee<span>$<?= number_format($standard_metro_shipping_fee,2)?></span></li>
                                    <li id="EM">Shipping Fee<span>$<?= number_format($express_metro_shipping_fee,2)?></span></li>
                                    <li id="SR">Shipping Fee<span>$<?= number_format($standard_regional_shipping_fee,2)?></span></li>
                                    <li id="ER">Shipping Fee<span>$<?= number_format($express_regional_shipping_fee,2)?></span></li>
                                    <li id="TSM">Total<span>$<?= number_format($subtotal->sum+$standard_metro_shipping_fee,2)?></span></li>
                                    <li id="TEM">Total<span>$<?= number_format($subtotal->sum+$express_metro_shipping_fee,2)?></span></li>
                                    <li id="TSR">Total<span>$<?= number_format($subtotal->sum+$standard_regional_shipping_fee,2)?></span></li>
                                    <li id="TER">Total<span>$<?= number_format($subtotal->sum+$express_regional_shipping_fee,2)?></span></li>
                                    <?php endforeach?>
                                </ul>

                                <?= $this->form->hidden('tax_cost', ['value' => $subtotal->sum-($subtotal->sum/1.1)])?>
                                <div id="checkoutbtn">
                                    <p id="warning">Please choose your shipping method before proceeding.</p>
                                    <?=$this->Form->submit(__('Proceed to checkout'),['class' => 'checkoutbtn','id'=> 'proceed']);?>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<!-- shopping-cart-area end -->

<!--Edit model-->
<?php
foreach ($shopping_cart_items as $shopping_cart_item):

        ?>
        <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'order_lines',
            'action' => 'updateCart'
        ]
    ])?>
        <div class="modal fade" id="exampleModal-<?= $shopping_cart_item->items_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="pe-7s-close" aria-hidden="true"></span>
            </button>
            <div class="modal-dialog modal-quickview-width" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="qwick-view-left">
                            <div class="quick-view-learg-img">
                                <div class="quick-view-tab-content tab-content">
                                    <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                        <?= $this->Html->image($shopping_cart_item->image, ['alt' => 'CakePHP']) ?>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="qwick-view-right">
                            <div class="qwick-view-content">
                                <h5><?= $shopping_cart_item->title?></h5>
                                <div class="price">
                                    <span class="new">$ <?= $shopping_cart_item->retail_price?></span>
                                </div>
                                <p><?= $shopping_cart_item->body?></p>
                                <div class="quickview-plus-minus">
                                        <?= $this->form->hidden('order_id', ['value' => $shopping_cart_item->orders_id]);?>
                                        <?=$this->form->hidden('item_id',['value'=>$shopping_cart_item->items_id ]);?>
                                        <?=$this->form->hidden('retail_price',['value'=>$shopping_cart_item->price/$shopping_cart_item->amount]);?>
                                        <?=$this->form->hidden('oldAmount',['value'=>$shopping_cart_item->amount]);?>
                                        <?= $this->Form->control(null,['value'=>$shopping_cart_item->amount, 'name'=>'amount', 'type'=>'number', 'min'=>1, 'max'=>$shopping_cart_item->retail_quantity]); ?>

                                    <div class="quickview-btn-cart">
                                        <?php
                                                echo $this->Form->button(__('Update'),['id' => 'updatebtn']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?= $this->Form->end() ?>
<?php endforeach ?>

<!--Delete Confirm Model-->
<?php
    foreach ($shopping_cart_items as $shopping_cart_item):?>
       <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'order_lines',
            'action' => 'deleteItem'
        ]
    ])?>
        <div class="modal fade" id="exampleModal-delete-<?= $shopping_cart_item->items_id ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="pe-7s-close" aria-hidden="true"></span>
            </button>
            <div class="modal-dialog delete-modal-quickview-width" role="document">
                <div class="modal-content">
                    <div class="remove-quick-view-content">
                        <h6>Do you want to remove the item from the cart?</h6><br>
                        <h4><?= $shopping_cart_item->title?></h4><br>
                        <?=$this->form->hidden('item_id',['value'=>$shopping_cart_item->items_id ]);?>
                        <?= $this->form->hidden('order_id',['value'=>$shopping_cart_item->orders_id]);?>
                        <?=$this->form->hidden('amount',['value'=>$shopping_cart_item->amount]);?>
                        <?=$this->Form->submit(__('Confirm'),['id' => 'confirmbtn']);?>
                    </div>
                </div>
            </div>
        </div>
    <?= $this->Form->end() ?>
<?php endforeach ?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    $(document).ready(function(){
        $("#metro-shipping").hide();
        $("#regional-shipping").hide();
        $("#postcodeError").hide();
        $("#warning").show();
        $("#SM").hide();
        $("#EM").hide();
        $("#SR").hide();
        $("#ER").hide();
        $("#TSM").hide();
        $("#TEM").hide();
        $("#TSR").hide();
        $("#TER").hide();
        document.getElementById("proceed").disabled = true;
        $("#postcode").on("input", function(){
            if(
                $(this).val()>=2600&&$(this).val()<=2610 ||
                $(this).val()>=1100&&$(this).val()<=1299 || $(this).val()>=2000&&$(this).val()<=2001 || $(this).val()==2007 || $(this).val()==2009 ||
                $(this).val()>=2000&&$(this).val()<=2234 ||
                $(this).val()>=3000&&$(this).val()<=3207 || $(this).val()>=8000&&$(this).val()<=8399 ||
                $(this).val()>=4000&&$(this).val()<=4207 || $(this).val()>=4300&&$(this).val()<=4305 || $(this).val()>=4500&&$(this).val()<=4519 || $(this).val()>=9000&&$(this).val()<=9015 ||
                $(this).val()>=5000&&$(this).val()<=5199 || $(this).val()>=5880&&$(this).val()<=5889 || $(this).val()==5810 || $(this).val()==5839 ||
                $(this).val()>=6000&&$(this).val()<=6199 || $(this).val()>=6830&&$(this).val()<=6832 || $(this).val()>=6837&&$(this).val()<=6849 || $(this).val()==6827 ||
                $(this).val()>=7000&&$(this).val()<=7099 ||
                $(this).val()=='0800'|| $(this).val()=='0801'|| $(this).val()=='0802'|| $(this).val()=='0803'|| $(this).val()=='0804'||
                $(this).val()=='0805'|| $(this).val()=='0806'|| $(this).val()=='0807'|| $(this).val()=='0808'|| $(this).val()=='0809'||
                $(this).val()=='0810'|| $(this).val()=='0811'|| $(this).val()=='0812'|| $(this).val()=='0813'|| $(this).val()=='0814'||
                $(this).val()=='0815'|| $(this).val()=='0816'|| $(this).val()=='0817'|| $(this).val()=='0818'|| $(this).val()=='0819'||
                $(this).val()=='0820'|| $(this).val()=='0821'|| $(this).val()=='0822'|| $(this).val()=='0823'|| $(this).val()=='0824'||
                $(this).val()=='0825'|| $(this).val()=='0826'|| $(this).val()=='0827'|| $(this).val()=='0828'|| $(this).val()=='0829'||
                $(this).val()=='0830'|| $(this).val()=='0831'|| $(this).val()=='0832'
            ){
                $("#metro-shipping").show();
                $("#regional-shipping").hide();
                $("#postcodeError").hide();
                $('input[type="radio"]').click(function() {
                    if($(this).attr('value') == 'StandardMetro') {
                        $("#SM").show();
                        $("#EM").hide();
                        $("#SR").hide();
                        $("#ER").hide();
                        $("#TSM").show();
                        $("#TEM").hide();
                        $("#TSR").hide();
                        $("#TER").hide();
                        $("#warning").hide();
                        document.getElementById("proceed").disabled = false;
                    }
                    else {
                        $("#SM").hide();
                        $("#EM").show();
                        $("#SR").hide();
                        $("#ER").hide();
                        $("#TSM").hide();
                        $("#TEM").show();
                        $("#TSR").hide();
                        $("#TER").hide();
                        $("#warning").hide();
                        document.getElementById("proceed").disabled = false;
                    }

                });
            }else if(
                $(this).val()>=2611&&$(this).val()<=2620 ||
                $(this).val()>=2235&&$(this).val()<=2999 ||
                $(this).val()>=2640&&$(this).val()<=2660 ||
                $(this).val()>=2500&&$(this).val()<=2534 ||
                $(this).val()>=2265&&$(this).val()<=2333 ||
                $(this).val()>=2413&&$(this).val()<=2484 || $(this).val()>=2460&&$(this).val()<=2465 || $(this).val()==2450 ||
                $(this).val()>=3208&&$(this).val()<=3999 ||
                $(this).val()>=4208&&$(this).val()<=4299 || $(this).val()>=4306&&$(this).val()<=4499 || $(this).val()>=4520&&$(this).val()<=4999 ||
                $(this).val()>=4550&&$(this).val()<=4575 ||
                $(this).val()>=5200&&$(this).val()<=5749 || $(this).val()>=5825&&$(this).val()<=5854 ||
                $(this).val()>=6200&&$(this).val()<=6999 ||
                $(this).val()>=7100&&$(this).val()<=7999 ||
                $(this).val()=='0833'|| $(this).val()=='0834'|| $(this).val()=='0835'|| $(this).val()=='0836'|| $(this).val()=='0837'||
                $(this).val()=='0838'|| $(this).val()=='0839'|| $(this).val()=='0840'|| $(this).val()=='0841'|| $(this).val()=='0842'||
                $(this).val()=='0843'|| $(this).val()=='0844'|| $(this).val()=='0845'|| $(this).val()=='0846'|| $(this).val()=='0847'||
                $(this).val()=='0848'|| $(this).val()=='0849'|| $(this).val()=='0850'|| $(this).val()=='0851'|| $(this).val()=='0852'||
                $(this).val()=='0853'|| $(this).val()=='0854'|| $(this).val()=='0855'|| $(this).val()=='0856'|| $(this).val()=='0857'||
                $(this).val()=='0858'|| $(this).val()=='0859'|| $(this).val()=='0860'|| $(this).val()=='0861'|| $(this).val()=='0862'||
                $(this).val()=='0863'|| $(this).val()=='0864'|| $(this).val()=='0865'|| $(this).val()=='0866'|| $(this).val()=='0867'||
                $(this).val()=='0868'|| $(this).val()=='0869'|| $(this).val()=='0870'|| $(this).val()=='0871'|| $(this).val()=='0872'||
                $(this).val()=='0873'|| $(this).val()=='0874'|| $(this).val()=='0875'|| $(this).val()=='0876'|| $(this).val()=='0877'||
                $(this).val()=='0878'|| $(this).val()=='0879'|| $(this).val()=='0880'|| $(this).val()=='0881'|| $(this).val()=='0882'||
                $(this).val()=='0883'|| $(this).val()=='0884'|| $(this).val()=='0885'|| $(this).val()=='0886'|| $(this).val()=='0887'||
                $(this).val()=='0888'|| $(this).val()=='0889'|| $(this).val()=='0890'|| $(this).val()=='0891'|| $(this).val()=='0892'||
                $(this).val()=='0893'|| $(this).val()=='0894'|| $(this).val()=='0895'|| $(this).val()=='0896'|| $(this).val()=='0897'||
                $(this).val()=='0898'|| $(this).val()=='0899'
            ){
                $("#regional-shipping").show();
                $("#metro-shipping").hide();
                $("#postcodeError").hide();
                $('input[type="radio"]').click(function() {
                    if($(this).attr('value') == 'StandardRegional') {
                        $("#SM").hide();
                        $("#EM").hide();
                        $("#SR").show();
                        $("#ER").hide();
                        $("#TSM").hide();
                        $("#TEM").hide();
                        $("#TSR").show();
                        $("#TER").hide();
                        $("#warning").hide();
                        document.getElementById("proceed").disabled = false;
                    }
                    else {
                        $("#SM").hide();
                        $("#EM").hide();
                        $("#SR").hide();
                        $("#ER").show();
                        $("#TSM").hide();
                        $("#TEM").hide();
                        $("#TSR").hide();
                        $("#TER").show();
                        $("#warning").hide();
                        document.getElementById("proceed").disabled = false;
                    }

                });
            }
            else {
                $("#metro-shipping").hide();
                $("#regional-shipping").hide();
                $("#warning").show();
                $("#SM").hide();
                $("#EM").hide();
                $("#SR").hide();
                $("#ER").hide();
                $("#TSM").hide();
                $("#TEM").hide();
                $("#TSR").hide();
                $("#TER").hide();
                $("#postcodeError").show();
                document.getElementById("proceed").disabled = true;
            }
        });
    });
</script>
