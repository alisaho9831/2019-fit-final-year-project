<?php $this->layout = 'default2';?>
<!-- checkout-area start -->
<div class="checkout-area ptb-20">
    <?=$this->Form->create(null, [
        'url' => [
            'controller' => 'Orderlines',
            'action' => 'checkout'
        ]
    ])?>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Shipping</h2><br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-12 col-12">
                <div class="checkbox-form">
                    <h3>Shipping Address</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $this->Form->control('First Name*',['required' => True,'class'=>'checkout-form-list','name'=>'first_name']);?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('Last Name*',['required' => True,'class'=>'checkout-form-list','name'=>'last_name']);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('Address*',['required' => True,'class'=>'checkout-form-list','name'=>'address1','placeholder'=>'Street address']);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('',['class'=>'checkout-form-list','name'=>'address2','placeholder'=>'Apartment, suite, unit etc. (optional)']);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('City*',['required' => True,'class'=>'checkout-form-list','name'=>'city']);?>
                        </div>
                        <div class="col-md-6">
                            <!-- State Select -->
                            <?= $this->Form->control('State/Territory*', [
                                'options' => $states, 'required' => True,'class'=>'checkout-form-list','name'=>'state'
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?php
                            foreach ($postcode as $addresspostcode){
                                $postcode = [
                                    $addresspostcode->postcode => $addresspostcode->postcode
                                ];
                            }
                            ?>

                            <?= $this->Form->control('Postcode*', [
                                'options' => $postcode,'required' => True,'class'=>'checkout-form-list','name'=>'postcode'
                            ]); ?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('Country*', [
                                'options' => $country,'required' => True,'class'=>'checkout-form-list','name'=>'country'
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('Phone*',['required' => True,'class'=>'checkout-form-list','name'=>'phone','pattern'=>'[0-9]{10}','type'=>"text"]);?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('Email*',['required' => True,'class'=>'checkout-form-list','name'=>'email','type'=>'email']);?>
                        </div>
                    </div>
                    <div class="order-notes">
                        <?= $this->Form->control('Order Notes',['type'=>'textarea','class'=>'checkout-form-list mrg-nn','name'=>'notes','cols'=>30, 'rows'=>10, 'placeholder'=>'Notes about your order, e.g. special notes for delivery.']);?>
                    </div>
                    <?=$this->Form->control('sameForBillingAddress', array('id'=>'sameBillingAddress', 'div' => 'form-group', 'class' => 'form-control', 'type' => 'checkbox','checked'=>true, 'label' => array('text' => ' Use the same address as billing address', 'class' => 'label-checkbox'), 'format' => array('input','label')));;?>
                </div>

            </div>

            <div class="col-lg-6 col-md-12 col-12">
                <div class="your-order">
                    <div class="your-order-table table-responsive">
                        <h3>Your order</h3>
                        <table>
                            <thead>
                            <tr>
                                <th class="product-name">Product</th>
                                <th class="product-total">Quantity</th>
                                <th class="product-total">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $totalWeight = 0;
                            foreach ( $shopping_cart_items as $shopping_cart_item):
                                //print_r($shopping_cart_item);

                                $shipping_method = $shopping_cart_item->shipping_method;
                                $totalWeight += $shopping_cart_item->weight*$shopping_cart_item->amount;
                                $gst = $shopping_cart_item->tax;

                                ?>
                                <tr class="cart_item">
                                    <td class="product-name">
                                        <?= $shopping_cart_item->title?>
                                    </td>
                                    <td class="product-name">
                                        <strong class="product-quantity"><?= $shopping_cart_item->amount?></strong>
                                    </td>
                                    <td class="product-total">
                                        <span class="amount"><?= number_format($shopping_cart_item->price,2)?></span>
                                    </td>
                                </tr>
                                <?= $this->Form->hidden('order_id',['value'=>$shopping_cart_item->orders_id]);?>
                            <?php endforeach?>
                            </tbody>
                            <tfoot>
                            <tr class="cart-subtotal">
                                <th>Shipping Method</th>
                                <td>              </td>
                                <td><span class="amount"><?= $shipping_method?></span></td>
                            </tr>
                            <tr class="cart-subtotal">
                                <th>Total Weight</th>
                                <td>              </td>
                                <td><span class="amount"><?= number_format($totalWeight/1000,2)?>KG</span></td>
                            </tr>
                            <?php
                            foreach ( $subtotal as $subtotal):
                                ?>
                                <tr class="cart-subtotal">
                                    <th>Shipping Fee</th>
                                    <?php
                                    if($shipping_method=='Standard Metro'){
                                        $shipping_fee = 6.95;
                                    }elseif ($shipping_method=='Express Metro'){
                                        if($totalWeight <= 500){
                                            $shipping_fee = 12.95;
                                        }elseif ($totalWeight >= 500){
                                            $shipping_fee = 29.95;
                                        }
                                    }elseif ($shipping_method=='Standard Regional'){
                                        if($totalWeight <= 500){
                                            $shipping_fee = 9.95;
                                        }elseif ($totalWeight >= 500){
                                            $shipping_fee = 19.95;
                                        }
                                    }elseif ($shipping_method=='Express Regional'){
                                        if($totalWeight <= 500){
                                            $shipping_fee = 19.95;
                                        }elseif ($totalWeight >= 500){
                                            $shipping_fee =29.95;
                                        }
                                    }

                                    $total = $shipping_fee+$subtotal->sum;
                                    ?>
                                    <td>              </td>
                                    <td><span class="amount">$<?= number_format($shipping_fee,2)?></span></td>
                                </tr>
                                <tr class="cart-subtotal">
                                    <th>Cart Subtotal</th>
                                    <td>              </td>
                                    <td><span class="amount">$<?= number_format($subtotal->sum,2)?></span></td>
                                </tr>

                                <tr class="cart-subtotal">
                                    <th>GST Included</th>
                                    <td>              </td>
                                    <td><span class="amount">$<?= number_format($gst,2)?></span></td>
                                </tr>
                                <tr class="order-total">
                                    <th>Order Total</th>
                                    <td>              </td>
                                    <td><strong><span class="amount">$<?= number_format($total,2)?></span></strong>
                                    </td>
                                </tr>
                            <?php endforeach?>
                            </tfoot>
                        </table>
                    </div>
                    <?= $this->form->hidden('shipping_cost', ['value' => $shipping_fee])?>
                    <?= $this->form->hidden('total_cost', ['value' => $total])?>
                    <?= $this->Html->link( 'Back', ['controller' => 'orders', 'action' => 'cart']) ?>
                    <?= $this->Form->button(__('Proceed to Payment'),['id' => 'updatebtn']); ?>
                </div>
            </div>
        </div>
        <div id="billingAddress">
            <div class="col-lg-6 col-md-12 col-12">
                <div class="checkbox-form">
                    <h3>Billing Address</h3>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $this->Form->control('First Name*',['class'=>'checkout-form-list','name'=>'b_first_name','id'=>'b_first_name']);?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('Last Name*',['class'=>'checkout-form-list','name'=>'b_last_name','id'=>'b_last_name']);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('Address*',['class'=>'checkout-form-list','name'=>'b_address1','placeholder'=>'Street address','id'=>'b_address1']);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('',['class'=>'checkout-form-list','name'=>'b_address2','placeholder'=>'Apartment, suite, unit etc. (optional)']);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('City*',['class'=>'checkout-form-list','name'=>'b_city','id'=>'b_city']);?>
                        </div>
                        <div class="col-md-6">
                            <!-- State Select -->
                            <?= $this->Form->control('State/Territory*', [
                                'options' => $states, 'class'=>'checkout-form-list','name'=>'b_state','id'=>'b_state'
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('Postcode*',['class'=>'checkout-form-list','name'=>'b_postcode','id'=>'b_postcode','pattern'=>'[0-9]{4}','type'=>"text"]);?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->control('Country*', [
                                'options' => $country,'class'=>'checkout-form-list','name'=>'b_country','id'=>'b_country'
                            ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->control('Email*',['class'=>'checkout-form-list','name'=>'b_email','type'=>'email','id'=>'b_email']);?>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <?= $this->Form->end() ?>
</div>
<!-- checkout-area end -->




<!-- all js here -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    $(document).ready(function(){
        $("#billingAddress").hide();
        $('#sameBillingAddress').on('click', function () {
            if ($(this).prop('checked')) {
                $("#billingAddress").hide();
                $("#b_first_name").prop('required',false);
                $("#b_last_name").prop('required',false);
                $("#b_address1").prop('required',false);
                $("#b_city").prop('required',false);
                $("#b_state").prop('required',false);
                $("#b_postcode").prop('required',false);
                $("#b_country").prop('required',false);
                $("#b_email").prop('required',false);
            } else {
                $("#billingAddress").show();
                $("#b_first_name").prop('required',true);
                $("#b_last_name").prop('required',true);
                $("#b_address1").prop('required',true);
                $("#b_city").prop('required',true);
                $("#b_state").prop('required',true);
                $("#b_postcode").prop('required',true);
                $("#b_country").prop('required',true);
                $("#b_email").prop('required',true);
            }
        });
    });
</script>
</body>
</html>
