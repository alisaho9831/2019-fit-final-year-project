
<?php $this->layout = 'wholesale_default';?>
<?php
$this->assign('title', 'Smooth Sales Wholesale');
?>

<div class="pl-20 pr-20 overflow clearfix">
    <div class="categori-menu-slider-wrapper clearfix">
        <div class="categories-menu ">
            <div class="category-menu-list">
                <ul>
                    <?php

                    foreach ($mainMenu as $mainMenuItems):
                        $id = $mainMenuItems->id;
                        ?>
                        <li>
                            <?= $this->Html->link( $mainMenuItems->title, ['controller' => 'products', 'action' => 'wholesale_listing', $mainMenuItems->id]) ?>
                            <div class="category-menu-dropdown">
                                <div class="category-dropdown-style category-common4 ">
                                    <h4 class="categories-subtitle"><?= $mainMenuItems->title ?></h4>

                                    <ul>
                                        <?php foreach ($submenu as $submenuItems):
                                            $child_id = $submenuItems->parent_collection;
                                            if($id==$child_id){
                                                ?>
                                                <li>
                                                    <?= $this->Html->link( $submenuItems->title, ['controller' => 'products', 'action' => 'wholesale_listing', $submenuItems->id]) ?>
                                                </li>
                                                <?php
                                            }
                                            ?>
                                        <?php endforeach ?>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>

        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?= $this->Html->image('slider/Slider4.jpg', ['url' => ['controller' => 'Products', 'action' => 'wholesale_listing',24]], ['class' => 'd-block w-100','alt' => 'First slide']) ?>
                </div>
                <div class="carousel-item">
                    <?= $this->Html->image('slider/Slider5.jpg', ['url' => ['controller' => 'Products', 'action' => 'wholesale_listing',8]], ['class' => 'd-block w-100','alt' => 'Second slide']) ?>
                </div>
                <div class="carousel-item">
                    <?= $this->Html->image('slider/Slider6.jpg', ['url' => ['controller' => 'Products', 'action' => 'wholesale_listing',22]], ['class' => 'd-block w-100','alt' => 'Third slide']) ?>
                </div>
            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<br/><br/>

