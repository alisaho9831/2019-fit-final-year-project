
<?php
$this->assign('title', 'Smooth Sales');

?>

<div class="pl-20 pr-20 overflow clearfix">
    <div class="categori-menu-slider-wrapper clearfix">
        <div class="categories-menu ">
            <div class="category-menu-list">
                <ul>
                    <?php

                    foreach ($mainMenu as $mainMenuItems):
                        $id = $mainMenuItems->id;
                    ?>
                    <li>
                        <?= $this->Html->link( $mainMenuItems->title, ['controller' => 'products', 'action' => 'listing', $mainMenuItems->id]) ?>
                        <div class="category-menu-dropdown">
                            <div class="category-dropdown-style category-common4 ">
                                <h4 class="categories-subtitle"><?= $mainMenuItems->title ?></h4>

                                <ul>
                                    <?php foreach ($submenu as $submenuItems):
                                        $child_id = $submenuItems->parent_collection;
                                        if($id==$child_id){
                                        ?>
                                            <li>
                                                <?= $this->Html->link( $submenuItems->title, ['controller' => 'products', 'action' => 'listing', $submenuItems->id]) ?>
                                            </li>
                                    <?php
                                        }
                                            ?>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>

        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>

            <div class="carousel-inner">
                <div class="carousel-item active">
                    <?= $this->Html->image('slider/Slider7.jpg', ['url' => ['controller' => 'Products', 'action' => 'listing',24]], ['class' => 'd-block w-100','alt' => 'First slide']) ?>
                </div>
                <div class="carousel-item">
                    <?= $this->Html->image('slider/Slider8.jpg', ['url' => ['controller' => 'Products', 'action' => 'listing',8]], ['class' => 'd-block w-100','alt' => 'Second slide']) ?>
                </div>
                <div class="carousel-item">
                    <?= $this->Html->image('slider/Slider9.jpg', ['url' => ['controller' => 'Products', 'action' => 'listing',22]], ['class' => 'd-block w-100','alt' => 'Third slide']) ?>
                </div>
            </div>

            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>

            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</div>

<!-- banner area start -->
<div class="banner-area pt-600 pb-60" >
    <div class="container">
        <div class="smoothScrolling">
            <a name="hotDeals">
            <h2>Hot Deals </h2> </a> <br>
        </div>

        <div class="row">
            <?php

            foreach ($flashsales as $flashsale):
            ?>
            <div class="col-md-6">
                <div class="book-banner-wrapper mr-20">

                    <?= $this->Html->image($flashsale->image, ['url' => ['controller' => 'Products', 'action' => 'listing',$flashsale->collection_id]]) ?>
                    <h4><center><?php echo $this->Html->link($flashsale->description,['controller'=>'Products','action'=>'listing', $flashsale->collection_id]);?></center></h4>
                    <div class="hmCountdown">
                        <h6 id="countdown_<?= $flashsale->id?>"></h6>

                    </div>




                </div>
            </div>
            <?php endforeach?>
        </div>
    </div>
</div>

<!-- banner area end -->
<script>
    function createCountDown(elementId, date) {
        // Set the date we're counting down to
        var countDownDate = new Date(date).getTime();

        // Update the count down every 1 second
        var x = setInterval(function() {

            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now an the count down date
            var distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);

            // Display the result in the element with id="demo"
            document.getElementById(elementId).innerHTML = "End in "+days + "Days ";

        }, 1000);
    }
    <?php foreach ($flashsales as $flashsale): ?>
    createCountDown('countdown_<?= $flashsale->id?>', "<?= $flashsale->end_date?>");
    <?php endforeach?>
</script>

