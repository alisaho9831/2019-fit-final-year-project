
<?php $this->layout = 'wholesale_default';?>
<div class="shop-page-wrapper shop-page-padding ptb-30">
    <div class="listing_tite">
        <?php foreach ($collection as $collections ):?>
            <h4><?= strtoupper($collections->title) ?></h4>
        <?php endforeach?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="shop-sidebar mr-50">
                    <div class="sidebar-widget mb-45">
                        <h3 class="sidebar-title">Brands</h3>
                        <div class="sidebar-categories">
                            <ul>
                                <label class="form-check">
                                    <!-- Populates the brand list with the items loaded for the page -->
                                    <?php
                                    $brands = array();
                                    $brandsCount = array();
                                    foreach ($productlistings as $listProduct):
                                        foreach ($listProduct['items'] as $item):
                                            $itemBrand = $listProduct['brand_name'];
                                            $found = false;
                                            for($i = 0; $i < sizeOf($brands); $i++) {
                                                if ($brands[$i] == $itemBrand) {
                                                    $brandsCount[$i]++;
                                                    $found = true;
                                                }
                                            }
                                            if(!$found) {
                                                array_push($brands, $itemBrand);
                                                array_push($brandsCount, 1);
                                            }
                                        endforeach;
                                    endforeach;
                                    for($i = 0; $i < sizeOf($brands); $i++) {?>
                                        <li><?= $this->Html->link($brands[$i],['action' => 'filterWholesale',$items_collection_id,$brands[$i]])?> <span style="float:right"><?= $brandsCount[$i] ?></span></li>
                                    <?php } ?>
                                    <!---->
                                </label>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="shop-product-wrapper res-xl">
                    <div class="shop-bar-area">
                        <div class="shop-bar pb-60">
                            <!--                            <div class="shop-found-selector">-->
                            <!--                                <div class="shop-found">-->
                            <!--                                    <p><span>1</span> Product Found of <span>1</span></p>-->
                            <!--                                </div>-->
                            <!--                                <div class="shop-selector">-->
                            <!--                                    <label>Sort By : </label>-->
                            <!--                                    <select name="select">-->
                            <!--                                        <option value="">Default</option>-->
                            <!--                                        <option value="">A to Z</option>-->
                            <!--                                        <option value="">Z to A</option>-->
                            <!--                                        <option value="">In stock</option>-->
                            <!--                                    </select>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!--                            <div class="shop-filter-tab">-->
                            <!--                                <div class="shop-tab nav" role=tablist>-->
                            <!--                                    <a class="active" href="#grid-sidebar3" data-toggle="tab" role="tab" aria-selected="false">-->
                            <!--                                        <i class="ti-layout-grid4-alt"></i>-->
                            <!--                                    </a>-->
                            <!--                                    <a href="#grid-sidebar4" data-toggle="tab" role="tab" aria-selected="true">-->
                            <!--                                        <i class="ti-menu"></i>-->
                            <!--                                    </a>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                        </div>
                        <div class="shop-product-content tab-content">
                            <div id="grid-sidebar3" class="tab-pane fade active show">

                                    <div class="row">

                                        <?php
                                        foreach ( $collection as $collection_id):
                                            foreach ($productlistings as $listProduct):
                                                foreach ($listProduct['items'] as $item):
                                                    foreach ($listProduct['productimages'] as $image):
                                                        ?>

                                                        <div class="col-md-6 col-xl-4">
                                                            <div class="product-wrapper mb-30">
                                                                <div class="product-img">
                                                                    <?php /*foreach ($listProductImage as $listProductImages ):*/
                                                                    ?>
                                                                    <?php /*echo $this->Html->image($listProductImages->image,['url' => ['controller'=>'Products', 'action'=>'detail'],$listProductImages->product_id ]) */
                                                                    ?>

                                                                    <span>hot</span>
                                                                    <!--                                                        <div class="product-action">-->
                                                                    <!--                                                            <a class="animate-left" title="Wishlist" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-like"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                            <a class="animate-top" title="Add To Cart" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-cart"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                            <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-look"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                        </div>-->
                                                                    <?php /*endforeach */
                                                                    ?>
                                                                    <?= $this->Html->image($image->image, ['url' => ['controller' => 'Products', 'action' => 'wholesale_detail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]]); ?>
                                                                    <span>hot</span>
                                                                    <div class="product-action">
                                                                        <a class="animate-right" title="Quick View"
                                                                           data-toggle="modal"
                                                                           data-target="#exampleModal-<?= $listProduct->id ?>">
                                                                            <i class="pe-7s-look"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h4><?php echo $this->Html->link($listProduct->title, ['controller' => 'Products', 'action' => 'wholesale_detail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]); ?></h4>
                                                                    <span>$ <?= number_format($item->wholesale_price,2) ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                <?php endforeach ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                    </div>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="pagination-style mt-50 text-center">
                    <ul>
                        <!--                        <li><a href="#"><i class="ti-angle-left"></i></a></li>-->
                        <!--                        <li><a href="#">1</a></li>-->
                        <!--                        <li><a href="#">2</a></li>-->
                        <!--                        <li><a href="#">...</a></li>-->
                        <!--                        <li><a href="#">19</a></li>-->
                        <!--                        <li class="active"><a href="#"><i class="ti-angle-right"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php

    foreach ($productlistings as $listProduct ):
        foreach ($listProduct['items'] as $item):
            foreach ($listProduct['productimages'] as $image):
                ?>

                <div class="modal fade" id="exampleModal-<?= $listProduct->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span class="pe-7s-close" aria-hidden="true"></span>
                    </button>
                    <div class="modal-dialog modal-quickview-width" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="qwick-view-left">
                                    <div class="quick-view-learg-img">
                                        <div class="quick-view-tab-content tab-content">
                                            <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                                <?= $this->Html->image($image->image, ['alt' => 'CakePHP']) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="qwick-view-right">
                                    <div class="qwick-view-content">
                                        <h5><?= $listProduct->title?></h5>
                                        <div class="price">
                                            <span class="new">$ <?= number_format($item->wholesale_price,2)?></span>
                                            <span class="old">$ <?= number_format($item->retail_price,2)?></span>
                                        </div>
                                        <p><?= $listProduct->body?></p>
                                        <!--                                <div class="quick-view-select">-->
                                        <!--                                    <div class="select-option-part">-->
                                        <!--                                        <label>Size*</label>-->
                                        <!--                                        <select class="select">-->
                                        <!--                                            <option value="">- Please Select -</option>-->
                                        <!--                                            <option value="900">900</option>-->
                                        <!--                                            <option value="">700</option>-->
                                        <!--                                        </select>-->
                                        <!--                                    </div>-->
                                        <!--                                    <div class="select-option-part">-->
                                        <!--                                        <label>Colour*</label>-->
                                        <!--                                        <select class="select">-->
                                        <!--                                            <option value="">- Please Select -</option>-->
                                        <!--                                            <option value="">orange</option>-->
                                        <!--                                            <option value="">pink</option>-->
                                        <!--                                            <option value="">yellow</option>-->
                                        <!--                                        </select>-->
                                        <!--                                    </div>-->
                                        <!--                                </div>-->

                                        <?=$this->Form->create(null, [
                                            'url' => [
                                                'controller' => 'order_lines',
                                                'action' => 'addToCartWholesale'
                                            ],'onsubmit'=>"return verify()"
                                        ])?>
                                        <?php echo $this->Form->radio('is_inner_carton', [['value' => 1, 'text' => 'Inner','class' => 'form-control'],['value' => 0, 'text' => 'Outer','class' => 'form-control']],['value'=>1,'onClick'=>'checkInner()']); ?>

                                        <div class="quickview-plus-minus">

                                            <div class="cart-plus-minus">

                                                <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                                                <?=$this->form->hidden('wholesale_price',['value'=>$item->wholesale_price]);?>
                                                <?=$this->form->hidden('is_flash_sales',['value'=>0]);?>
                                                <?=$this->form->hidden('inner',['value'=>$item->inner_carton_size]);?>
                                                <?=$this->form->hidden('outer',['value'=>$item->outer_carton_size]);?>
                                                <?php $defaultMax=(int)($item->wholesale_quantity / $item->inner_carton_size); ?>
                                                <?= $this->Form->control(null,['name'=>'carton_amount', 'type'=>'number','value'=>1 ,'min'=>'1','max'=>$defaultMax,'id'=>"checkMinMax",'class' => "cart-plus-minus-box"]); ?>
                                            </div>
                                            <div class="quickview-btn-cart">
                                                <?php
                                                if($shoppingCartId->count()==0){
                                                    ?>
                                                    <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'wholesale_cart'],
                                                        ['id' => 'updatebtn','onClick'=>' return verify()']);
                                                }
                                                else{
                                                    ?>

                                                    <?php
                                                    foreach ($shoppingCartId as $shoppingcartid):
                                                        echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                                        echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                                    endforeach;

                                                }

                                                ?>
                                                <?= $this->Form->end() ?>
                                            </div>
                                            <?php if($this->request->getSession()->read('Auth')){ ?>
                                                <div class="quickview-btn-wishlist">
                                                    <?=$this->Form->create(null, [
                                                        'url' => [
                                                            'controller' => 'customers_products',
                                                            'action' => 'addToWishlistWholesale'
                                                        ]
                                                    ])?>
                                                    <?=$this->form->hidden('product_id',['value'=>$item->product_id]);?>
                                                    <?= $this->Form->button(__($this->Html->tag("i",'', array('class' => 'pe-7s-like'))),['id' => 'updatebtn']); ?>

                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?= $this->Form->end() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            <?php endforeach ?>
        <?php endforeach ?>
    <?php endforeach ?>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sorry</h4>
            </div>
            <div class="modal-body">
                <p>Please verified your email address first.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!--    <div class="paginator">
        <ul class="pagination">
            <?/*= $this->Paginator->first('<< ' . __('first')) */?>
            <?/*= $this->Paginator->prev('< ' . __('previous')) */?>
            <?/*= $this->Paginator->numbers() */?>
            <?/*= $this->Paginator->next(__('next') . ' >') */?>
            <?/*= $this->Paginator->last(__('last') . ' >>') */?>
        </ul>
        <p><?/*= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) */?></p>
    </div>-->

<script type="text/javascript" >

    function checkInner(){


        var wholesale_quantity="<?php echo $itemInfo[0]['wholesale_quantity'] ?>";
        var inner_carton_size="<?php echo $itemInfo[0]['inner_carton_size'] ?>";
        var outer_carton_size="<?php echo $itemInfo[0]['outer_carton_size'] ?>";
        var input = document.getElementById("checkMinMax");
        var isInner=document.querySelector('input[name="is_inner_carton"]:checked').value;

        var maxValue=wholesale_quantity / inner_carton_size;
        if(isInner==1){
            maxValue=Math.floor(wholesale_quantity / inner_carton_size);
        }
        else{
            maxValue=Math.floor(wholesale_quantity / outer_carton_size)
        }
        input.setAttribute("max",maxValue); // set a new value;
    }

    function verify(){
        if("<?php echo $log; ?>"=='yes') {
            if ("<?php echo $userVerified; ?>" == false) {
                $("#myModal").modal('toggle');
                //alert("please verified your email address first.");
                return false;
            }
        }

    }
</script>
