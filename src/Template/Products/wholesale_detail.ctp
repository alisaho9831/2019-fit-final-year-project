
<?php $this->layout = 'wholesale_default';?>

    <div class="product-details ptb-100 pb-90">
    <div class="container">

        <div class="row">
            <?php
            foreach ($listProducts as $listProduct ):
                foreach ($listProduct['items'] as $item):
                    foreach ($listProduct['productimages'] as $image):
            ?>
            <div class="col-md-12 col-lg-7 col-12">
                <div class="product-details-img-content">
                    <div class="product-details-tab mr-35 product-details-tab2">
                        <div class="product-details-large tab-content">
                            <?= $this->Html->image($image->image, ['alt' => 'CakePHP']) ?>
                            <div class="tab-pane fade" id="pro-details2" role="tabpanel">
                                <div class="easyzoom easyzoom--overlay  is-ready">
                                    <a href="product-details/bl2.jpg">
                                        <?= $this->Html->image('product-details/l6-details-2.jpg', ['alt' => 'CakePHP']) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pro-details3" role="tabpanel">
                                <div class="easyzoom easyzoom--overlay  is-ready">
                                    <a href="product-details/bl3.jpg">
                                        <?= $this->Html->image('product-details/l7-details-2.jpg', ['alt' => 'CakePHP']) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pro-details4" role="tabpanel">
                                <div class="easyzoom easyzoom--overlay  is-ready">
                                    <a href="product-details/bl4.jpg">
                                        <?= $this->Html->image('product-details/l8-details-2.jpg', ['alt' => 'CakePHP']) ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-5 col-12">
                <div class="product-details-content">
                    <h3><?= $listProduct->title?></h3>
                        <div class="price" >
                            <span class="new" style="font-size: 23px;">$ <?= number_format($item->wholesale_price,2) ?></span>&ensp;
                            <span class="old" style="font-size: 23px;">$ <?= number_format($item->retail_price,2)?></span>
                        </div>
                        <div style="font-size: 20px;">
                        <p style="font-size: 18px;">
                            Inner carton size: &nbsp;<?= h($item->inner_carton_size) ?>&emsp;&emsp;
                            Outer carton size: &nbsp;<?= h($item->outer_carton_size) ?></p>
                        <p style="font-size: 18px;"> Units available: &nbsp;<?= h($item->wholesale_quantity) ?></p>
                        </div>
                    <p><?= $listProduct->body?></p>


                    <?=$this->Form->create(null, [
                    'url' => [
                    'controller' => 'order_lines',
                    'action' => 'addToCartWholesale'
                    ],'onsubmit'=>"return verify()"
                    ])?>
                    <?php $this->Form->setTemplates([
                    'nestingLabel' => '<div>{{input}}<label style="font-size: 20px;padding-left:15px;text-align: center;" {{attrs}}>{{text}}</label></div>',
                    ]); ?>
                    <?php echo $this->Form->radio('is_inner_carton', [['value' => 1, 'text' => 'Inner ('.$item->inner_carton_size.' units)'],['value' => 0, 'text' => 'Outer ('.$item->outer_carton_size.' units)']],['value'=>1,'onClick'=>'checkInner()','style'=>'width: 5%;vertical-align: middle;'
                    ]); ?>

                    <div class="quickview-plus-minus">
                        <div class="cart-plus-minus">
                            <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                            <?=$this->form->hidden('wholesale_price',['value'=>$item->wholesale_price]);?>
                            <?=$this->form->hidden('is_flash_sales',['value'=>0]);?>
                            <?=$this->form->hidden('inner',['value'=>$item->inner_carton_size]);?>
                            <?=$this->form->hidden('outer',['value'=>$item->outer_carton_size]);?>
                            <?php $defaultMax=(int)($item->wholesale_quantity / $item->inner_carton_size); ?>
                            <?= $this->Form->control(null,['name'=>'carton_amount', 'type'=>'number','value'=>1 ,'min'=>'1','max'=>$defaultMax,'id'=>"checkMinMax",'class' => "cart-plus-minus-box"]); ?>
                        </div>

                                <div class="quickview-btn-cart">
                                <?php
                                    if($shoppingCartId->count()==0){
                                        ?>
                                        <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'wholesale_cart'],
                                            ['id' => 'updatebtn','onClick'=>' return verify()']);
                                        }
                                else{
                                ?>

                                <?php
                                    foreach ($shoppingCartId as $shoppingcartid):
                                echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                    endforeach;

                                }

                                ?>

                                <?= $this->Form->end() ?>
                        </div>
                         <?php if($this->request->getSession()->read('Auth')){ ?>
                        <div class="quickview-btn-wishlist">
                                <?=$this->Form->create(null, [
                                    'url' => [
                                        'controller' => 'customers_products',
                                        'action' => 'addToWishlistWholesale'
                                    ]
                                ])?>
                                <?=$this->form->hidden('product_id',['value'=>$item->product_id]);?>
                                <?= $this->Form->button(__($this->Html->tag("i",'', array('class' => 'pe-7s-like'))),['id' => 'updatebtn']); ?>
                                <?= $this->Form->end() ?>
                        </div>
                         <?php } ?>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>

<div class="product-description-review-area pb-90">
    <div class="container">
        <div class="product-description-review text-center">
            <div class="description-review-title nav" role=tablist>
                <a class="active" href="#pro-dec" data-toggle="tab" role="tab" aria-selected="true">Description</a>
<!--                <a href="#pro-review" data-toggle="tab" role="tab" aria-selected="false">Reviews (0)</a>-->
            </div>
            <div class="description-review-text tab-content">
                <div class="tab-pane active show fade" id="pro-dec" role="tabpanel">
                    <p><?= $listProduct->body?></p>
                </div>
<!--                <div class="tab-pane fade" id="pro-review" role="tabpanel">-->
<!--                    <a href="#">Be the first to write your review!</a>-->
<!--                </div>-->
            </div>
        </div>
    </div>
</div>
    <?php endforeach ?>
<?php endforeach ?>
<?php endforeach ?>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sorry</h4>
            </div>
            <div class="modal-body">
                <p>Please verified your email address first.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<!-- product area start -->





<!--<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script>
    $(document).ready(function(){
        $("#inner").hide();
        $("#outer").hide();

    });
</script>-->

<script type="text/javascript" >

    function checkInner(){


        var wholesale_quantity="<?php echo $itemInfo[0]['wholesale_quantity'] ?>";
        var inner_carton_size="<?php echo $itemInfo[0]['inner_carton_size'] ?>";
        var outer_carton_size="<?php echo $itemInfo[0]['outer_carton_size'] ?>";
        var input = document.getElementById("checkMinMax");
        var isInner=document.querySelector('input[name="is_inner_carton"]:checked').value;

        var maxValue=wholesale_quantity / inner_carton_size;
        if(isInner==1){
            maxValue=Math.floor(wholesale_quantity / inner_carton_size);
        }
        else{
            maxValue=Math.floor(wholesale_quantity / outer_carton_size)
        }
        input.setAttribute("max",maxValue); // set a new value;
    }

    function verify(){
        if("<?php echo $log; ?>"=='yes') {
            if ("<?php echo $userVerified; ?>" == false) {
                $("#myModal").modal('toggle');
                //alert("please verified your email address first.");
                return false;
            }
        }

    }
</script>
