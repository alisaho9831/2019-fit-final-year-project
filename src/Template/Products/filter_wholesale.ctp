<?php $this->layout = 'wholesale_default';?>

<div class="shop-page-wrapper shop-page-padding ptb-30">
    <div class="listing_tite">
        <?php foreach ($collection as $collections ):?>
            <h4><?= strtoupper($collections->title) ?></h4>
        <?php endforeach?>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="shop-sidebar mr-50">
                    <div class="sidebar-widget mb-45">
                        <h3 class="sidebar-title">Brands</h3>
                        <div class="sidebar-categories">
                            <ul>
                                <label class="form-check">
                                    <!-- Populates the brand list with the items loaded for the page -->
                                    <?php
                                    $brands = array();
                                    $brandsCount = array();
                                    foreach ($productlistings as $listProduct):
                                        foreach ($listProduct['items'] as $item):
                                            $itemBrand = $listProduct['brand_name'];
                                            $found = false;
                                            for($i = 0; $i < sizeOf($brands); $i++) {
                                                if ($brands[$i] == $itemBrand) {
                                                    $brandsCount[$i]++;
                                                    $found = true;
                                                }
                                            }
                                            if(!$found) {
                                                array_push($brands, $itemBrand);
                                                array_push($brandsCount, 1);
                                            }
                                        endforeach;
                                    endforeach;
                                    if($currentFilters[0] && $currentFilters[0] !== "all"){ ?>
                                        <style>
                                            #clearFilterButtonX {
                                                float:right;
                                                display: inline-block;
                                                height: 22px;
                                                width: 22px;
                                                line-height: 18px;
                                                text-align: center;
                                                background: rgb(200,0,0);
                                                border: 1px solid black;
                                                box-shadow: 1px 1px 1px 1px #ffaaaa inset, -1px -1px 1px 1px #440000 inset;
                                                border-radius: 3px;
                                            }

                                            #clearFilterButtonX:hover {
                                                box-shadow: 1px 1px 1px 1px #ffaaaa inset, -1px -1px 1px 1px #440000 inset, 0px 0px 2px 3px white inset;
                                            }

                                            #clearFilterButtonX a {
                                                color: white;
                                            }

                                            #clearFilterButtonX a:hover {
                                                color: white;
                                                text-decoration: none;
                                            }
                                        </style>
                                        <li><?= $currentFilters[0]?> <span id="clearFilterButtonX"><?= $this->Html->link("x",['action' => 'wholesaleListing',$items_collection_id,"all",$currentFilters[1],$currentFilters[2]]) ?></span></li>
                                    <?php } else {
                                        for($i = 0; $i < sizeOf($brands); $i++) {?>
                                            <li><?= $this->Html->link($brands[$i],['action' => 'filterWholesale',$items_collection_id,$brands[$i],$currentFilters[1],$currentFilters[2]])?> <span style="float:right"><?= $brandsCount[$i] ?></span></li>
                                        <?php }
                                    } ?>
                                </label>
                            </ul>
                        </div>
                        <h3 class="sidebar-title" style="margin-top:30px;">Price</h3>
                        <div class="sidebar-price">
                            <?= $this->Form->Create('PriceFilter',['action' => 'filterWholesale/'.$items_collection_id.'/'.$currentFilters[0]]) ?>
                            <?= $this->Form->input('minPrice',["value" => $currentFilters[1], 'type'=>'number', 'text'=> 'Please enter a number', 'min'=>1]) ?>
                            <div style="text-align:center;">to</div>
                            <?= $this->Form->input('maxPrice',["value" => $currentFilters[2], 'type'=>'number', 'text'=> 'Please enter a number', 'min'=>1]) ?>
                            <?= $this->Form->button('Go',array('type'=>'submit')) ?>
                            <?= $this->Form->end() ?>
                            <?php
                            if($currentFilters[1] || $currentFilters[2]){ ?>
                                <style>
                                    #clearPriceFiltersButton {
                                        line-height: 18px;
                                        text-align: center;
                                        width: 80%;
                                        background: rgb(200,0,0);
                                        border: 1px solid black;
                                        padding: 13px;
                                        height: 50px;
                                        margin: 0 auto;
                                    }

                                    #clearPriceFiltersButton:hover {
                                        background: rgb(220,50,50);
                                    }

                                    #clearPriceFiltersButton a {
                                        color: white;
                                    }

                                    #clearPriceFiltersButton a:hover {
                                        color: white;
                                        text-decoration: none;
                                    }
                                </style>
                                <div id="clearPriceFiltersButton">
                                    <?= $this->Html->link('Clear Prices',['action' => 'filterWholesale',$items_collection_id,$currentFilters[0]]) ?>
                                </div>
                            <?php } ?>
                        </div>
                        <style>
                            .sidebar-price button {
                                margin:10%;
                                width:80%;
                                border:1px solid #545454;
                                background:#909090;
                                color:white;
                                padding: 13px;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <!--                If problem, remove following three lines?-->
                <!--                --><?php //if($productlistings){ ?>
                <!--                <h4>No Matching Products Found.</h4>-->
                <!--                --><?php //} ?>
                <div class="shop-product-wrapper res-xl">
                    <div class="shop-bar-area">
                        <div class="shop-bar pb-60">
                            <!--                            <div class="shop-found-selector">-->
                            <!--                                <div class="shop-found">-->
                            <!--                                    <p><span>1</span> Product Found of <span>1</span></p>-->
                            <!--                                </div>-->
                            <!--                                <div class="shop-selector">-->
                            <!--                                    <label>Sort By : </label>-->
                            <!--                                    <select name="select">-->
                            <!--                                        <option value="">Default</option>-->
                            <!--                                        <option value="">A to Z</option>-->
                            <!--                                        <option value="">Z to A</option>-->
                            <!--                                        <option value="">In stock</option>-->
                            <!--                                    </select>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                            <!--                            <div class="shop-filter-tab">-->
                            <!--                                <div class="shop-tab nav" role=tablist>-->
                            <!--                                    <a class="active" href="#grid-sidebar3" data-toggle="tab" role="tab" aria-selected="false">-->
                            <!--                                        <i class="ti-layout-grid4-alt"></i>-->
                            <!--                                    </a>-->
                            <!--                                    <a href="#grid-sidebar4" data-toggle="tab" role="tab" aria-selected="true">-->
                            <!--                                        <i class="ti-menu"></i>-->
                            <!--                                    </a>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
                        </div>
                        <div class="shop-product-content tab-content">
                            <div id="grid-sidebar3" class="tab-pane fade active show">
                                <div class="row">
                                    <?php
                                    foreach ($collection as $collection_id):
                                        foreach ($productlistings as $listProduct):
                                            foreach ($listProduct['items'] as $item):
                                                foreach ($listProduct['productimages'] as $image):
                                                    ?>
                                                    <div class="col-md-6 col-xl-4">
                                                        <div class="product-wrapper mb-30">
                                                            <div class="product-img">
                                                                <?php /*foreach ($listProductImage as $listProductImages ):*/?>
                                                                <?php /*echo $this->Html->image($listProductImages->image,['url' => ['controller'=>'Products', 'action'=>'wholesaleDetail'],$listProductImages->product_id ]) */?>

                                                                <span>hot</span>
                                                                <?php /*endforeach */?>
                                                                <?= $this->Html->image($image->image, ['url' => ['controller'=> 'Products' , 'action'=>'wholesaleDetail' , '?' => ['product' => $listProduct->id, 'collection' => $collection_id->id]]]);?>
                                                                <span>hot</span>
                                                                <div class="product-action">
                                                                    <!--                                                            Removed useless buttons-->
                                                                    <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal-<?= $listProduct->id ?>" >
                                                                        <i class="pe-7s-look"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="product-content">
                                                                <h4><?php echo $this->Html->link($listProduct->title, ['controller'=> 'Products' , 'action'=>'wholesaleDetail' , '?' => ['product' => $listProduct->id, 'collection' => $collection_id->id]]);?></h4>
                                                                <?php /* echo $this->Html->link($listProduct->title, ['controller' => 'Products', 'action' => 'wholesaleDetail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]); */?>
                                                                <span>$ <?= $item->wholesale_price?></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php endforeach ?>
                                            <?php endforeach?>
                                        <?php endforeach?>
                                    <?php endforeach?>
                                </div>
                            </div>
                            <div id="grid-sidebar4" class="tab-pane fade">
                                <div class="row">
                                    <?php /*foreach ($listproduct as $listProducts ):*/?>
                                    <div class="col-lg-12 col-xl-6">
                                        <div class="product-wrapper mb-30 single-product-list product-list-right-pr mb-60">
                                            <div class="product-img list-img-width">
                                                <a href="#">
                                                    <?/*= $this->Html->image('product/fashion-colorful/1.jpg', ['alt' => 'CakePHP']) */?>
                                                </a>
                                                <span>hot</span>
                                                <div class="product-action-list-style">
                                                    <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal" href="#">
                                                        <i class="pe-7s-look"></i>
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="product-content-list">
                                                <div class="product-list-info">
                                                    <h4> <?php /*echo $this->Html->link($listProducts->title,['controller'=>'Products','action'=>'wholesaleDetail', $listProducts->id]);*/?> </h4>
                                                    <span>$ <?/*= $listProducts->wholesale_price*/?></span>
                                                    <p><?/*= $listProducts->body*/?> </p>
                                                </div>
                                                <div class="product-list-cart-wishlist">
                                                    <div class="product-list-cart">
                                                        <a class="btn-hover list-btn-style" href="#">add to cart</a>
                                                    </div>
                                                    <div class="product-list-wishlist">
                                                        <a class="btn-hover list-btn-wishlist" href="#">
                                                            <i class="pe-7s-like"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php /*endforeach */?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="pagination-style mt-50 text-center">
                    <ul>
                        <!--                        <li><a href="#"><i class="ti-angle-left"></i></a></li>-->
                        <!--                        <li><a href="#">1</a></li>-->
                        <!--                        <li><a href="#">2</a></li>-->
                        <!--                        <li><a href="#">...</a></li>-->
                        <!--                        <li><a href="#">19</a></li>-->
                        <!--                        <li class="active"><a href="#"><i class="ti-angle-right"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
foreach ($productlistings as $listProduct ):
foreach ($listProduct['items'] as $item):
?>
<div class="modal fade" id="exampleModal-<?= $listProduct->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span class="pe-7s-close" aria-hidden="true"></span>
    </button>
    <div class="modal-dialog modal-quickview-width" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="qwick-view-left">
                    <div class="quick-view-learg-img">
                        <div class="quick-view-tab-content tab-content">
                            <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                <?= $this->Html->image('quick-view/l1.jpg', ['alt' => 'CakePHP']) ?>
                            </div>
                            <div class="tab-pane fade" id="modal2" role="tabpanel">
                                <?= $this->Html->image('quick-view/l1.jpg', ['alt' => 'CakePHP']) ?>
                            </div>
                            <div class="tab-pane fade" id="modal3" role="tabpanel">
                                <?= $this->Html->image('quick-view/l1.jpg', ['alt' => 'CakePHP']) ?>
                            </div>
                        </div>
                    </div>
                    <div class="quick-view-list nav" role="tablist">
                        <a class="active" href="#modal1" data-toggle="tab" role="tab">
                            <?= $this->Html->image('quick-view/s1.jpg', ['alt' => 'CakePHP']) ?>
                        </a>
                        <a href="#modal2" data-toggle="tab" role="tab">
                            <?= $this->Html->image('quick-view/s2.jpg', ['alt' => 'CakePHP']) ?>
                        </a>
                        <a href="#modal3" data-toggle="tab" role="tab">
                            <?= $this->Html->image('quick-view/s3.jpg', ['alt' => 'CakePHP']) ?>
                        </a>
                    </div>
                </div>
                <div class="qwick-view-right">
                    <div class="qwick-view-content">
                        <h5><?= $listProduct->title?></h5>
                        <div class="price">
                            <span class="new">$ <?= $item->wholesale_price?></span>
                            <span class="old">$ <?= $listProduct->market_price?></span>
                        </div>
                        <p><?= $listProduct->body?></p>

                        <?php echo $this->Form->radio('is_inner_carton', [['value' => 1, 'text' => 'Inner','class' => 'form-control'],['value' => 0, 'text' => 'Outer','class' => 'form-control']],['value'=>1,'onClick'=>'checkInner()']); ?>

                        <div class="quickview-plus-minus">
                            <div class="cart-plus-minus">

                                <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                                <?=$this->form->hidden('wholesale_price',['value'=>$item->wholesale_price]);?>
                                <?= $this->Form->control(null,['name'=>'amount', 'type'=>'number','value'=>1 , 'min'=>1, 'max'=>$item->wholesale_quantity, 'class' => "cart-plus-minus-box"]); ?>

                            </div>
                            <div class="quickview-btn-cart">
                                <?php
                                if($shoppingCartId->count()==0){
                                    ?>
                                    <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'wholesaleCart'],
                                        ['id' => 'updatebtn']);
                                }
                                else{
                                    ?>

                                    <?php
                                    foreach ($shoppingCartId as $shoppingcartid):
                                        echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                        echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                    endforeach;

                                }

                                ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php endforeach ?>
    <?php endforeach ?>
