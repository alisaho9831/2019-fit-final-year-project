<div class="shop-page-wrapper shop-page-padding ptb-30">
    <?php
    if($flashSales->isEmpty()) {
    ?>
    <div class="listing_tite">
        <?php foreach ($collection as $collections ):?>
            <h4><b><?= strtoupper($collections->title) ?></b></h4>
        <?php endforeach?>
    </div>
    <?php } else{ ?>
        <div class="listing_tite">
            <?php
                foreach ($collection as $collections ):
                    foreach ($flashSales as $flashSale):
                        ?>

                        <h4><b><?= strtoupper($collections->title) ?></b></h4>

                        <h5 id="countdown"></h5>
                        <script>
                            // Set the date we're counting down to
                            var countDownDate = new Date("<?= $flashSale->end_date;?>").getTime();

                            // Update the count down every 1 second
                            var x = setInterval(function() {

                                // Get today's date and time
                                var now = new Date().getTime();

                                // Find the distance between now and the count down date
                                var distance = countDownDate - now;

                                // Time calculations for days, hours, minutes and seconds
                                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                                var seconds = Math.floor((distance % (1000 * 60)) / 1000);

                                // Display the result in the element with id="demo"
                                document.getElementById("countdown").innerHTML = "End in "+days + "D " + hours + "H "
                                    + minutes + "M " + seconds + "S ";

                                // If the count down is finished, write some text
                                if (distance < 0) {
                                    clearInterval(x);
                                    document.getElementById("countdown").innerHTML = "EXPIRED";
                                }
                            }, 1000);
                        </script>

                    <?php endforeach?>
                <?php endforeach?>
        </div>
    <?php } ?>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3">
                <div class="shop-sidebar mr-50">
                    <div class="sidebar-widget mb-45">
                        <h3 class="sidebar-title">Brands</h3>
                        <div class="sidebar-categories">
                            <ul>
                                <label class="form-check">
                                    <!-- Populates the brand list with the items loaded for the page -->
                                    <?php
                                    $brands = array();
                                    $brandsCount = array();
                                    foreach ($productlistings as $listProduct):
                                        foreach ($listProduct['items'] as $item):
                                            $itemBrand = $listProduct['brand_name'];
                                            $found = false;
                                            for($i = 0; $i < sizeOf($brands); $i++) {
                                                if ($brands[$i] == $itemBrand) {
                                                    $brandsCount[$i]++;
                                                    $found = true;
                                                }
                                            }
                                            if(!$found) {
                                                array_push($brands, $itemBrand);
                                                array_push($brandsCount, 1);
                                            }
                                        endforeach;
                                    endforeach;
                                    if(sizeOf($brands) == 1){ ?>
                                        <style>
                                            .sidebar-categories ul li :hover{
                                                text-decoration: line-through;
                                            }
                                        </style>
                                        <li><?= $this->Html->link($brands[0],['action' => 'filter',$items_collection_id,"all"])?> <span style="float:right"><?= $brandsCount[0] ?></span></li>
                                    <?php } else {
                                        for($i = 0; $i < sizeOf($brands); $i++) {?>
                                            <li><?= $this->Html->link($brands[$i],['action' => 'filter',$items_collection_id,$brands[$i]])?> <span style="float:right"><?= $brandsCount[$i] ?></span></li>
                                        <?php }
                                    } ?>
                                    <!---->
                                </label>
                            </ul>
                        </div>
                        <h3 class="sidebar-title" style="margin-top:30px;">Price</h3>
                        <div class="sidebar-price">
<!--                            Code now only allows numbers and decimal dots-->
                            <?= $this->Form->Create('PriceFilter',['action' => 'filter/'.$items_collection_id.'/all']) ?>
                            <?= $this->Form->input('minPrice',['type'=>'number', 'text'=> 'Please enter a number', 'min'=>1]) ?>
                            <div style="text-align:center;">to</div>
                            <?= $this->Form->input('maxPrice',['type'=>'number', 'text'=> 'Please enter a number', 'min'=>1]) ?>
                            <?= $this->Form->button('Go',array('type'=>'submit')) ?>
                            <?= $this->Form->end() ?>
                        </div>
                        <style>
                            .sidebar-price button {
                                margin:10%;
                                width:80%;
                                border:1px solid #545454;
                                background:#909090;
                                color:white;
                                padding: 12px;
                            }
                        </style>
                    </div>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="shop-product-wrapper res-xl">
                    <div class="shop-bar-area">
                        <div class="shop-bar pb-60">
                            <!--                            <div class="shop-found-selector">-->
                            <!--                                <div class="shop-found">-->
                            <!--                                    <p><span>1</span> Product Found of <span>1</span></p>-->
                            <!--                                </div>-->
                            <!--                                <div class="shop-selector">-->
                            <!--                                    <label>Sort By : </label>-->
                            <!--                                    <select name="select">-->
                            <!--                                        <option value="">Default</option>-->
                            <!--                                        <option value="">A to Z</option>-->
                            <!--                                        <option value="">Z to A</option>-->
                            <!--                                        <option value="">In stock</option>-->
                            <!--                                    </select>-->
                            <!--                                </div>-->
                            <!--                            </div>-->
<!--                            <div class="shop-filter-tab">-->
<!--                                <div class="shop-tab nav" role=tablist>-->
<!--                                    <a class="active" href="#grid-sidebar3" data-toggle="tab" role="tab" aria-selected="false">-->
<!--                                        <i class="ti-layout-grid4-alt"></i>-->
<!--                                    </a>-->
<!--                                    <a href="#grid-sidebar4" data-toggle="tab" role="tab" aria-selected="true">-->
<!--                                        <i class="ti-menu"></i>-->
<!--                                    </a>-->
<!--                                </div>-->
<!--                            </div>-->
                        </div>
                        <div class="shop-product-content tab-content">
                            <div id="grid-sidebar3" class="tab-pane fade active show">

                                <?php
                                    if($flashSales->isEmpty()) {

                                        ?>

                                        <div class="row">

                                            <?php
                                        foreach ( $collection as $collection_id):
                                            foreach ($productlistings as $listProduct):
                                                foreach ($listProduct['items'] as $item):
                                                    foreach ($listProduct['productimages'] as $image):
                                                        ?>

                                                        <div class="col-md-6 col-xl-4">
                                                            <div class="product-wrapper mb-30">
                                                                <div class="product-img">
                                                                    <?php /*foreach ($listProductImage as $listProductImages ):*/
                                                                    ?>
                                                                    <?php /*echo $this->Html->image($listProductImages->image,['url' => ['controller'=>'Products', 'action'=>'detail'],$listProductImages->product_id ]) */
                                                                    ?>

                                                                    <span>hot</span>
                                                                    <!--                                                        <div class="product-action">-->
                                                                    <!--                                                            <a class="animate-left" title="Wishlist" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-like"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                            <a class="animate-top" title="Add To Cart" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-cart"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                            <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-look"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                        </div>-->
                                                                    <?php /*endforeach */?>
                                                                    <?= $this->Html->image($image->image, ['url' => ['controller' => 'Products', 'action' => 'detail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]]); ?>
                                                                    <span>hot</span>
                                                                    <div class="product-action">
                                                                        <!--<a class="animate-left" title="Wishlist"
                                                                           href="#">
                                                                            <i class="pe-7s-like"></i>
                                                                        </a
                                                                        <?/*= $this->Form->create(null, [
                                                                            'url' => [
                                                                                'controller' => 'order_lines',
                                                                                'action' => 'addToCart'
                                                                            ]
                                                                        ]) */?>
                                                                        <?/*= $this->form->hidden('item_id', ['value' => $item->id]); */?>
                                                                        <?/*= $this->form->hidden('retail_price', ['value' => $item->retail_price]); */?>
                                                                        <?/*= $this->form->hidden('amount', ['value' => 1]); */?>
                                                                        <?php
/*                                                                        if ($shoppingCartId->count() == 0) {
                                                                            */?>
                                                                            <?php /*echo $this->Html->link($this->Html->tag("i", '', array('class' => 'pe-7s-cart')), ["controller" => "Orders", "action" => "cart"], ["escape" => false]);
                                                                        } else {
                                                                            */?>

                                                                            <?php
/*                                                                            foreach ($shoppingCartId as $shoppingcartid):
                                                                                echo $this->form->hidden('order_id', ['value' => $shoppingcartid->order_id]);
                                                                                echo $this->Form->button(__($this->Html->tag("i", '', array('class' => 'pe-7s-cart'))), ['class' => 'animate-top'], ["escape" => false]);
                                                                            endforeach;

                                                                        }
                                                                        */?>
                                                                        <?/*= $this->Form->end() */?>-->
                                                                        <a class="animate-right" title="Quick View"
                                                                           data-toggle="modal"
                                                                           data-target="#exampleModal-<?= $listProduct->id ?>">
                                                                            <i class="pe-7s-look"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h4><?php echo $this->Html->link($listProduct->title, ['controller' => 'Products', 'action' => 'detail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]); ?></h4>
                                                                    <span>$ <?= number_format($item->retail_price,2) ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    <?php endforeach ?>
                                                <?php endforeach ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                        </div>
                                        <?php
                                    }else{
                                ?>
                                        <div class="row">

                                            <?php
                                        foreach ( $collection as $collection_id):
                                        foreach ($flashSales as $flashSale):
                                            foreach ($productlistings as $listProduct):
                                                foreach ($listProduct['items'] as $item):
                                                    foreach ($listProduct['productimages'] as $image):
                                                        ?>

                                                        <div class="col-md-6 col-xl-4">
                                                            <div class="product-wrapper mb-30">
                                                                <div class="product-img">
                                                                    <?php /*foreach ($listProductImage as $listProductImages ):*/
                                                                    ?>
                                                                    <?php /*echo $this->Html->image($listProductImages->image,['url' => ['controller'=>'Products', 'action'=>'detail'],$listProductImages->product_id ]) */
                                                                    ?>

                                                                    <span>hot</span>
                                                                    <!--                                                        <div class="product-action">-->
                                                                    <!--                                                            <a class="animate-left" title="Wishlist" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-like"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                            <a class="animate-top" title="Add To Cart" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-cart"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                            <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal" href="#">-->
                                                                    <!--                                                                <i class="pe-7s-look"></i>-->
                                                                    <!--                                                            </a>-->
                                                                    <!--                                                        </div>-->
                                                                    <?php /*endforeach */
                                                                    ?>
                                                                    <?= $this->Html->image($image->image, ['url' => ['controller' => 'Products', 'action' => 'detail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]]); ?>
                                                                    <span>New</span>
                                                                    <div class="product-action">
                                                                        <!--<a class="animate-left" title="Wishlist"
                                                                           href="#">
                                                                            <i class="pe-7s-like"></i>
                                                                        </a-->
                                                                        <?php
                                                                            if($flashSale->discount_type == "Subtract"){
                                                                                $specialPrice = $item->retail_price - $flashSale->discount_amount;
                                                                            } else if($flashSale->discount_type == "Multiply"){
                                                                                $specialPrice = $item->retail_price * $flashSale->discount_amount;
                                                                            }
                                                                        ?>
                                                                        <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal-<?= $listProduct->id ?>">
                                                                            <i class="pe-7s-look"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                <div class="product-content">
                                                                    <h4><?php echo $this->Html->link($listProduct->title, ['controller' => 'Products', 'action' => 'detail', '?' => ['product' => $listProduct->id, 'collection' => $collection_id -> id]]); ?></h4>
                                                                    <span>$ <?= number_format($specialPrice,2) ?></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php endforeach ?>
                                                    <?php endforeach ?>
                                                <?php endforeach ?>
                                            <?php endforeach ?>
                                        <?php endforeach ?>
                                        </div>
                                <?php }?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="pagination-style mt-50 text-center">
                    <ul>
                        <!--                        <li><a href="#"><i class="ti-angle-left"></i></a></li>-->
                        <!--                        <li><a href="#">1</a></li>-->
                        <!--                        <li><a href="#">2</a></li>-->
                        <!--                        <li><a href="#">...</a></li>-->
                        <!--                        <li><a href="#">19</a></li>-->
                        <!--                        <li class="active"><a href="#"><i class="ti-angle-right"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if($flashSales->isEmpty()) {
?>
<?php

foreach ($productlistings as $listProduct ):
    foreach ($listProduct['items'] as $item):
        foreach ($listProduct['productimages'] as $image):
        ?>

        <div class="modal fade" id="exampleModal-<?= $listProduct->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="pe-7s-close" aria-hidden="true"></span>
            </button>
            <div class="modal-dialog modal-quickview-width" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="qwick-view-left">
                            <div class="quick-view-learg-img">
                                <div class="quick-view-tab-content tab-content">
                                    <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                        <?= $this->Html->image($image->image, ['alt' => 'CakePHP']) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="qwick-view-right">
                            <div class="qwick-view-content">
                                <h5><?= $listProduct->title?></h5>
                                <div class="price">
                                    <span class="new">$ <?= $item->retail_price?></span>
                                    <span class="old">$ <?= $listProduct->market_price?></span>
                                </div>
                                <p><?= $listProduct->body?></p>
<!--                                <div class="quick-view-select">-->
<!--                                    <div class="select-option-part">-->
<!--                                        <label>Size*</label>-->
<!--                                        <select class="select">-->
<!--                                            <option value="">- Please Select -</option>-->
<!--                                            <option value="900">900</option>-->
<!--                                            <option value="">700</option>-->
<!--                                        </select>-->
<!--                                    </div>-->
<!--                                    <div class="select-option-part">-->
<!--                                        <label>Colour*</label>-->
<!--                                        <select class="select">-->
<!--                                            <option value="">- Please Select -</option>-->
<!--                                            <option value="">orange</option>-->
<!--                                            <option value="">pink</option>-->
<!--                                            <option value="">yellow</option>-->
<!--                                        </select>-->
<!--                                    </div>-->
<!--                                </div>-->
                                <div class="quickview-plus-minus">

                                    <div class="cart-plus-minus">
                                        <?=$this->Form->create(null, [
                                            'url' => [
                                                'controller' => 'order_lines',
                                                'action' => 'addToCart'
                                            ],'onsubmit'=>"return verify()"
                                        ])?>
                                        <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                                        <?=$this->form->hidden('is_flash_sales',['value'=>1]);?>
                                        <?=$this->form->hidden('retail_price',['value'=>$item->retail_price]);?>
                                        <?= $this->Form->control(null,['name'=>'amount', 'type'=>'number','value'=>1 , 'min'=>1, 'max'=>$item->retail_quantity, 'class' => "cart-plus-minus-box"]); ?>
                                    </div>
                                    <div class="quickview-btn-cart">
                                        <?php
                                        if($shoppingCartId->count()==0){
                                            ?>
                                            <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'cart'],
                                                ['id' => 'updatebtn','onClick'=>'return verify()']);
                                        }
                                        else{
                                            ?>

                                            <?php
                                            foreach ($shoppingCartId as $shoppingcartid):
                                                echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                                echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                            endforeach;

                                        }

                                        ?>
                                        <?= $this->Form->end() ?>
                                    </div>
                                <?php if($this->request->getSession()->read('Auth')){ ?>
                                    <div class="quickview-btn-wishlist">
                                        <?=$this->Form->create(null, [
                                            'url' => [
                                                'controller' => 'customers_products',
                                                'action' => 'addToWishlist'
                                            ]
                                        ])?>
                                        <?=$this->form->hidden('product_id',['value'=>$item->product_id]);?>
                                        <?= $this->Form->button(__($this->Html->tag("i",'', array('class' => 'pe-7s-like'))),['id' => 'updatebtn']); ?>
                                        <?= $this->Form->end() ?>
                                    </div>
                                <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php endforeach ?>
    <?php endforeach ?>
<?php endforeach ?>

<!--    <div class="paginator">
        <ul class="pagination">
            <?/*= $this->Paginator->first('<< ' . __('first')) */?>
            <?/*= $this->Paginator->prev('< ' . __('previous')) */?>
            <?/*= $this->Paginator->numbers() */?>
            <?/*= $this->Paginator->next(__('next') . ' >') */?>
            <?/*= $this->Paginator->last(__('last') . ' >>') */?>
        </ul>
        <p><?/*= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) */?></p>
    </div>-->

    <?php
}else{
    foreach ($flashSales as $flashSale):
    foreach ($productlistings as $listProduct ):
    foreach ($listProduct['items'] as $item):
    foreach ($listProduct['productimages'] as $image):
    ?>

    <div class="modal fade" id="exampleModal-<?= $listProduct->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span class="pe-7s-close" aria-hidden="true"></span>
        </button>
        <div class="modal-dialog modal-quickview-width" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="qwick-view-left">
                        <div class="quick-view-learg-img">
                            <div class="quick-view-tab-content tab-content">
                                <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                    <?= $this->Html->image($image->image, ['alt' => 'CakePHP']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($flashSale->discount_type == "Subtract"){
                        $specialPrice = $item->retail_price - $flashSale->discount_amount;
                    } else if($flashSale->discount_type == "Multiply"){
                        $specialPrice = $item->retail_price * $flashSale->discount_amount;
                    }
                    ?>
                    <div class="qwick-view-right">
                        <div class="qwick-view-content">
                            <h5><?= $listProduct->title?></h5>
                            <div class="price">
                                <span class="new">$ <?= number_format($specialPrice,2)?></span>
                                <span class="old">$ <?= number_format($item->retail_price,2)?></span>
                            </div>
                            <p><?= $listProduct->body?></p>
                            <!--                                <div class="quick-view-select">-->
                            <!--                                    <div class="select-option-part">-->
                            <!--                                        <label>Size*</label>-->
                            <!--                                        <select class="select">-->
                            <!--                                            <option value="">- Please Select -</option>-->
                            <!--                                            <option value="900">900</option>-->
                            <!--                                            <option value="">700</option>-->
                            <!--                                        </select>-->
                            <!--                                    </div>-->
                            <!--                                    <div class="select-option-part">-->
                            <!--                                        <label>Colour*</label>-->
                            <!--                                        <select class="select">-->
                            <!--                                            <option value="">- Please Select -</option>-->
                            <!--                                            <option value="">orange</option>-->
                            <!--                                            <option value="">pink</option>-->
                            <!--                                            <option value="">yellow</option>-->
                            <!--                                        </select>-->
                            <!--                                    </div>-->
                            <!--                                </div>-->
                            <div class="quickview-plus-minus">

                                <div class="cart-plus-minus">
                                    <?=$this->Form->create(null, [
                                        'url' => [
                                            'controller' => 'order_lines',
                                            'action' => 'addToCart'
                                        ],'onsubmit'=>"return verify()"
                                    ])?>
                                    <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                                    <?=$this->form->hidden('retail_price',['value'=>$specialPrice]);?>
                                    <?=$this->form->hidden('is_flash_sales',['value'=>1]);?>
                                    <?=$this->form->hidden('expiry_date',['value'=>date_format($flashSale->end_date,"Y/m/d H:i:s")]);?>
                                    <?= $this->Form->control(null,['name'=>'amount', 'type'=>'number','value'=>1 , 'min'=>1, 'max'=>$item->retail_quantity, 'class' => "cart-plus-minus-box"]); ?>
                                </div>
                                <div class="quickview-btn-cart">
                                    <?php
                                    if($shoppingCartId->count()==0){
                                        ?>
                                        <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'cart'],
                                            ['id' => 'updatebtn','onClick'=>' return verify()']);
                                    }
                                    else{
                                        ?>

                                        <?php
                                        foreach ($shoppingCartId as $shoppingcartid):
                                            echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                            echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                        endforeach;

                                    }

                                    ?>
                                    <?= $this->Form->end() ?>
                                </div>
                                <?php if($this->request->getSession()->read('Auth')){ ?>
                                    <div class="quickview-btn-wishlist">
                                        <?=$this->Form->create(null, [
                                            'url' => [
                                                'controller' => 'customers_products',
                                                'action' => 'addToWishlist'
                                            ]
                                        ])?>
                                        <?=$this->form->hidden('product_id',['value'=>$item->product_id]);?>
                                        <?= $this->Form->button(__($this->Html->tag("i",'', array('class' => 'pe-7s-like'))),['id' => 'updatebtn']); ?>
                                        <?= $this->Form->end() ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php endforeach ?>
    <?php endforeach ?>
    <?php endforeach ?>
    <?php endforeach ?>
<?php }?>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Sorry</h4>
            </div>
            <div class="modal-body">
                <p>Please verified your email address first.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript" >
    function verify(){
        if("<?php echo $log; ?>"=='yes'){
            if("<?php echo $userVerified; ?>"==false){
                $("#myModal").modal('toggle');
                return false;
            }
        }


    }
</script>
