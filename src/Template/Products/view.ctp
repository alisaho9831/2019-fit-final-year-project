
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Product $product
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Product'), ['action' => 'edit', $product->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Product'), ['action' => 'delete', $product->id], ['confirm' => __('Are you sure you want to delete # {0}?', $product->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Product Images'), ['controller' => 'Productimages', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Image'), ['controller' => 'Productimages', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['controller' => 'Customers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['controller' => 'Customers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="products view large-9 medium-8 columns content">
    <h3><?= h($product->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Brand Name') ?></th>
            <td><?= h($product->brand_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($product->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Url') ?></th>
            <td><?= h($product->url) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Body') ?></th>
            <td><?= h($product->body) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Seo Description') ?></th>
            <td><?= h($product->seo_description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($product->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Market Price') ?></th>
            <td><?= $this->Number->format($product->market_price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cost Per Item') ?></th>
            <td><?= $this->Number->format($product->cost_per_item) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deactivated') ?></th>
            <td><?= $product->is_deactivated ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Customers') ?></h4>
        <?php if (!empty($product->customers)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Verify Code') ?></th>
                <th scope="col"><?= __('Is Verified') ?></th>
                <th scope="col"><?= __('Company') ?></th>
                <th scope="col"><?= __('Phone1') ?></th>
                <th scope="col"><?= __('Phone2') ?></th>
                <th scope="col"><?= __('Accepts Marketing') ?></th>
                <th scope="col"><?= __('Is Deactivated') ?></th>
                <th scope="col"><?= __('Total Spent') ?></th>
                <th scope="col"><?= __('Total Orders') ?></th>
                <th scope="col"><?= __('Is Wholesale') ?></th>
                <th scope="col"><?= __('Note') ?></th>
                <th scope="col"><?= __('Abn') ?></th>
                <th scope="col"><?= __('Wholesale Discount') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->customers as $customers): ?>
            <tr>
                <td><?= h($customers->id) ?></td>
                <td><?= h($customers->first_name) ?></td>
                <td><?= h($customers->last_name) ?></td>
                <td><?= h($customers->email) ?></td>
                <td><?= h($customers->verify_code) ?></td>
                <td><?= h($customers->is_verified) ?></td>
                <td><?= h($customers->company) ?></td>
                <td><?= h($customers->phone1) ?></td>
                <td><?= h($customers->phone2) ?></td>
                <td><?= h($customers->accepts_marketing) ?></td>
                <td><?= h($customers->is_deactivated) ?></td>
                <td><?= h($customers->total_spent) ?></td>
                <td><?= h($customers->total_orders) ?></td>
                <td><?= h($customers->is_wholesale) ?></td>
                <td><?= h($customers->note) ?></td>
                <td><?= h($customers->abn) ?></td>
                <td><?= h($customers->wholesale_discount) ?></td>
                <td><?= h($customers->user_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Customers', 'action' => 'view', $customers->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Customers', 'action' => 'edit', $customers->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Customers', 'action' => 'delete', $customers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customers->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Items') ?></h4>
        <?php if (!empty($product->items)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Barcode') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Option Name') ?></th>
                <th scope="col"><?= __('Option Value') ?></th>
                <th scope="col"><?= __('Retail Quantity') ?></th>
                <th scope="col"><?= __('Wholesale Quantity') ?></th>
                <th scope="col"><?= __('Inner Carton Size') ?></th>
                <th scope="col"><?= __('Outer Carton Size') ?></th>
                <th scope="col"><?= __('Wholesale Price') ?></th>
                <th scope="col"><?= __('Retail Price') ?></th>
                <th scope="col"><?= __('Weight') ?></th>
                <th scope="col"><?= __('Is Wholesale') ?></th>
                <th scope="col"><?= __('Is Retail') ?></th>
                <th scope="col"><?= __('Published Wholesale') ?></th>
                <th scope="col"><?= __('Published Retail') ?></th>
                <th scope="col"><?= __('Is Deactivated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->items as $items): ?>
            <tr>
                <td><?= h($items->id) ?></td>
                <td><?= h($items->barcode) ?></td>
                <td><?= h($items->product_id) ?></td>
                <td><?= h($items->option_name) ?></td>
                <td><?= h($items->option_value) ?></td>
                <td><?= h($items->retail_quantity) ?></td>
                <td><?= h($items->wholesale_quantity) ?></td>
                <td><?= h($items->inner_carton_size) ?></td>
                <td><?= h($items->outer_carton_size) ?></td>
                <td><?= h($items->wholesale_price) ?></td>
                <td><?= h($items->retail_price) ?></td>
                <td><?= h($items->weight) ?></td>
                <td><?= h($items->is_wholesale) ?></td>
                <td><?= h($items->is_retail) ?></td>
                <td><?= h($items->published_wholesale) ?></td>
                <td><?= h($items->published_retail) ?></td>
                <td><?= h($items->is_deactivated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Items', 'action' => 'view', $items->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Items', 'action' => 'edit', $items->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Items', 'action' => 'delete', $items->id], ['confirm' => __('Are you sure you want to delete # {0}?', $items->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Productimages') ?></h4>
        <?php if (!empty($product->product_images)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Image Dir') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($product->product_images as $productImages): ?>
            <tr>
                <td><?= h($productImages->id) ?></td>
                <td><?= h($productImages->product_id) ?></td>
                <td><?= h($productImages->image) ?></td>
                <td><?= h($productImages->image_dir) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Productimages', 'action' => 'view', $productImages->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Productimages', 'action' => 'edit', $productImages->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Productimages', 'action' => 'delete', $productImages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productImages->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
