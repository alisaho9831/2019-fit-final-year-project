<div class="product-details ptb-100 pb-90">
    <div class="container">

        <div class="row">
            <?php
            foreach ($listProducts as $listProduct ):
            foreach ($listProduct['items'] as $item):
            foreach ($listProduct['productimages'] as $image):
            ?>
            <div class="col-md-12 col-lg-7 col-12">
                <div class="product-details-img-content">
                    <div class="product-details-tab mr-35 product-details-tab2">
                        <div class="product-details-large tab-content">
                            <?= $this->Html->image($image->image, ['alt' => 'CakePHP']) ?>
                            <div class="tab-pane fade" id="pro-details2" role="tabpanel">
                                <div class="easyzoom easyzoom--overlay  is-ready">
                                    <a href="product-details/bl2.jpg">
                                        <?= $this->Html->image('product-details/l6-details-2.jpg', ['alt' => 'CakePHP']) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pro-details3" role="tabpanel">
                                <div class="easyzoom easyzoom--overlay  is-ready">
                                    <a href="product-details/bl3.jpg">
                                        <?= $this->Html->image('product-details/l7-details-2.jpg', ['alt' => 'CakePHP']) ?>
                                    </a>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="pro-details4" role="tabpanel">
                                <div class="easyzoom easyzoom--overlay  is-ready">
                                    <a href="product-details/bl4.jpg">
                                        <?= $this->Html->image('product-details/l8-details-2.jpg', ['alt' => 'CakePHP']) ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12 col-lg-5 col-12">
                <div class="product-details-content">
                    <h3><?= $listProduct->title?></h3>
                    <div class="price" >
                        <span class="new" style="font-size: 23px;">$ <?= $item->retail_price ?></span>&ensp;
                        <span class="old" style="font-size: 23px;">$ <?= $listProduct->market_price?></span>
                    </div>
                    <p><?= $listProduct->body?></p>
                    <!--                    <div class="quick-view-select">-->
                    <!--                        <div class="select-option-part">-->
                    <!--                            <label>Size*</label>-->
                    <!--                            <select class="select">-->
                    <!--                                <option value="">- Please Select -</option>-->
                    <!--                                <option value="">xl</option>-->
                    <!--                                <option value="">ml</option>-->
                    <!--                                <option value="">m</option>-->
                    <!--                                <option value="">sl</option>-->
                    <!--                            </select>-->
                    <!--                        </div>-->
                    <!--                        <div class="select-option-part">-->
                    <!--                            <label>Color*</label>-->
                    <!--                            <select class="select">-->
                    <!--                                <option value="">- Please Select -</option>-->
                    <!--                                <option value="">orange</option>-->
                    <!--                                <option value="">pink</option>-->
                    <!--                                <option value="">yellow</option>-->
                    <!--                            </select>-->
                    <!--                        </div>-->
                    <!--                    </div>-->

                    <div class="quickview-plus-minus">
                        <div class="cart-plus-minus">
                            <?=$this->Form->create(null, [
                                'url' => [
                                    'controller' => 'order_lines',
                                    'action' => 'addToCart'
                                ]
                            ])?>
                            <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                            <?=$this->form->hidden('retail_price',['value'=>$item->retail_price]);?>
                            <?=$this->form->hidden('is_flash_sales',['value'=>0]);?>
                            <?= $this->Form->control(null,['name'=>'amount', 'type'=>'number','value'=>1 , 'min'=>1, 'max'=>$item->retail_quantity, 'class' => "cart-plus-minus-box"]); ?>

                        </div>
                        <div class="quickview-btn-cart">
                            <?php
                            if($shoppingCartId->count()==0){
                                ?>
                                <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'cart'],
                                    ['id' => 'updatebtn']);
                            }
                            else{
                                ?>

                                <?php
                                foreach ($shoppingCartId as $shoppingcartid):
                                    echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                    echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                endforeach;

                            }

                            ?>
                            <?= $this->Form->end() ?>
                        </div>
                        <?php if($this->request->getSession()->read('Auth')){ ?>
                            <div class="quickview-btn-wishlist">
                                <?=$this->Form->create(null, [
                                    'url' => [
                                        'controller' => 'customers_products',
                                        'action' => 'addToWishlist'
                                    ]
                                ])?>
                                <?=$this->form->hidden('product_id',['value'=>$item->product_id]);?>
                                <?= $this->Form->button(__($this->Html->tag("i",'', array('class' => 'pe-7s-like'))),['id' => 'updatebtn']); ?>
                                <?= $this->Form->end() ?>
                            </div>
                        <?php } ?>
                        <!--                    <div class="product-details-cati-tag mt-35">-->
                        <!--                        <ul>-->
                        <!--                            <li class="categories-title">Categories :</li>-->
                        <!--                            <li><a href="#">fashion</a></li>-->
                        <!--                            <li><a href="#">electronics</a></li>-->
                        <!--                            <li><a href="#">toys</a></li>-->
                        <!--                            <li><a href="#">food</a></li>-->
                        <!--                            <li><a href="#">jewellery</a></li>-->
                        <!--                        </ul>-->
                        <!--                    </div>-->
                        <!--                    <div class="product-details-cati-tag mtb-10">-->
                        <!--                        <ul>-->
                        <!--                            <li class="categories-title">Tags :</li>-->
                        <!--                            <li><a href="#">fashion</a></li>-->
                        <!--                            <li><a href="#">electronics</a></li>-->
                        <!--                            <li><a href="#">toys</a></li>-->
                        <!--                            <li><a href="#">food</a></li>-->
                        <!--                            <li><a href="#">jewellery</a></li>-->
                        <!--                        </ul>-->
                        <!--                    </div>-->
                        <!--                    <div class="product-share">-->
                        <!--                        <ul>-->
                        <!--                            <li class="categories-title">Share :</li>-->
                        <!--                            <li>-->
                        <!--                                <a href="#">-->
                        <!--                                    <i class="icofont icofont-social-facebook"></i>-->
                        <!--                                </a>-->
                        <!--                            </li>-->
                        <!--                            <li>-->
                        <!--                                <a href="#">-->
                        <!--                                    <i class="icofont icofont-social-twitter"></i>-->
                        <!--                                </a>-->
                        <!--                            </li>-->
                        <!--                            <li>-->
                        <!--                                <a href="#">-->
                        <!--                                    <i class="icofont icofont-social-pinterest"></i>-->
                        <!--                                </a>-->
                        <!--                            </li>-->
                        <!--                            <li>-->
                        <!--                                <a href="#">-->
                        <!--                                    <i class="icofont icofont-social-flikr"></i>-->
                        <!--                                </a>-->
                        <!--                            </li>-->
                        <!--                        </ul>-->
                        <!--                    </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="product-description-review-area pb-90">
    <div class="container">
        <div class="product-description-review text-center">
            <div class="description-review-title nav" role=tablist>
                <a class="active" href="#pro-dec" data-toggle="tab" role="tab" aria-selected="true">Description</a>
                <!--                <a href="#pro-review" data-toggle="tab" role="tab" aria-selected="false">Reviews (0)</a>-->
            </div>
            <div class="description-review-text tab-content">
                <div class="tab-pane active show fade" id="pro-dec" role="tabpanel">
                    <p><?= $listProduct->body?></p>
                </div>
                <!--                <div class="tab-pane fade" id="pro-review" role="tabpanel">-->
                <!--                    <a href="#">Be the first to write your review!</a>-->
                <!--                </div>-->
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
<?php endforeach ?>
<?php endforeach ?>
<!-- product area start -->
<div class="section-title-3 text-center mb-50">
    <h2>Related products</h2>
</div>

<div class="shop-product-content tab-content">
    <div id="grid-sidebar3" class="tab-pane fade active show">

            <div class="row">

                <?php
                foreach ($relatedProducts as $relatedProduct ):
                foreach ($relatedProduct['items'] as $item):
                foreach ($relatedProduct['productimages'] as $image):
                                ?>

                                <div class="col-md-6 col-xl-4">
                                    <div class="product-wrapper mb-30">
                                        <div class="product-img">
                                            <?php /*foreach ($listProductImage as $listProductImages ):*/
                                            ?>
                                            <?php /*echo $this->Html->image($listProductImages->image,['url' => ['controller'=>'Products', 'action'=>'detail'],$listProductImages->product_id ]) */
                                            ?>

                                            <span>hot</span>
                                            <!--                                                        <div class="product-action">-->
                                            <!--                                                            <a class="animate-left" title="Wishlist" href="#">-->
                                            <!--                                                                <i class="pe-7s-like"></i>-->
                                            <!--                                                            </a>-->
                                            <!--                                                            <a class="animate-top" title="Add To Cart" href="#">-->
                                            <!--                                                                <i class="pe-7s-cart"></i>-->
                                            <!--                                                            </a>-->
                                            <!--                                                            <a class="animate-right" title="Quick View" data-toggle="modal" data-target="#exampleModal" href="#">-->
                                            <!--                                                                <i class="pe-7s-look"></i>-->
                                            <!--                                                            </a>-->
                                            <!--                                                        </div>-->
                                            <?php /*endforeach */?>
                                            <?= $this->Html->image($image->image, ['url' => ['controller' => 'Products', 'action' => 'normalDetail', '?' => ['product' => $relatedProduct->id]]]); ?>
                                            <span>hot</span>
                                            <div class="product-action">
                                                <!--<a class="animate-left" title="Wishlist"
                                                                           href="#">
                                                                            <i class="pe-7s-like"></i>
                                                                        </a
                                                                        <?/*= $this->Form->create(null, [
                                                                            'url' => [
                                                                                'controller' => 'order_lines',
                                                                                'action' => 'addToCart'
                                                                            ]
                                                                        ]) */?>
                                                                        <?/*= $this->form->hidden('item_id', ['value' => $item->id]); */?>
                                                                        <?/*= $this->form->hidden('retail_price', ['value' => $item->retail_price]); */?>
                                                                        <?/*= $this->form->hidden('amount', ['value' => 1]); */?>
                                                                        <?php
                                                /*                                                                        if ($shoppingCartId->count() == 0) {
                                                                                                                            */?>
                                                                            <?php /*echo $this->Html->link($this->Html->tag("i", '', array('class' => 'pe-7s-cart')), ["controller" => "Orders", "action" => "cart"], ["escape" => false]);
                                                                        } else {
                                                                            */?>

                                                                            <?php
                                                /*                                                                            foreach ($shoppingCartId as $shoppingcartid):
                                                                                                                                echo $this->form->hidden('order_id', ['value' => $shoppingcartid->order_id]);
                                                                                                                                echo $this->Form->button(__($this->Html->tag("i", '', array('class' => 'pe-7s-cart'))), ['class' => 'animate-top'], ["escape" => false]);
                                                                                                                            endforeach;

                                                                                                                        }
                                                                                                                        */?>
                                                                        <?/*= $this->Form->end() */?>-->
                                                <a class="animate-right" title="Quick View"
                                                   data-toggle="modal"
                                                   data-target="#exampleModal-<?= $relatedProduct->id ?>">
                                                    <i class="pe-7s-look"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="product-content">
                                            <h4><?php echo $this->Html->link($relatedProduct->title, ['controller' => 'Products', 'action' => 'normalDetail', '?' => ['product' => $relatedProduct->id]]); ?></h4>
                                            <span>$ <?= $item->retail_price ?></span>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        <?php endforeach ?>
                    <?php endforeach ?>
            </div>
    </div>
</div>
<?php
foreach ($relatedProducts as $relatedProduct ):
    foreach ($relatedProduct['items'] as $item):
        foreach ($relatedProduct['productimages'] as $image):
            ?>

            <div class="modal fade" id="exampleModal-<?= $relatedProduct->id ?>" tabindex="-1" role="dialog" aria-hidden="true">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="pe-7s-close" aria-hidden="true"></span>
                </button>
                <div class="modal-dialog modal-quickview-width" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="qwick-view-left">
                                <div class="quick-view-learg-img">
                                    <div class="quick-view-tab-content tab-content">
                                        <div class="tab-pane active show fade" id="modal1" role="tabpanel">
                                            <?= $this->Html->image($image->image, ['alt' => 'CakePHP']) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="qwick-view-right">
                                <div class="qwick-view-content">
                                    <h5><?= $relatedProduct->title?></h5>
                                    <div class="price">
                                        <span class="new">$ <?= $item->retail_price?></span>
                                        <span class="old">$ <?= $relatedProduct->market_price?></span>
                                    </div>
                                    <p><?= $relatedProduct->body?></p>
                                    <!--                                <div class="quick-view-select">-->
                                    <!--                                    <div class="select-option-part">-->
                                    <!--                                        <label>Size*</label>-->
                                    <!--                                        <select class="select">-->
                                    <!--                                            <option value="">- Please Select -</option>-->
                                    <!--                                            <option value="900">900</option>-->
                                    <!--                                            <option value="">700</option>-->
                                    <!--                                        </select>-->
                                    <!--                                    </div>-->
                                    <!--                                    <div class="select-option-part">-->
                                    <!--                                        <label>Colour*</label>-->
                                    <!--                                        <select class="select">-->
                                    <!--                                            <option value="">- Please Select -</option>-->
                                    <!--                                            <option value="">orange</option>-->
                                    <!--                                            <option value="">pink</option>-->
                                    <!--                                            <option value="">yellow</option>-->
                                    <!--                                        </select>-->
                                    <!--                                    </div>-->
                                    <!--                                </div>-->
                                    <div class="quickview-plus-minus">

                                        <div class="cart-plus-minus">
                                            <?=$this->Form->create(null, [
                                                'url' => [
                                                    'controller' => 'order_lines',
                                                    'action' => 'addToCart'
                                                ]
                                            ])?>
                                            <?=$this->form->hidden('item_id',['value'=>$item->id]);?>
                                            <?=$this->form->hidden('is_flash_sales',['value'=>1]);?>
                                            <?=$this->form->hidden('retail_price',['value'=>$item->retail_price]);?>
                                            <?= $this->Form->control(null,['name'=>'amount', 'type'=>'number','value'=>1 , 'min'=>1, 'max'=>$item->retail_quantity, 'class' => "cart-plus-minus-box"]); ?>
                                        </div>
                                        <div class="quickview-btn-cart">
                                            <?php
                                            if($shoppingCartId->count()==0){
                                                ?>
                                                <?php echo $this->Html->link('add to cart', ['controller' => 'Orders', 'action' => 'cart'],
                                                    ['id' => 'updatebtn']);
                                            }
                                            else{
                                                ?>

                                                <?php
                                                foreach ($shoppingCartId as $shoppingcartid):
                                                    echo $this->form->hidden('order_id',['value'=>$shoppingcartid->order_id]);
                                                    echo $this->Form->button(__('Add to cart'),['id' => 'updatebtn']);
                                                endforeach;

                                            }

                                            ?>
                                            <?= $this->Form->end() ?>
                                        </div>
                                        <?php if($this->request->getSession()->read('Auth')){ ?>
                                            <div class="quickview-btn-wishlist">
                                                <?=$this->Form->create(null, [
                                                    'url' => [
                                                        'controller' => 'customers_products',
                                                        'action' => 'addToWishlist'
                                                    ]
                                                ])?>
                                                <?=$this->form->hidden('product_id',['value'=>$item->product_id]);?>
                                                <?= $this->Form->button(__($this->Html->tag("i",'', array('class' => 'pe-7s-like'))),['id' => 'updatebtn']); ?>
                                                <?= $this->Form->end() ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <?php endforeach ?>
    <?php endforeach ?>
<?php endforeach ?>