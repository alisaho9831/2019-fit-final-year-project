<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PromoCode $promoCode
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Promo Codes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="promoCodes form large-9 medium-8 columns content">
    <?= $this->Form->create($promoCode) ?>
    <fieldset>
        <legend><?= __('Add Promo Code') ?></legend>
        <?php
            echo $this->Form->control('discount_type');
            echo $this->Form->control('discount_amount');
            echo $this->Form->control('start_date');
            echo $this->Form->control('end_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
