<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PromoCode $promoCode
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Promo Code'), ['action' => 'edit', $promoCode->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Promo Code'), ['action' => 'delete', $promoCode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $promoCode->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Promo Codes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Promo Code'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="promoCodes view large-9 medium-8 columns content">
    <h3><?= h($promoCode->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Promo Code') ?></th>
            <td><?= h($promoCode->promo_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount Type') ?></th>
            <td><?= h($promoCode->discount_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($promoCode->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount Amount') ?></th>
            <td><?= $this->Number->format($promoCode->discount_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($promoCode->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($promoCode->end_date) ?></td>
        </tr>
    </table>
</div>
