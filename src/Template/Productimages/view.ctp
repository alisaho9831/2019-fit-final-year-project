<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Productimage $productimage
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Productimage'), ['action' => 'edit', $productimage->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Productimage'), ['action' => 'delete', $productimage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productimage->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Productimages'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Productimage'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productimages view large-9 medium-8 columns content">
    <h3><?= h($productimage->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Product') ?></th>
            <td><?= $productimage->has('product') ? $this->Html->link($productimage->product->title, ['controller' => 'Products', 'action' => 'view', $productimage->product->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image') ?></th>
            <td><?= h($productimage->image) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Image Dir') ?></th>
            <td><?= h($productimage->image_dir) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($productimage->id) ?></td>
        </tr>
    </table>
</div>
