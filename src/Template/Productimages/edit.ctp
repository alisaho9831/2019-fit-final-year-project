<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Productimage $productimage
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $productimage->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $productimage->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Productimages'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productimages form large-9 medium-8 columns content">
    <?= $this->Form->create($productimage) ?>
    <fieldset>
        <legend><?= __('Edit Productimage') ?></legend>
        <?php
            echo $this->Form->control('product_id', ['options' => $products]);
            echo $this->Form->control('image');
            echo $this->Form->control('image_dir');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
