<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Productimage[]|\Cake\Collection\CollectionInterface $productimages
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Productimage'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="productimages index large-9 medium-8 columns content">
    <h3><?= __('Productimages') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('product_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image') ?></th>
                <th scope="col"><?= $this->Paginator->sort('image_dir') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($productimages as $productimage): ?>
            <tr>
                <td><?= $this->Number->format($productimage->id) ?></td>
                <td><?= $productimage->has('product') ? $this->Html->link($productimage->product->title, ['controller' => 'Products', 'action' => 'view', $productimage->product->id]) : '' ?></td>
                <td><?= h($productimage->image) ?></td>
                <td><?= h($productimage->image_dir) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $productimage->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productimage->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productimage->id], ['confirm' => __('Are you sure you want to delete # {0}?', $productimage->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
