<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Collection'), ['action' => 'edit', $collection->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Collection'), ['action' => 'delete', $collection->id], ['confirm' => __('Are you sure you want to delete # {0}?', $collection->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Collections'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collection'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Flash Sales'), ['controller' => 'FlashSales', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Flash Sale'), ['controller' => 'FlashSales', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Product Collections'), ['controller' => 'ProductCollections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product Collection'), ['controller' => 'ProductCollections', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Tag Collections'), ['controller' => 'TagCollections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Tag Collection'), ['controller' => 'TagCollections', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="collections view large-9 medium-8 columns content">
    <h3><?= h($collection->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($collection->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($collection->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($collection->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Menu Item') ?></th>
            <td><?= $this->Number->format($collection->menu_item) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Parent Collection') ?></th>
            <td><?= $this->Number->format($collection->parent_collection) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Flash Sales') ?></h4>
        <?php if (!empty($collection->flash_sales)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Collection Id') ?></th>
                <th scope="col"><?= __('Image') ?></th>
                <th scope="col"><?= __('Discount Type') ?></th>
                <th scope="col"><?= __('Discount Amount') ?></th>
                <th scope="col"><?= __('Start Date') ?></th>
                <th scope="col"><?= __('End Date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($collection->flash_sales as $flashSales): ?>
            <tr>
                <td><?= h($flashSales->id) ?></td>
                <td><?= h($flashSales->collection_id) ?></td>
                <td><?= h($flashSales->image) ?></td>
                <td><?= h($flashSales->discount_type) ?></td>
                <td><?= h($flashSales->discount_amount) ?></td>
                <td><?= h($flashSales->start_date) ?></td>
                <td><?= h($flashSales->end_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'FlashSales', 'action' => 'view', $flashSales->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'FlashSales', 'action' => 'edit', $flashSales->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'FlashSales', 'action' => 'delete', $flashSales->id], ['confirm' => __('Are you sure you want to delete # {0}?', $flashSales->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Product Collections') ?></h4>
        <?php if (!empty($collection->product_collections)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Product Id') ?></th>
                <th scope="col"><?= __('Collection Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($collection->product_collections as $productCollections): ?>
            <tr>
                <td><?= h($productCollections->product_id) ?></td>
                <td><?= h($productCollections->collection_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'ProductCollections', 'action' => 'view', $productCollections->product_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'ProductCollections', 'action' => 'edit', $productCollections->product_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'ProductCollections', 'action' => 'delete', $productCollections->product_id], ['confirm' => __('Are you sure you want to delete # {0}?', $productCollections->product_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Tag Collections') ?></h4>
        <?php if (!empty($collection->tag_collections)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Tag Id') ?></th>
                <th scope="col"><?= __('Collection Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($collection->tag_collections as $tagCollections): ?>
            <tr>
                <td><?= h($tagCollections->tag_id) ?></td>
                <td><?= h($tagCollections->collection_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'TagCollections', 'action' => 'view', $tagCollections->tag_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'TagCollections', 'action' => 'edit', $tagCollections->tag_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'TagCollections', 'action' => 'delete', $tagCollections->tag_id], ['confirm' => __('Are you sure you want to delete # {0}?', $tagCollections->tag_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
