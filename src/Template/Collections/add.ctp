<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Collection $collection
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Flash Sales'), ['controller' => 'FlashSales', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Flash Sale'), ['controller' => 'FlashSales', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Product Collections'), ['controller' => 'ProductCollections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Product Collection'), ['controller' => 'ProductCollections', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Tag Collections'), ['controller' => 'TagCollections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Tag Collection'), ['controller' => 'TagCollections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="collections form large-9 medium-8 columns content">
    <?= $this->Form->create($collection) ?>
    <fieldset>
        <legend><?= __('Add Collection') ?></legend>
        <?php
            echo $this->Form->control('title');
            echo $this->Form->control('description');
            echo $this->Form->control('menu_item');
            echo $this->Form->control('parent_collection');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
