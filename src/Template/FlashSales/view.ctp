<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FlashSale $flashSale
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Flash Sale'), ['action' => 'edit', $flashSale->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Flash Sale'), ['action' => 'delete', $flashSale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $flashSale->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Flash Sales'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Flash Sale'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="flashSales view large-9 medium-8 columns content">
    <h3><?= h($flashSale->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Collection') ?></th>
            <td><?= $flashSale->has('collection') ? $this->Html->link($flashSale->collection->title, ['controller' => 'Collections', 'action' => 'view', $flashSale->collection->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount Type') ?></th>
            <td><?= h($flashSale->discount_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($flashSale->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Discount Amount') ?></th>
            <td><?= $this->Number->format($flashSale->discount_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Start Date') ?></th>
            <td><?= h($flashSale->start_date) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('End Date') ?></th>
            <td><?= h($flashSale->end_date) ?></td>
        </tr>
    </table>
</div>
