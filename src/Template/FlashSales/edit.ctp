<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FlashSale $flashSale
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $flashSale->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $flashSale->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Flash Sales'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="flashSales form large-9 medium-8 columns content">
    <?= $this->Form->create($flashSale) ?>
    <fieldset>
        <legend><?= __('Edit Flash Sale') ?></legend>
        <?php
            echo $this->Form->control('collection_id', ['options' => $collections]);
            echo $this->Form->control('discount_type');
            echo $this->Form->control('discount_amount');
            echo $this->Form->control('start_date');
            echo $this->Form->control('end_date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
