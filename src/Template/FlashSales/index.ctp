<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\FlashSale[]|\Cake\Collection\CollectionInterface $flashSales
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Flash Sale'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Collections'), ['controller' => 'Collections', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Collection'), ['controller' => 'Collections', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="flashSales index large-9 medium-8 columns content">
    <h3><?= __('Flash Sales') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('collection_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('discount_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('discount_amount') ?></th>
                <th scope="col"><?= $this->Paginator->sort('start_date') ?></th>
                <th scope="col"><?= $this->Paginator->sort('end_date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($flashSales as $flashSale): ?>
            <tr>
                <td><?= $this->Number->format($flashSale->id) ?></td>
                <td><?= $flashSale->has('collection') ? $this->Html->link($flashSale->collection->title, ['controller' => 'Collections', 'action' => 'view', $flashSale->collection->id]) : '' ?></td>
                <td><?= h($flashSale->discount_type) ?></td>
                <td><?= $this->Number->format($flashSale->discount_amount) ?></td>
                <td><?= h($flashSale->start_date) ?></td>
                <td><?= h($flashSale->end_date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $flashSale->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $flashSale->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $flashSale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $flashSale->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
