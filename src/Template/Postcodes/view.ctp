<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Postcode $postcode
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Postcode'), ['action' => 'edit', $postcode->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Postcode'), ['action' => 'delete', $postcode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $postcode->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Postcodes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Postcode'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="postcodes view large-9 medium-8 columns content">
    <h3><?= h($postcode->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($postcode->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Postcode') ?></th>
            <td><?= $this->Number->format($postcode->postcode) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Metro') ?></th>
            <td><?= $postcode->is_metro ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
