<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Postcode $postcode
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $postcode->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $postcode->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Postcodes'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="postcodes form large-9 medium-8 columns content">
    <?= $this->Form->create($postcode) ?>
    <fieldset>
        <legend><?= __('Edit Postcode') ?></legend>
        <?php
            echo $this->Form->control('postcode');
            echo $this->Form->control('is_metro');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
