<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Postcode[]|\Cake\Collection\CollectionInterface $postcodes
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Postcode'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="postcodes index large-9 medium-8 columns content">
    <h3><?= __('Postcodes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('postcode') ?></th>
                <th scope="col"><?= $this->Paginator->sort('is_metro') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($postcodes as $postcode): ?>
            <tr>
                <td><?= $this->Number->format($postcode->id) ?></td>
                <td><?= $this->Number->format($postcode->postcode) ?></td>
                <td><?= h($postcode->is_metro) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $postcode->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $postcode->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $postcode->id], ['confirm' => __('Are you sure you want to delete # {0}?', $postcode->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
