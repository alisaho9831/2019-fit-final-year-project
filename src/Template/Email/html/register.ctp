
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!--[if gte mso 15]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <title>*|MC:SUBJECT|*</title>


    <style type="text/css" media="screen">
        /* Linked Styles */
        body { padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f1f1f1; -webkit-text-size-adjust:none }
        a { color:#ff0000; text-decoration:none }
        p { padding:0 !important; margin:0 !important }
        img { -ms-interpolation-mode: bicubic; /* Allow smoother rendering of resized image in Internet Explorer */ }
        .text4 a { color: #777777 !important; text-decoration: none !important; }
        .h2-white-m-center a { color: #ffffff !important; }
        .text-white-m-center a { color: #ffffff !important; }
        .red a { color: #ff5e56 !important; }

        /* Mobile styles */
        @media only screen and (max-device-width: 480px), only screen and (max-width: 480px) {
            div[class='mobile-br-1'] { height: 1px !important; background: #e8e8e8 !important; display: block !important; }
            div[class='mobile-br-5'] { height: 5px !important; }
            div[class='mobile-br-10'] { height: 10px !important; }
            div[class='mobile-br-15'] { height: 15px !important; }

            th[class='m-td'],
            td[class='m-td'],
            div[class='hide-for-mobile'],
            span[class='hide-for-mobile'] { display: none !important; width: 0 !important; height: 0 !important; font-size: 0 !important; line-height: 0 !important; min-height: 0 !important; }

            span[class='mobile-block'] { display: block !important; }
            div[class='h2-white-m-center'],
            div[class='text-white-m-center'],
            div[class='h2-m-center'],
            div[class='text-m-center'],
            div[class='text-r-m-center'],
            div[class='text-right'] { text-align: left !important; }

            div[class='img-m-center'] { text-align: center !important; }

            div[class='fluid-img'] img,
            td[class='fluid-img'] img { width: 100% !important; max-width: 100% !important; height: auto !important; }
            div[class='fluid-img-logo'] img { width: 100% !important; max-width: 260px !important; height: auto !important; }

            table[class='mobile-shell'] { width: 100% !important; min-width: 100% !important; }
            table[class='center'] { margin: 0 auto; }

            th[class='column-top'],
            th[class='column'] { float: left !important; width: 100% !important; display: block !important; }
            td[class='td'] { width: 100% !important; min-width: 100% !important; }

            td[class='content-spacing'] { width: 15px !important; }
        }
    </style>
</head>
<body class="body" style="padding:0 !important; margin:0 !important; display:block !important; min-width:100% !important; width:100% !important; background:#f1f1f1; -webkit-text-size-adjust:none">
<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#f1f1f1">
    <tr>
        <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1"></td>
        <td align="center" valign="top">
            <table width="650" border="0" cellspacing="0" cellpadding="0" class="mobile-shell">

                <td height="38" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td>

                <tr>
                    <td class="td" style="width:650px; min-width:650px; font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; Margin:0">

                        <!-- Section 8 -->
                        <div mc:repeatable="Select" mc:variant="Section 8">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
                                <tr>
                                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                        <!-- Article Image On the Left -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff" dir="rtl" style="direction: rtl;">
                                            <tr>
                                                <!-- Column -->
                                                <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0" dir="ltr" style="direction: ltr;">
                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td align="left">
                                                                <div class="h3" style="color:#2c2c2c; font-family:Arial,sans-serif; font-size:18px; line-height:24px; text-align:left"><div mc:edit="text_47">EMAIL VERIFICATION </div></div>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="14" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#e8e8e8" class="border" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td bgcolor="#e8e8e8" height="1" class="border" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>


                                                                <!-- List -->
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="text-list3" style="color:#777777; font-family:Arial,sans-serif; font-size:12px; line-height:20px; text-align:left; text-transform:uppercase"><div mc:edit="text_48">Hi  <?= $name ?>,</div></div>
                                                                            <div class="text-list3" style="color:#777777; font-family:Arial,sans-serif; font-size:12px; line-height:20px; text-align:left; text-transform:uppercase"><div mc:edit="text_49">Please confirm your email link below:</div></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                                                <!-- END List -->
                                                                <!-- Button -->
                                                                <table border="0" cellspacing="0" cellpadding="0" bgcolor="#ff5e56" class="mobile-shell">
                                                                    <tr>
                                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
                                                                        <td>
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="12" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                                                            <div class="text-button" style="color:#ffffff; font-family:Arial,sans-serif; font-size:12px; line-height:16px; text-align:center; text-transform:uppercase">
                                                                                <div mc:edit="text_51">
                                                                                    <a href="#" target="_blank" class="link-white" style="color:#ffffff; text-decoration:none">
                                                                                        <span class="link-white" style="color:#ffffff; text-decoration:none">
                                                                                            <?=
                                                                                            $this->Html->link('Verify account',
                                                                                                ['controller' => 'users', 'action' => 'verification', $token,'_full' => true]
                                                                                            );
                                                                                            ?>
                                                                                        </span></a>
                                                                                </div>
                                                                            </div>
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="12" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                                                        </td>
                                                                        <td class="img" style="font-size:0pt; line-height:0pt; text-align:left" width="20"></td>
                                                                    </tr>
                                                                </table>
                                                                <!-- END Button -->
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="16" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                    <tr>
                                                                        <td>
                                                                            <div class="text-list3" style="color:#777777; font-family:Arial,sans-serif; font-size:12px; line-height:20px; text-align:left; text-transform:uppercase"><div mc:edit="text_50">Thank you for joining us!<span class="hide-for-mobile"><br /></span></div></div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="8" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </th>
                                                <!-- END Column -->
                                            </tr>
                                        </table>
                                        <!-- END Article Image On the Left -->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                    </td>
                                    <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                        </div>
                        <!-- END Section 8 -->

                        <!-- Footer -->
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ff5e56">
                            <tr>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <!-- Column -->
                                            <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="h2-white-m-center" style="color:#ffffff; font-family:Arial,sans-serif; font-size:22px; line-height:26px; text-align:left; font-weight:bold; text-transform:uppercase"><div mc:edit="text_52">Smooth Sales</div></div>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="4" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                                            <div class="text-white-m-center" style="color:#ffffff; font-family:Arial,sans-serif; font-size:12px; line-height:20px; text-align:left"><div mc:edit="text_53">Crazy Deals. Every Day.</div></div>
                                                            <div style="font-size:0pt; line-height:0pt;" class="mobile-br-15"></div>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </th>
                                            <!-- END Column -->

                                        </tr>
                                    </table>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="26" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <!-- Column -->
                                            <th class="column-top" style="font-size:0pt; line-height:0pt; padding:0; margin:0; font-weight:normal; vertical-align:top; Margin:0" width="320">
                                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td>
                                                            <div class="text-white-m-center" style="color:#ffffff; font-family:Arial,sans-serif; font-size:12px; line-height:20px; text-align:left">
                                                                <div mc:edit="text_54">
                                                                    <a href="#" target="_blank" class="link-white" style="color:#ffffff; text-decoration:none"><span class="link-white" style="color:#ffffff; text-decoration:none">E. info@smoothsales.com.au</span></a> <br />W. www.smoothsales.com.au <a href="" target="_blank" class="link-white" style="color:#ffffff; text-decoration:none"><span class="link-white" style="color:#ffffff; text-decoration:none"></span></a>
                                                                </div>
                                                            </div>
                                                            <div style="font-size:0pt; line-height:0pt;" class="mobile-br-15"></div>

                                                        </td>
                                                    </tr>
                                                </table>
                                            </th>
                                            <!-- END Column -->

                                        </tr>
                                    </table>

                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="30" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                                </td>
                                <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="30"></td>
                            </tr>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%"><tr><td height="20" class="spacer" style="font-size:0pt; line-height:0pt; text-align:center; width:100%; min-width:100%">&nbsp;</td></tr></table>

                        <!-- END Footer -->
                    </td>
                </tr>
            </table>
        </td>
        <td class="content-spacing" style="font-size:0pt; line-height:0pt; text-align:left" width="1"></td>
    </tr>
</table>
</body>
</html>
