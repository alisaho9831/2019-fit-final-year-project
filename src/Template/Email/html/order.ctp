<h2>Hi Admin:</h2>
<p>You Got a new order.</p>
<br>
<p>Customer information:</p>
<table style="border: 1px solid black;">
    <tr>
        <td style="border: 1px solid black;">first name</td>
        <td style="border: 1px solid black;">last name</td>
        <td style="border: 1px solid black;">email</td>
        <td style="border: 1px solid black;">phone number</td>
    </tr>
    <tr>
        <td style="border: 1px solid black;"><?php echo $orderdetail[0]['customer']['first_name'];   ?></td>
        <td style="border: 1px solid black;"><?php echo $orderdetail[0]['customer']['last_name'];   ?></td>
        <td style="border: 1px solid black;"><?php echo $orderdetail[0]['customer']['email'];   ?></td>
        <td style="border: 1px solid black;"><?php echo $orderdetail[0]['customer']['phone1'];   ?></td>
    </tr>
</table>

<br>
<br>
<p>Order information</p>
<table style="border: 1px solid black;">
    <tr >
        <td style="border: 1px solid black;">product</td>
        <td style="border: 1px solid black;">price</td>
        <td style="border: 1px solid black;">amount</td>
        <td style="border: 1px solid black;">inner/outer</td>
    </tr>
    <?php
    $sum=0;
    foreach($orderLinedetails as $order){ ?>
    <tr >
        <td style="border: 1px solid black;"><?php echo $order['Products']['title'];   ?></td>
        <td style="border: 1px solid black;"><?php echo $order['price'];   ?></td>
        <td style="border: 1px solid black;"><?php echo $order['amount'];   ?></td>
        <?php if ($order['is_inner_carton'] == 1){?>
            <td style="border: 1px solid black;">Inner</td>
        <?php }elseif ($order['is_inner_carton'] == 0){?>
        <td style="border: 1px solid black;">Outer</td>
        <?php }?>
    </tr>
    <?php
    $sum += $order['price'];
    } ?>
</table>
<p>Total price: <?php echo $sum ?></p>
