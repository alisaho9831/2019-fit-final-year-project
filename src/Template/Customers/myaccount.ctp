
<div class="section-heading text-center"></div>
<?= $this->Flash->render() ?>
<div class="row">

    <div class="customers form large-9 medium-8 columns content col-md-8 offset-md-2">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-8 col-md-6 col-sm-4">
                        <h3>My Account</h3>
                    </div>
                </div>
            </div>

             <?php
            foreach ( $myaccount_customers as $myaccount_customer):
            ?>
            <div class="card-body">
                <div class="form-group">
                    <b> First Name</b>
                    <p><?= h($myaccount_customer->first_name) ?></p>
                </div>
                <div class="form-group">
                    <b> Last Name</b>
                    <p><?= h($myaccount_customer->last_name) ?></p>
                </div>
                <div class="form-group">
                    <b> Email</b>
                    <p><?= h($myaccount_customer->username) ?></p>
                </div>
                <div class="form-group">
                    <b> Phone</b>
                    <p><?= h($myaccount_customer->phone1) ?></p>
                    <p><?= h($myaccount_customer->phone2) ?></p>
                </div>
                <br>
                <?php endforeach?>
                <div>
                    <?= $this->Html->link("Change Password", ['action' => 'changepw']); ?>

                </div>

            </div>


        </div>
    </div>
</div>
<br/>

