<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Customer'), ['action' => 'edit', $customer->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Customer'), ['action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Customers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Customer'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Addresses'), ['controller' => 'Addresses', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Address'), ['controller' => 'Addresses', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Products'), ['controller' => 'Products', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Product'), ['controller' => 'Products', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="customers view large-9 medium-8 columns content">
    <h3><?= h($customer->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($customer->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($customer->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($customer->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Verify Code') ?></th>
            <td><?= h($customer->verify_code) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Company') ?></th>
            <td><?= h($customer->company) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone1') ?></th>
            <td><?= h($customer->phone1) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone2') ?></th>
            <td><?= h($customer->phone2) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Note') ?></th>
            <td><?= h($customer->note) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Abn') ?></th>
            <td><?= h($customer->abn) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $customer->has('user') ? $this->Html->link($customer->user->id, ['controller' => 'Users', 'action' => 'view', $customer->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($customer->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Spent') ?></th>
            <td><?= $this->Number->format($customer->total_spent) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Orders') ?></th>
            <td><?= $this->Number->format($customer->total_orders) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Wholesale Discount') ?></th>
            <td><?= $this->Number->format($customer->wholesale_discount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Verified') ?></th>
            <td><?= $customer->is_verified ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Accepts Marketing') ?></th>
            <td><?= $customer->accepts_marketing ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Deactivated') ?></th>
            <td><?= $customer->is_deactivated ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Is Wholesale') ?></th>
            <td><?= $customer->is_wholesale ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Products') ?></h4>
        <?php if (!empty($customer->products)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Brand Name') ?></th>
                <th scope="col"><?= __('Title') ?></th>
                <th scope="col"><?= __('Url') ?></th>
                <th scope="col"><?= __('Body') ?></th>
                <th scope="col"><?= __('Market Price') ?></th>
                <th scope="col"><?= __('Cost Per Item') ?></th>
                <th scope="col"><?= __('Seo Description') ?></th>
                <th scope="col"><?= __('Is Deactivated') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($customer->products as $products): ?>
            <tr>
                <td><?= h($products->id) ?></td>
                <td><?= h($products->brand_name) ?></td>
                <td><?= h($products->title) ?></td>
                <td><?= h($products->url) ?></td>
                <td><?= h($products->body) ?></td>
                <td><?= h($products->market_price) ?></td>
                <td><?= h($products->cost_per_item) ?></td>
                <td><?= h($products->seo_description) ?></td>
                <td><?= h($products->is_deactivated) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Addresses') ?></h4>
        <?php if (!empty($customer->addresses)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('First Name') ?></th>
                <th scope="col"><?= __('Last Name') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Address1') ?></th>
                <th scope="col"><?= __('Address2') ?></th>
                <th scope="col"><?= __('City') ?></th>
                <th scope="col"><?= __('State') ?></th>
                <th scope="col"><?= __('Country') ?></th>
                <th scope="col"><?= __('Post Code') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($customer->addresses as $addresses): ?>
            <tr>
                <td><?= h($addresses->id) ?></td>
                <td><?= h($addresses->customer_id) ?></td>
                <td><?= h($addresses->first_name) ?></td>
                <td><?= h($addresses->last_name) ?></td>
                <td><?= h($addresses->email) ?></td>
                <td><?= h($addresses->address1) ?></td>
                <td><?= h($addresses->address2) ?></td>
                <td><?= h($addresses->city) ?></td>
                <td><?= h($addresses->state) ?></td>
                <td><?= h($addresses->country) ?></td>
                <td><?= h($addresses->post_code) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Addresses', 'action' => 'view', $addresses->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Addresses', 'action' => 'edit', $addresses->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Addresses', 'action' => 'delete', $addresses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $addresses->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Orders') ?></h4>
        <?php if (!empty($customer->orders)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Email') ?></th>
                <th scope="col"><?= __('Financial Status') ?></th>
                <th scope="col"><?= __('Paid Date') ?></th>
                <th scope="col"><?= __('Fulfillment Status') ?></th>
                <th scope="col"><?= __('Fulfillment Date') ?></th>
                <th scope="col"><?= __('Subtotal') ?></th>
                <th scope="col"><?= __('Taxes Cost') ?></th>
                <th scope="col"><?= __('Shipping Cost') ?></th>
                <th scope="col"><?= __('Total Cost') ?></th>
                <th scope="col"><?= __('Discount Code') ?></th>
                <th scope="col"><?= __('Discount Amount') ?></th>
                <th scope="col"><?= __('Shipping Method') ?></th>
                <th scope="col"><?= __('Order Date') ?></th>
                <th scope="col"><?= __('Shipping Address Id') ?></th>
                <th scope="col"><?= __('Billing Address Id') ?></th>
                <th scope="col"><?= __('Promo Code Id') ?></th>
                <th scope="col"><?= __('Payment Method') ?></th>
                <th scope="col"><?= __('Order Status') ?></th>
                <th scope="col"><?= __('Tracking Number') ?></th>
                <th scope="col"><?= __('Shipping Company') ?></th>
                <th scope="col"><?= __('Notes') ?></th>
                <th scope="col"><?= __('Is Deactivated') ?></th>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('Is Shopping Cart') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($customer->orders as $orders): ?>
            <tr>
                <td><?= h($orders->id) ?></td>
                <td><?= h($orders->email) ?></td>
                <td><?= h($orders->financial_status) ?></td>
                <td><?= h($orders->paid_date) ?></td>
                <td><?= h($orders->fulfillment_status) ?></td>
                <td><?= h($orders->fulfillment_date) ?></td>
                <td><?= h($orders->subtotal) ?></td>
                <td><?= h($orders->taxes_cost) ?></td>
                <td><?= h($orders->shipping_cost) ?></td>
                <td><?= h($orders->total_cost) ?></td>
                <td><?= h($orders->discount_code) ?></td>
                <td><?= h($orders->discount_amount) ?></td>
                <td><?= h($orders->shipping_method) ?></td>
                <td><?= h($orders->order_date) ?></td>
                <td><?= h($orders->shipping_address_id) ?></td>
                <td><?= h($orders->billing_address_id) ?></td>
                <td><?= h($orders->promo_code_id) ?></td>
                <td><?= h($orders->payment_method) ?></td>
                <td><?= h($orders->order_status) ?></td>
                <td><?= h($orders->tracking_number) ?></td>
                <td><?= h($orders->shipping_company) ?></td>
                <td><?= h($orders->notes) ?></td>
                <td><?= h($orders->is_deactivated) ?></td>
                <td><?= h($orders->customer_id) ?></td>
                <td><?= h($orders->is_shopping_cart) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
