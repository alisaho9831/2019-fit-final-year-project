<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\OrderLine $orderLine
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Order Line'), ['action' => 'edit', $orderLine->order_id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Order Line'), ['action' => 'delete', $orderLine->order_id], ['confirm' => __('Are you sure you want to delete # {0}?', $orderLine->order_id)]) ?> </li>
        <li><?= $this->Html->link(__('List Order Lines'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order Line'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Orders'), ['controller' => 'Orders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Order'), ['controller' => 'Orders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['controller' => 'Items', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['controller' => 'Items', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="orderLines view large-9 medium-8 columns content">
    <h3><?= h($orderLine->order_id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Order') ?></th>
            <td><?= $orderLine->has('order') ? $this->Html->link($orderLine->order->id, ['controller' => 'Orders', 'action' => 'view', $orderLine->order->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Item') ?></th>
            <td><?= $orderLine->has('item') ? $this->Html->link($orderLine->item->id, ['controller' => 'Items', 'action' => 'view', $orderLine->item->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fulfillment Status') ?></th>
            <td><?= h($orderLine->fulfillment_status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Price') ?></th>
            <td><?= $this->Number->format($orderLine->price) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Amount') ?></th>
            <td><?= $this->Number->format($orderLine->amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Expiry Date') ?></th>
            <td><?= h($orderLine->expiry_date) ?></td>
        </tr>
    </table>
</div>
