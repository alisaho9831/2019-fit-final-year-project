<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PromoCodes Model
 *
 * @method \App\Model\Entity\PromoCode get($primaryKey, $options = [])
 * @method \App\Model\Entity\PromoCode newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PromoCode[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PromoCode|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PromoCode patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PromoCode[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PromoCode findOrCreate($search, callable $callback = null, $options = [])
 */
class PromoCodesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('promo_codes');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('promo_code')
            ->maxLength('promo_code', 255)
            ->requirePresence('promo_code', 'create')
            ->notEmpty('promo_code')
            ->add('promo_code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('discount_type')
            ->maxLength('discount_type', 255)
            ->requirePresence('discount_type', 'create')
            ->notEmpty('discount_type');

        $validator
            ->numeric('discount_amount')
            ->requirePresence('discount_amount', 'create')
            ->notEmpty('discount_amount');

        $validator
            ->date('start_date')
            ->requirePresence('start_date', 'create')
            ->notEmpty('start_date');

        $validator
            ->date('end_date')
            ->requirePresence('end_date', 'create')
            ->notEmpty('end_date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['promo_code']));

        return $rules;
    }
}
