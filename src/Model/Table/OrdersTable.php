<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Orders Model
 *
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\BelongsTo $Addresses
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\BelongsTo $Addresses
 * @property \App\Model\Table\PromoCodesTable|\Cake\ORM\Association\BelongsTo $PromoCodes
 * @property \App\Model\Table\CustomersTable|\Cake\ORM\Association\BelongsTo $Customers
 * @property |\Cake\ORM\Association\HasMany $Admins
 * @property \App\Model\Table\OrderLinesTable|\Cake\ORM\Association\HasMany $Orderlines
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 */
class OrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Addresses', [
            'foreignKey' => 'shipping_address_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Addresses', [
            'foreignKey' => 'billing_address_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PromoCodes', [
            'foreignKey' => 'promo_code_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Admins', [
            'foreignKey' => 'order_id'
        ]);
        $this->hasMany('Orderlines', [
            'foreignKey' => 'order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('financial_status')
            ->maxLength('financial_status', 20)
            ->requirePresence('financial_status', 'create')
            ->notEmpty('financial_status');

        $validator
            ->date('paid_date')
            ->requirePresence('paid_date', 'create')
            ->notEmpty('paid_date');

        $validator
            ->scalar('fulfillment_status')
            ->maxLength('fulfillment_status', 20)
            ->requirePresence('fulfillment_status', 'create')
            ->notEmpty('fulfillment_status');

        $validator
            ->date('fulfillment_date')
            ->requirePresence('fulfillment_date', 'create')
            ->notEmpty('fulfillment_date');

        $validator
            ->numeric('subtotal')
            ->requirePresence('subtotal', 'create')
            ->notEmpty('subtotal');

        $validator
            ->numeric('taxes_cost')
            ->requirePresence('taxes_cost', 'create')
            ->notEmpty('taxes_cost');

        $validator
            ->numeric('shipping_cost')
            ->requirePresence('shipping_cost', 'create')
            ->notEmpty('shipping_cost');

        $validator
            ->numeric('total_cost')
            ->requirePresence('total_cost', 'create')
            ->notEmpty('total_cost');

        $validator
            ->scalar('discount_code')
            ->maxLength('discount_code', 255)
            ->requirePresence('discount_code', 'create')
            ->notEmpty('discount_code');

        $validator
            ->numeric('discount_amount')
            ->requirePresence('discount_amount', 'create')
            ->notEmpty('discount_amount');

        $validator
            ->scalar('shipping_method')
            ->maxLength('shipping_method', 255)
            ->requirePresence('shipping_method', 'create')
            ->notEmpty('shipping_method');

        $validator
            ->date('order_date')
            ->requirePresence('order_date', 'create')
            ->notEmpty('order_date');

        $validator
            ->scalar('payment_method')
            ->maxLength('payment_method', 255)
            ->requirePresence('payment_method', 'create')
            ->notEmpty('payment_method');

        $validator
            ->scalar('order_status')
            ->maxLength('order_status', 255)
            ->requirePresence('order_status', 'create')
            ->notEmpty('order_status');

        $validator
            ->scalar('tracking_number')
            ->maxLength('tracking_number', 255)
            ->requirePresence('tracking_number', 'create')
            ->notEmpty('tracking_number');

        $validator
            ->scalar('shipping_company')
            ->maxLength('shipping_company', 255)
            ->requirePresence('shipping_company', 'create')
            ->notEmpty('shipping_company');

        $validator
            ->scalar('notes')
            ->maxLength('notes', 500)
            ->requirePresence('notes', 'create')
            ->notEmpty('notes');

        $validator
            ->boolean('is_deactivated')
            ->requirePresence('is_deactivated', 'create')
            ->notEmpty('is_deactivated');

        $validator
            ->boolean('is_shopping_cart')
            ->requirePresence('is_shopping_cart', 'create')
            ->notEmpty('is_shopping_cart');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['shipping_address_id'], 'Addresses'));
        $rules->add($rules->existsIn(['billing_address_id'], 'Addresses'));
        $rules->add($rules->existsIn(['promo_code_id'], 'PromoCodes'));
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));

        return $rules;
    }
}
