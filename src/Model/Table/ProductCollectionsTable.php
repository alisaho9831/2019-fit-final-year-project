<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductCollections Model
 *
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\CollectionsTable|\Cake\ORM\Association\BelongsTo $Collections
 *
 * @method \App\Model\Entity\ProductCollection get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProductCollection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProductCollection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProductCollection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProductCollection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProductCollection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProductCollection findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductCollectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('product_collections');
        $this->setDisplayField('product_id');
        $this->setPrimaryKey(['product_id', 'collection_id']);

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Collections', [
            'foreignKey' => 'collection_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['collection_id'], 'Collections'));

        return $rules;
    }
}
