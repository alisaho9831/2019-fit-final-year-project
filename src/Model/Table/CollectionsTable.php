<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Behavior\TreeBehavior;

/**
 * Collections Model
 *
 * @property \App\Model\Table\FlashSalesTable|\Cake\ORM\Association\HasMany $FlashSales
 * @property \App\Model\Table\ProductCollectionsTable|\Cake\ORM\Association\HasMany $ProductCollections
 * @property \App\Model\Table\TagCollectionsTable|\Cake\ORM\Association\HasMany $TagCollections
 *
 * @method \App\Model\Entity\Collection get($primaryKey, $options = [])
 * @method \App\Model\Entity\Collection newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Collection[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Collection|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Collection patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Collection[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Collection findOrCreate($search, callable $callback = null, $options = [])
 */
class CollectionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {


        parent::initialize($config);
//        $this->addBehavior('Tree', [
//            'parent' => 'parent_collection', // Use this instead of parent_id
//            'left' => 'id',
//            'right' => 'parent_collection'
//        ]);

        $this->setTable('collections');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->hasMany('FlashSales', [
            'foreignKey' => 'collection_id'
        ]);
        $this->hasMany('ProductCollections', [
            'foreignKey' => 'collection_id'
        ]);
        $this->hasMany('TagCollections', [
            'foreignKey' => 'collection_id'
        ]);;
        $this->hasMany('ChildrenCollections', [
            'className' => 'Collections',
            'foreignKey' => 'parent_collection'
        ]);;
        $this->belongsTo('ParentCollections', [
            'className' => 'Collections',
            'foreignKey' => 'parent_collection'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('description')
            ->maxLength('description', 1000)
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->integer('menu_item')
            ->requirePresence('menu_item', 'create')
            ->notEmpty('menu_item');

        $validator
            ->integer('parent_collection')
            ->requirePresence('parent_collection', 'create')
            ->notEmpty('parent_collection');

        return $validator;
    }
}
