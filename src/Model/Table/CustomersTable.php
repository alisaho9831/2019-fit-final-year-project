<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Customers Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\AddressesTable|\Cake\ORM\Association\HasMany $Addresses
 * @property \App\Model\Table\OrdersTable|\Cake\ORM\Association\HasMany $Orders
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsToMany $Products
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null, $options = [])
 */
class CustomersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Addresses', [
            'foreignKey' => 'customer_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'customer_id'
        ]);
        $this->belongsToMany('Products', [
            'foreignKey' => 'customer_id',
            'targetForeignKey' => 'product_id',
            'joinTable' => 'customers_products'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 255)
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        $validator
            ->scalar('last_name')
            ->maxLength('last_name', 255)
            ->requirePresence('last_name', 'create')
            ->notEmpty('last_name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('verify_code')
            ->maxLength('verify_code', 10)
            ->requirePresence('verify_code', 'create')
            ->notEmpty('verify_code');

        $validator
            ->boolean('is_verified')
            ->requirePresence('is_verified', 'create')
            ->notEmpty('is_verified');

        $validator
            ->scalar('company')
            ->maxLength('company', 255)
            ->requirePresence('company', 'create')
            ->notEmpty('company');

        $validator
            ->scalar('phone1')
            ->maxLength('phone1', 255)
            ->requirePresence('phone1', 'create')
            ->notEmpty('phone1');

        $validator
            ->scalar('phone2')
            ->maxLength('phone2', 255)
            ->requirePresence('phone2', 'create')
            ->notEmpty('phone2');

        $validator
            ->boolean('accepts_marketing')
            ->requirePresence('accepts_marketing', 'create')
            ->notEmpty('accepts_marketing');

        $validator
            ->boolean('is_deactivated')
            ->requirePresence('is_deactivated', 'create')
            ->notEmpty('is_deactivated');

        $validator
            ->numeric('total_spent')
            ->requirePresence('total_spent', 'create')
            ->notEmpty('total_spent');

        $validator
            ->integer('total_orders')
            ->requirePresence('total_orders', 'create')
            ->notEmpty('total_orders');

        $validator
            ->boolean('is_wholesale')
            ->requirePresence('is_wholesale', 'create')
            ->notEmpty('is_wholesale');

        $validator
            ->scalar('note')
            ->maxLength('note', 500)
            ->requirePresence('note', 'create')
            ->notEmpty('note');

        $validator
            ->scalar('abn')
            ->maxLength('abn', 11)
            ->requirePresence('abn', 'create')
            ->notEmpty('abn');

        $validator
            ->numeric('wholesale_discount')
            ->requirePresence('wholesale_discount', 'create')
            ->notEmpty('wholesale_discount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
