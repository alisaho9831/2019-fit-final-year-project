<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Products Model
 *
 * @property \App\Model\Table\ItemsTable|\Cake\ORM\Association\HasMany $Items
 * @property |\Cake\ORM\Association\HasMany $Productimages
 * @property |\Cake\ORM\Association\BelongsToMany $Collections
 * @property |\Cake\ORM\Association\BelongsToMany $Customers
 * @property |\Cake\ORM\Association\BelongsToMany $Tags
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 */
class ProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->hasMany('Items', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasMany('Productimages', [
            'foreignKey' => 'product_id'
        ]);
        $this->belongsToMany('Collections', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'collection_id',
            'joinTable' => 'collections_products'
        ]);
        $this->belongsToMany('Customers', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'customer_id',
            'joinTable' => 'customers_products'
        ]);
        $this->belongsToMany('Tags', [
            'foreignKey' => 'product_id',
            'targetForeignKey' => 'tag_id',
            'joinTable' => 'products_tags'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('brand_name')
            ->maxLength('brand_name', 255)
            ->requirePresence('brand_name', 'create')
            ->notEmpty('brand_name');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->scalar('url')
            ->maxLength('url', 255)
            ->requirePresence('url', 'create')
            ->notEmpty('url')
            ->add('url', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('body')
            ->maxLength('body', 2000)
            ->requirePresence('body', 'create')
            ->notEmpty('body');

        $validator
            ->numeric('market_price')
            ->requirePresence('market_price', 'create')
            ->notEmpty('market_price');

        $validator
            ->numeric('cost_per_item')
            ->requirePresence('cost_per_item', 'create')
            ->notEmpty('cost_per_item');

        $validator
            ->scalar('seo_description')
            ->maxLength('seo_description', 160)
            ->requirePresence('seo_description', 'create')
            ->notEmpty('seo_description');

        $validator
            ->boolean('is_deactivated')
            ->requirePresence('is_deactivated', 'create')
            ->notEmpty('is_deactivated');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['url']));

        return $rules;
    }
}
