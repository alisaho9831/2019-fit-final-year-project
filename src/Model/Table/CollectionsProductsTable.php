<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CollectionsProducts Model
 *
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\CollectionsTable|\Cake\ORM\Association\BelongsTo $Collections
 *
 * @method \App\Model\Entity\CollectionsProduct get($primaryKey, $options = [])
 * @method \App\Model\Entity\CollectionsProduct newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CollectionsProduct[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CollectionsProduct|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CollectionsProduct patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CollectionsProduct[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CollectionsProduct findOrCreate($search, callable $callback = null, $options = [])
 */
class CollectionsProductsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('collections_products');
        $this->setDisplayField('product_id');
        $this->setPrimaryKey(['product_id', 'collection_id']);

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Collections', [
            'foreignKey' => 'collection_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['collection_id'], 'Collections'));

        return $rules;
    }
}
