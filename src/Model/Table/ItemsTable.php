<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Items Model
 *
 * @property \App\Model\Table\ProductsTable|\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\OrderLinesTable|\Cake\ORM\Association\HasMany $Orderlines
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Orderlines', [
            'foreignKey' => 'item_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('barcode')
            ->maxLength('barcode', 30)
            ->requirePresence('barcode', 'create')
            ->notEmpty('barcode')
            ->add('barcode', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('option_name')
            ->maxLength('option_name', 255)
            ->requirePresence('option_name', 'create')
            ->notEmpty('option_name');

        $validator
            ->scalar('option_value')
            ->maxLength('option_value', 255)
            ->requirePresence('option_value', 'create')
            ->notEmpty('option_value');

        $validator
            ->integer('retail_quantity')
            ->requirePresence('retail_quantity', 'create')
            ->notEmpty('retail_quantity');

        $validator
            ->integer('wholesale_quantity')
            ->requirePresence('wholesale_quantity', 'create')
            ->notEmpty('wholesale_quantity');

        $validator
            ->numeric('inner_carton_size')
            ->requirePresence('inner_carton_size', 'create')
            ->notEmpty('inner_carton_size');

        $validator
            ->numeric('outer_carton_size')
            ->requirePresence('outer_carton_size', 'create')
            ->notEmpty('outer_carton_size');

        $validator
            ->numeric('wholesale_price')
            ->requirePresence('wholesale_price', 'create')
            ->notEmpty('wholesale_price');

        $validator
            ->numeric('retail_price')
            ->requirePresence('retail_price', 'create')
            ->notEmpty('retail_price');

        $validator
            ->numeric('weight')
            ->requirePresence('weight', 'create')
            ->notEmpty('weight');

        $validator
            ->boolean('is_wholesale')
            ->requirePresence('is_wholesale', 'create')
            ->notEmpty('is_wholesale');

        $validator
            ->boolean('is_retail')
            ->requirePresence('is_retail', 'create')
            ->notEmpty('is_retail');

        $validator
            ->boolean('published_wholesale')
            ->requirePresence('published_wholesale', 'create')
            ->notEmpty('published_wholesale');

        $validator
            ->boolean('published_retail')
            ->requirePresence('published_retail', 'create')
            ->notEmpty('published_retail');

        $validator
            ->boolean('is_deactivated')
            ->requirePresence('is_deactivated', 'create')
            ->notEmpty('is_deactivated');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['barcode']));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }
}
