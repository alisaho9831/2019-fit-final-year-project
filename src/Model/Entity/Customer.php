<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Customer Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $verify_code
 * @property bool $is_verified
 * @property string $company
 * @property string $phone1
 * @property string $phone2
 * @property bool $accepts_marketing
 * @property bool $is_deactivated
 * @property float $total_spent
 * @property int $total_orders
 * @property bool $is_wholesale
 * @property string $note
 * @property string $abn
 * @property float $wholesale_discount
 * @property int $user_id
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\Order[] $orders
 * @property \App\Model\Entity\Product[] $products
 */
class Customer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'email' => true,
        'verify_code' => true,
        'is_verified' => true,
        'company' => true,
        'phone1' => true,
        'phone2' => true,
        'accepts_marketing' => true,
        'is_deactivated' => true,
        'total_spent' => true,
        'total_orders' => true,
        'is_wholesale' => true,
        'note' => true,
        'abn' => true,
        'wholesale_discount' => true,
        'user_id' => true,
        'user' => true,
        'addresses' => true,
        'orders' => true,
        'products' => true
    ];
}
