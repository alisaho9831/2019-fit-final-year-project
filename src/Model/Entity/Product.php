<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $brand_name
 * @property string $title
 * @property string $url
 * @property string $body
 * @property float $market_price
 * @property float $cost_per_item
 * @property string $seo_description
 * @property bool $is_deactivated
 *
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\ProductCollection[] $product_collections
 * @property \App\Model\Entity\Productimage[] $product_images
 * @property \App\Model\Entity\ProductTag[] $product_tags
 */
class Product extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'brand_name' => true,
        'title' => true,
        'url' => true,
        'body' => true,
        'market_price' => true,
        'cost_per_item' => true,
        'seo_description' => true,
        'is_deactivated' => true,
        'items' => true,
        'product_collections' => true,
        'product_images' => true,
        'product_tags' => true,
        'is_inner_carton'=>true
    ];
}
