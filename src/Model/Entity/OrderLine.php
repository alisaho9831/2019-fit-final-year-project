<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OrderLine Entity
 *
 * @property int $order_id
 * @property int $item_id
 * @property string $fulfillment_status
 * @property float $price
 * @property int $amount
 * @property \Cake\I18n\FrozenTime $expiry_date
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\Item $item
 */
class OrderLine extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fulfillment_status' => true,
        'price' => true,
        'amount' => true,
        'expiry_date' => true,
        'order' => true,
        'item' => true
    ];
}
