<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property string $barcode
 * @property int $product_id
 * @property string $option_name
 * @property string $option_value
 * @property int $retail_quantity
 * @property int $wholesale_quantity
 * @property float $inner_carton_size
 * @property float $outer_carton_size
 * @property float $wholesale_price
 * @property float $retail_price
 * @property float $weight
 * @property bool $is_wholesale
 * @property bool $is_retail
 * @property bool $published_wholesale
 * @property bool $published_retail
 * @property bool $is_deactivated
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\OrderLine[] $order_lines
 */
class Item extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'barcode' => true,
        'product_id' => true,
        'option_name' => true,
        'option_value' => true,
        'retail_quantity' => true,
        'wholesale_quantity' => true,
        'inner_carton_size' => true,
        'outer_carton_size' => true,
        'wholesale_price' => true,
        'retail_price' => true,
        'weight' => true,
        'is_wholesale' => true,
        'is_retail' => true,
        'published_wholesale' => true,
        'published_retail' => true,
        'is_deactivated' => true,
        'product' => true,
        'order_lines' => true
    ];
}
