<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Postcode Entity
 *
 * @property int $id
 * @property int $postcode
 * @property bool $is_metro
 */
class Postcode extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'postcode' => true,
        'is_metro' => true
    ];
}
