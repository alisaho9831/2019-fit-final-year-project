<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property string $email
 * @property string $financial_status
 * @property \Cake\I18n\FrozenDate $paid_date
 * @property string $fulfillment_status
 * @property \Cake\I18n\FrozenDate $fulfillment_date
 * @property float $subtotal
 * @property float $taxes_cost
 * @property float $shipping_cost
 * @property float $total_cost
 * @property string $discount_code
 * @property float $discount_amount
 * @property string $shipping_method
 * @property \Cake\I18n\FrozenDate $order_date
 * @property int $shipping_address_id
 * @property int $billing_address_id
 * @property int $promo_code_id
 * @property string $payment_method
 * @property string $order_status
 * @property string $tracking_number
 * @property string $shipping_company
 * @property string $notes
 * @property bool $is_deactivated
 * @property int $customer_id
 * @property bool $is_shopping_cart
 *
 * @property \App\Model\Entity\Address $address
 * @property \App\Model\Entity\PromoCode $promo_code
 * @property \App\Model\Entity\Customer $customer
 * @property \App\Model\Entity\OrderLine[] $order_lines
 */
class Order extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'email' => true,
        'financial_status' => true,
        'paid_date' => true,
        'fulfillment_status' => true,
        'fulfillment_date' => true,
        'subtotal' => true,
        'taxes_cost' => true,
        'shipping_cost' => true,
        'total_cost' => true,
        'discount_code' => true,
        'discount_amount' => true,
        'shipping_method' => true,
        'order_date' => true,
        'shipping_address_id' => true,
        'billing_address_id' => true,
        'promo_code_id' => true,
        'payment_method' => true,
        'order_status' => true,
        'tracking_number' => true,
        'shipping_company' => true,
        'notes' => true,
        'is_deactivated' => true,
        'customer_id' => true,
        'is_shopping_cart' => true,
        'address' => true,
        'promo_code' => true,
        'customer' => true,
        'order_lines' => true
    ];
}
