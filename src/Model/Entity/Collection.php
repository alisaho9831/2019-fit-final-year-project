<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Collection Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $menu_item
 * @property int $parent_collection
 *
 * @property \App\Model\Entity\FlashSale[] $flash_sales
 * @property \App\Model\Entity\ProductCollection[] $product_collections
 * @property \App\Model\Entity\TagCollection[] $tag_collections
 */
class Collection extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'title' => true,
        'description' => true,
        'menu_item' => true,
        'parent_collection' => true,
        'flash_sales' => true,
        'product_collections' => true,
        'tag_collections' => true
    ];
}
