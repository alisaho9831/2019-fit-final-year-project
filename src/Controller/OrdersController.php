<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\ORM\Query;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Addresses', 'PromoCodes', 'Customers']
        ];
        $orders = $this->paginate($this->Orders);

        $this->set(compact('orders'));
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Addresses', 'PromoCodes', 'Customers', 'Orderlines']
        ]);

        $this->set('order', $order);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $addresses = $this->Orders->Addresses->find('list', ['limit' => 200]);
        $promoCodes = $this->Orders->PromoCodes->find('list', ['limit' => 200]);
        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $this->set(compact('order', 'addresses', 'promoCodes', 'customers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->getData());
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The order has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order could not be saved. Please, try again.'));
        }
        $addresses = $this->Orders->Addresses->find('list', ['limit' => 200]);
        $promoCodes = $this->Orders->PromoCodes->find('list', ['limit' => 200]);
        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $this->set(compact('order', 'addresses', 'promoCodes', 'customers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The order has been deleted.'));
        } else {
            $this->Flash->error(__('The order could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function cart()
    {
        $this->loadModel('order_lines');
        $this->loadModel('orders');
        $this->loadModel('products');
        $this->loadModel('items');

        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

        $customer_id = $this->Orders->find()
            ->select([
                'customer_id',
            ])
            ->where(['email' => $email, 'is_shopping_cart' => 1]);
        $this->set('customer_id', $customer_id);

        $orders = $this->orders->find()
            ->where(['customer_id' => $customer_id, 'is_shopping_cart' => 1]);
        $this->set('orders', $orders);

        $shoppingcart = $this->order_lines->find()
            ->select([
                'orders_id' => 'orders.id',
                'products_id' => 'products.id',
                'items_id' => 'items.id',
                'price' => 'order_lines.price',
                'amount' => 'order_lines.amount',
                'retail_price' => 'items.retail_price',
                'retail_quantity' => 'items.retail_quantity',
                'title' => 'products.title',
                'body' => 'products.body',
                'image' => 'productimages.image',
                'weight' => 'items.weight'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = items.product_id',
                ]
            ])
            ->join([
                'table' => 'productimages',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = productimages.product_id',
                ]
            ]);
        $this->set('shopping_cart_items', $shoppingcart);

        $sum = $this->order_lines->find()
            ->select([
                'sum' => 'SUM(price)',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.email' => $this->Auth->user('username'),
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ]);
        $this->set('subtotal', $sum);


    }



    public function wholesaleCart()
    {
        $this->loadModel('order_lines');
        $this->loadModel('orders');
        $this->loadModel('products');
        $this->loadModel('items');

        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

        $customer_id = $this->Orders->find()
            ->select([
                'customer_id',
            ])
            ->where(['email' => $email, 'is_shopping_cart' => 1]);
        $this->set('customer_id', $customer_id);

        $orders = $this->orders->find()
            ->where(['customer_id' => $customer_id, 'is_shopping_cart' => 1]);
        $this->set('orders', $orders);

        $shoppingcart = $this->order_lines->find()
            ->select([
                'orders_id' => 'orders.id',
                'is_inner_carton'=>'order_lines.is_inner_carton',
                'products_id' => 'products.id',
                'items_id' => 'items.id',
                'price' => 'order_lines.price',
                'amount' => 'order_lines.amount',
                'carton_amount' => 'order_lines.carton_amount',
                'wholesale_price' => 'items.wholesale_price',
                'wholesale_quantity' => 'items.wholesale_quantity',
                'inner_carton_size'=>'items.inner_carton_size',
                'outer_carton_size'=>'items.outer_carton_size',
                'title' => 'products.title',
                'image' => 'productimages.image',
                'body' => 'products.body'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = items.product_id',
                ]
            ])
            ->join([
                'table' => 'productimages',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = productimages.product_id',
                ]
            ]);;
        $this->set('shopping_cart_items', $shoppingcart);

        $sum = $this->order_lines->find()
            ->select([
                'sum' => 'SUM(price)',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.email' => $this->Auth->user('username'),
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ]);
        $this->set('subtotal', $sum);


    }

    public function shippingaddress()
    {

        $states_list = [
            'ACT' => 'Australian Capital Territory',
            'NSW' => 'New South Wales',
            'NT' => 'Northern Territory',
            'QLD' => 'Queensland',
            'SA' => 'South Australia',
            'TAS' => 'Tasmania',
            'VIC' => 'Victoria',
            'WA' => 'Western Australia',
        ];

        $this->set('states', $states_list);

        $country_list = [
            'AUS' => '	Australia',
        ];

        $this->set('country', $country_list);

        //shopping cart item
        $this->loadModel('order_lines');
        $this->loadModel('orders');
        $this->loadModel('products');
        $this->loadModel('items');

        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

        $customer_id = $this->Orders->find()
            ->select([
                'customer_id',
            ])
            ->where(['email' => $email, 'is_shopping_cart' => 1]);
        $this->set('customer_id', $customer_id);

        $orders = $this->orders->find()
            ->where(['customer_id' => $customer_id, 'is_shopping_cart' => 1]);
        $this->set('orders', $orders);

        $shoppingcart = $this->order_lines->find()
            ->select([
                'orders_id' => 'orders.id',
                'products_id' => 'products.id',
                'items_id' => 'items.id',
                'price' => 'order_lines.price',
                'amount' => 'order_lines.amount',
                'retail_price' => 'items.retail_price',
                'retail_quantity' => 'items.retail_quantity',
                'title' => 'products.title',
                'body' => 'products.body',
                'shipping_method' => 'shipping_method',
                'weight' => 'items.weight',
                'tax' => 'orders.taxes_cost'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = items.product_id',
                ]
            ]);
        $this->set('shopping_cart_items', $shoppingcart);

        $sum = $this->order_lines->find()
            ->select([
                'sum' => 'SUM(price)',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.email' => $this->Auth->user('username'),
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ]);
        $this->set('subtotal', $sum);
        //shopping cart item end

        $Weight = $this->order_lines->find()
            ->select([
                'total' => 'items.weight',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ]);
        $this->set('weight', $Weight);

        $shippingAddress = $this->order_lines->find()
            ->select([
                'postcode' => 'addresses.post_code'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'addresses',
                'type' => 'INNER',
                'conditions' => [
                    'addresses.id = orders.shipping_address_id',
                ]
            ]);
        $postcodes = $shippingAddress->distinct()->toArray();
        $this->set('postcode', $postcodes);


    }

    public function payment()
    {
        $order_id=$this->request->query['order'];
        //$this->layout = '';
        //shopping cart item
        $this->loadModel('order_lines');
        $this->loadModel('orders');
        $this->loadModel('products');
        $this->loadModel('items');
        $this->loadModel('addresses');

        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

        $customer_id = $this->Orders->find()
            ->select([
                'customer_id',
            ])
            ->where(['email' => $email]);
        $this->set('customer_id', $customer_id);

        $orders = $this->Orders->find()
            ->where(['orders.id' => $order_id]);
        $this->set('orders', $orders);

        $shoppingcart = $this->order_lines->find()
            ->select([
                'orders_id' => 'orders.id',
                'products_id' => 'products.id',
                'items_id' => 'items.id',
                'price' => 'order_lines.price',
                'amount' => 'order_lines.amount',
                'retail_price' => 'items.retail_price',
                'retail_quantity' => 'items.retail_quantity',
                'title' => 'products.title',
                'body' => 'products.body'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.id' => $order_id,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = items.product_id',
                ]
            ]);
        $this->set('shopping_cart_items', $shoppingcart);

        $sum = $this->order_lines->find()
            ->select([
                'sum' => 'SUM(price)',
                'tax' => 'orders.taxes_cost',
                'shippingCost' => 'orders.shipping_cost',
                'shippingMethod' => 'orders.shipping_method',
                'total' => 'orders.total_cost',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.id' => $order_id,
                    'orders.id = order_lines.order_id'],
            ]);
        $this->set('subtotal', $sum);
        //shopping cart item end

        $shipping_id = $this->Orders->find()
            ->select(['shipping_address_id' => 'shipping_address_id'])
            ->where(['id' => $order_id]);

        $shipping = $this->addresses->find()
            ->where(['id' => $shipping_id]);
        $this->set('shippingaddress', $shipping);

        $billing_id = $this->Orders->find()
            ->select(['billing_address_id' => 'billing_address_id'])
            ->where(['id' => $order_id]);

        $billing = $this->addresses->find()
            ->where(['id' => $billing_id]);
        $this->set('billingaddress', $billing);


    }
    public function wholesalePayment()
    {
        $order_id=$this->request->query['order'];
        //$this->layout = '';
        //shopping cart item
        $this->loadModel('order_lines');
        $this->loadModel('orders');
        $this->loadModel('products');
        $this->loadModel('items');
        $this->loadModel('addresses');

        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

        $customer_id = $this->Orders->find()
            ->select([
                'customer_id',
            ])
            ->where(['email' => $email]);
        $this->set('customer_id', $customer_id);

        $orders = $this->Orders->find()
            ->where(['orders.id' => $order_id]);
        $this->set('orders', $orders);

        $shoppingcart = $this->order_lines->find()
            ->select([
                'orders_id' => 'orders.id',
                'products_id' => 'products.id',
                'items_id' => 'items.id',
                'price' => 'order_lines.price',
                'amount' => 'order_lines.amount',
                'carton_amount' => 'order_lines.carton_amount',
                'retail_price' => 'items.retail_price',
                'retail_quantity' => 'items.retail_quantity',
                'title' => 'products.title',
                'body' => 'products.body'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.id' => $order_id,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = items.product_id',
                ]
            ]);
        $this->set('shopping_cart_items', $shoppingcart);

        $sum = $this->order_lines->find()
            ->select([
                'sum' => 'SUM(price)',
                'tax' => 'orders.taxes_cost',
                'shippingCost' => 'orders.shipping_cost',
                'shippingMethod' => 'orders.shipping_method',
                'total' => 'orders.total_cost',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.id' => $order_id,
                    'orders.id = order_lines.order_id'],
            ]);
        $this->set('subtotal', $sum);
        //shopping cart item end

        $shipping_id = $this->Orders->find()
            ->select(['shipping_address_id' => 'shipping_address_id'])
            ->where(['id' => $order_id]);

        $shipping = $this->addresses->find()
            ->where(['id' => $shipping_id]);
        $this->set('shippingaddress', $shipping);

        $billing_id = $this->Orders->find()
            ->select(['billing_address_id' => 'billing_address_id'])
            ->where(['id' => $order_id]);

        $billing = $this->addresses->find()
            ->where(['id' => $billing_id]);
        $this->set('billingaddress', $billing);


    }

    public function addShippingMethod()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order_id = $this->request->getData('order_id');
            $order = $this->Orders->get($order_id, [
                'contain' => []
            ]);

            $metroShipping = $this->request->getData('metroShipping');
            $regionalShipping = $this->request->getData('regionalShipping');

            $postcode = $this->request->getData('postcode');
            $tax_cost = $this->request->getData('tax_cost');
            $addressesTable = TableRegistry::getTableLocator()->get('addresses');
            $shipping_address_id = $this->Orders->find()
                ->select([
                    'shipping_address_id',
                ])
                ->where(['id' => $order_id]);
            $this->set('shipping_address_id', $shipping_address_id);

            $address = $addressesTable->get([$shipping_address_id], [
                'contain' => []
            ]);
            $address->post_code = $postcode;

            $order->taxes_cost = $tax_cost;
            if ($metroShipping == 'StandardMetro') {
                $order->shipping_method = 'Standard Metro';
            } elseif ($metroShipping == 'ExpressMetro') {
                $order->shipping_method = 'Express Metro';
            } elseif ($regionalShipping == 'StandardRegional') {
                $order->shipping_method = 'Standard Regional';
            } elseif ($regionalShipping == 'ExpressRegional') {
                $order->shipping_method = 'Express Regional';
            }


            if ($this->Orders->save($order) && $addressesTable->save($address)) {
                return $this->redirect(['controller' => 'orders', 'action' => 'shippingaddress']);
            }

        }
    }

    public function completePayment(){

    }
    public function wholesaleShippingaddress()
    {

        $states_list = [
            'ACT' => 'Australian Capital Territory',
            'NSW' => 'New South Wales',
            'NT' => 'Northern Territory',
            'QLD' => 'Queensland',
            'SA' => 'South Australia',
            'TAS' => 'Tasmania',
            'VIC' => 'Victoria',
            'WA' => 'Western Australia',
        ];

        $this->set('states', $states_list);

        $country_list = [
            'AUS' => '	Australia',
        ];

        $this->set('country', $country_list);

        //shopping cart item
        $this->loadModel('order_lines');
        $this->loadModel('orders');
        $this->loadModel('products');
        $this->loadModel('items');

        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

        $customer_id = $this->Orders->find()
            ->select([
                'customer_id',
            ])
            ->where(['email' => $email]);
        $this->set('customer_id', $customer_id);

        $orders = $this->orders->find()
            ->where(['customer_id' => $customer_id, 'is_shopping_cart' => 1]);
        $this->set('orders', $orders);

        $shoppingcart = $this->order_lines->find()
            ->select([
                'orders_id' => 'orders.id',
                'products_id' => 'products.id',
                'items_id' => 'items.id',
                'price' => 'order_lines.price',
                'amount' => 'order_lines.amount',
                'carton_amount' => 'order_lines.carton_amount',
                'retail_price' => 'items.retail_price',
                'retail_quantity' => 'items.retail_quantity',
                'title' => 'products.title',
                'body' => 'products.body',
                'shipping_method' => 'shipping_method',
                'weight' => 'items.weight',
                'tax' => 'orders.taxes_cost',
                'is_inner_carton'=>'order_lines.is_inner_carton'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = items.product_id',
                ]
            ]);
        $this->set('shopping_cart_items', $shoppingcart);

        $sum = $this->order_lines->find()
            ->select([
                'sum' => 'SUM(price)',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'orders.email' => $this->Auth->user('username'),
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ]);
        $this->set('subtotal', $sum);
        //shopping cart item end

        $Weight = $this->order_lines->find()
            ->select([
                'total' => 'items.weight',
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.id = order_lines.item_id',
                ]
            ]);
        $this->set('weight', $Weight);

        $shippingAddress = $this->order_lines->find()
            ->select([
                'postcode' => 'addresses.post_code'
            ])
            ->join([
                'table' => 'orders',
                'type' => 'INNER',
                'conditions' => [
                    'customer_id' => $customer_id,
                    'is_shopping_cart' => 1,
                    'orders.id = order_lines.order_id'],
            ])
            ->join([
                'table' => 'addresses',
                'type' => 'INNER',
                'conditions' => [
                    'addresses.id = orders.shipping_address_id',
                ]
            ]);
        $postcodes = $shippingAddress->distinct()->toArray();

        $this->set('postcode', $postcodes);


    }

//    public function wholesaleAddShippingMethod()
//    {
//        if ($this->request->is(['patch', 'post', 'put'])) {
//            $order_id = $this->request->getData('order_id');
//            $order = $this->Orders->get($order_id, [
//                'contain' => []
//            ]);
//
//            $metroShipping = $this->request->getData('metroShipping');
//            $regionalShipping = $this->request->getData('regionalShipping');
//
//            $postcode = $this->request->getData('postcode');
//            $tax_cost = $this->request->getData('tax_cost');
//            $addressesTable = TableRegistry::getTableLocator()->get('addresses');
//            $shipping_address_id = $this->Orders->find()
//                ->select([
//                    'shipping_address_id',
//                ])
//                ->where(['id' => $order_id]);
//            $this->set('shipping_address_id', $shipping_address_id);
//
//            $address = $addressesTable->get([$shipping_address_id], [
//                'contain' => []
//            ]);
//            $address->post_code = $postcode;
//
//            $order->taxes_cost = $tax_cost;
//            if ($metroShipping == 'StandardMetro') {
//                $order->shipping_method = 'Standard Metro';
//            } elseif ($metroShipping == 'ExpressMetro') {
//                $order->shipping_method = 'Express Metro';
//            } elseif ($regionalShipping == 'StandardRegional') {
//                $order->shipping_method = 'Standard Regional';
//            } elseif ($regionalShipping == 'ExpressRegional') {
//                $order->shipping_method = 'Express Regional';
//            }
//
//
//            if ($this->Orders->save($order) && $addressesTable->save($address)) {
//                return $this->redirect(['controller' => 'orders', 'action' => 'wholesale_shippingaddress']);
//            }
//
//        }
//    }



}
