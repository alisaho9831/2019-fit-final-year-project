<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Controller\Component\FlashComponent;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\ORM\Behavior\TreeBehavior;
use Cake\ORM\Entity;



/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
   // var $components = array('Auth');
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadModel('Collections');
//        $collections = TableRegistry::getTableLocator()->get('Collections');
//        $collections->recover();


        //$subquery = $collections
            //->find('children', ['for' => 1])
            //->where(['menu_item' => 1]);

        $mainquery = $this->Collections->find()
            ->where(['parent_collection' => 1 ,'menu_item' => 1]);
        $this->set('mainMenu',$mainquery);

        $subquery = $this->Collections->find()
            ->where(['menu_item' => 1,'parent_collection !=' => 1  ]);
        $this->set('submenu', $subquery);

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'username', 'password' => 'password'],
                    'userModel' => 'Users'
                ]
            ],

            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutAction' => [
                'controller' => 'Users',
                'action' => 'logout'
            ],
            'storage' => 'Session'


        ]);

        //users will not be able to access cart, wishilist, checkout page when they are not logged in
        $this->Auth->allow();
        $this->Auth->deny(["cart","wishlist","checkout"]);


        /*
         * Enable the following components for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
    }

    public function beforeFilter(Event $event)
    {
        //if user logged in, change login button to my account button
        if($this->Auth->user()) {
            $this->loadModel('Customers');
            $isWholeSale=$this->Customers->find('all',['conditions'=>['user_id'=>$this->Auth->user('id')]])->toList()[0]['is_wholesale'];
            if($isWholeSale==1){
                $this->set('url',['controller' => 'Customers', 'action' => 'wholesale_myaccount']);
            }
            else{
                $this->set('url',['controller' => 'Customers', 'action' => 'myaccount']);
            }
            $this->set('text','My Account');
            $this->set('userID', $this->Auth->user('id'));

        }
        else{
            $this->set('text','Login');
            $this->set('url',['controller' => 'Users', 'action' => 'login']);
        }
        return parent::beforeFilter($event);
    }


    function beforeRender(Event $event) {
        $this->loadComponent('Auth');
//        $this->set("isLoggedIn", $this->request->getSession()->read("Auth.User") !== NULL);
////
////        $user = $this->Auth->User();
////        $userType = $this->Auth->User('type');
////
////        $this->set('user', $user);
////        $this->set('userType', $userType);
////        if(
////            /*$this->request->getRequestTarget() === Router::url(["action" => "report"])*/
////            $this->isJsonRequest() || $this->isCsvRequest()
////
////        ){
////            $this->viewBuilder()->setLayout('default');
////        }
////        elseif (
////            $this->request->getSession()->read("Auth.User") !== NULL
////            && $this->request->getRequestTarget() !== $this->request->getAttribute('webroot')
////        ){
////            $this->viewBuilder()->setClassName('AdminLTE.AdminLTE');
////            $this->viewBuilder()->setTheme('AdminLTE');
////            $this->viewBuilder()->setLayout('adminlte');
////            $this->set('theme', Configure::read('Theme'));
////        }
////
////        if (!array_key_exists('_serialize',$this->viewVars) &&
////            in_array($this->response->type(),['application/json', 'application/xml'])
////        ){
////            $this->set('_serialize',ture);
////        }

        //login check
        if($this->request->getSession()->read('Auth.User')){
            $this->set('loggedIn', true);

        }else{
            $this->set('loggedIn', false);
        }




    }



}
