<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductTags Controller
 *
 * @property \App\Model\Table\ProductTagsTable $ProductTags
 *
 * @method \App\Model\Entity\ProductTag[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductTagsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products']
        ];
        $productTags = $this->paginate($this->ProductTags);

        $this->set(compact('productTags'));
    }

    /**
     * View method
     *
     * @param string|null $id Product Tag id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productTag = $this->ProductTags->get($id, [
            'contain' => ['Products']
        ]);

        $this->set('productTag', $productTag);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productTag = $this->ProductTags->newEntity();
        if ($this->request->is('post')) {
            $productTag = $this->ProductTags->patchEntity($productTag, $this->request->getData());
            if ($this->ProductTags->save($productTag)) {
                $this->Flash->success(__('The product tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product tag could not be saved. Please, try again.'));
        }
        $products = $this->ProductTags->Products->find('list', ['limit' => 200]);
        $this->set(compact('productTag', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Tag id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productTag = $this->ProductTags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productTag = $this->ProductTags->patchEntity($productTag, $this->request->getData());
            if ($this->ProductTags->save($productTag)) {
                $this->Flash->success(__('The product tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product tag could not be saved. Please, try again.'));
        }
        $products = $this->ProductTags->Products->find('list', ['limit' => 200]);
        $this->set(compact('productTag', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Tag id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productTag = $this->ProductTags->get($id);
        if ($this->ProductTags->delete($productTag)) {
            $this->Flash->success(__('The product tag has been deleted.'));
        } else {
            $this->Flash->error(__('The product tag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
