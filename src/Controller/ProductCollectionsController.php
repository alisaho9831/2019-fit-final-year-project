<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ProductCollections Controller
 *
 * @property \App\Model\Table\ProductCollectionsTable $ProductCollections
 *
 * @method \App\Model\Entity\ProductCollection[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductCollectionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Collections']
        ];
        $productCollections = $this->paginate($this->ProductCollections);

        $this->set(compact('productCollections'));
    }

    /**
     * View method
     *
     * @param string|null $id Product Collection id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productCollection = $this->ProductCollections->get($id, [
            'contain' => ['Products', 'Collections']
        ]);

        $this->set('productCollection', $productCollection);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productCollection = $this->ProductCollections->newEntity();
        if ($this->request->is('post')) {
            $productCollection = $this->ProductCollections->patchEntity($productCollection, $this->request->getData());
            if ($this->ProductCollections->save($productCollection)) {
                $this->Flash->success(__('The product collection has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product collection could not be saved. Please, try again.'));
        }
        $products = $this->ProductCollections->Products->find('list', ['limit' => 200]);
        $collections = $this->ProductCollections->Collections->find('list', ['limit' => 200]);
        $this->set(compact('productCollection', 'products', 'collections'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Collection id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productCollection = $this->ProductCollections->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productCollection = $this->ProductCollections->patchEntity($productCollection, $this->request->getData());
            if ($this->ProductCollections->save($productCollection)) {
                $this->Flash->success(__('The product collection has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product collection could not be saved. Please, try again.'));
        }
        $products = $this->ProductCollections->Products->find('list', ['limit' => 200]);
        $collections = $this->ProductCollections->Collections->find('list', ['limit' => 200]);
        $this->set(compact('productCollection', 'products', 'collections'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Collection id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productCollection = $this->ProductCollections->get($id);
        if ($this->ProductCollections->delete($productCollection)) {
            $this->Flash->success(__('The product collection has been deleted.'));
        } else {
            $this->Flash->error(__('The product collection could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
