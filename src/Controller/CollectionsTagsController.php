<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CollectionsTags Controller
 *
 * @property \App\Model\Table\CollectionsTagsTable $CollectionsTags
 *
 * @method \App\Model\Entity\CollectionsTag[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CollectionsTagsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Tags', 'Collections']
        ];
        $collectionsTags = $this->paginate($this->CollectionsTags);

        $this->set(compact('collectionsTags'));
    }

    /**
     * View method
     *
     * @param string|null $id Collections Tag id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $collectionsTag = $this->CollectionsTags->get($id, [
            'contain' => ['Tags', 'Collections']
        ]);

        $this->set('collectionsTag', $collectionsTag);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $collectionsTag = $this->CollectionsTags->newEntity();
        if ($this->request->is('post')) {
            $collectionsTag = $this->CollectionsTags->patchEntity($collectionsTag, $this->request->getData());
            if ($this->CollectionsTags->save($collectionsTag)) {
                $this->Flash->success(__('The collections tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The collections tag could not be saved. Please, try again.'));
        }
        $tags = $this->CollectionsTags->Tags->find('list', ['limit' => 200]);
        $collections = $this->CollectionsTags->Collections->find('list', ['limit' => 200]);
        $this->set(compact('collectionsTag', 'tags', 'collections'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Collections Tag id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $collectionsTag = $this->CollectionsTags->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $collectionsTag = $this->CollectionsTags->patchEntity($collectionsTag, $this->request->getData());
            if ($this->CollectionsTags->save($collectionsTag)) {
                $this->Flash->success(__('The collections tag has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The collections tag could not be saved. Please, try again.'));
        }
        $tags = $this->CollectionsTags->Tags->find('list', ['limit' => 200]);
        $collections = $this->CollectionsTags->Collections->find('list', ['limit' => 200]);
        $this->set(compact('collectionsTag', 'tags', 'collections'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Collections Tag id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $collectionsTag = $this->CollectionsTags->get($id);
        if ($this->CollectionsTags->delete($collectionsTag)) {
            $this->Flash->success(__('The collections tag has been deleted.'));
        } else {
            $this->Flash->error(__('The collections tag could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
