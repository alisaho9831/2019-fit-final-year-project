<?php
namespace App\Controller;

use Cake\I18n\Time;

class ArticlesController extends AppController
{

    public function home()
    {
        $this->loadModel('flash_sales');

        $searchCollection = $this->Collections->find()
            ->where(['menu_item'=>1]);
        $this->set('searchCategories',$searchCollection);

        $time = Time::now();

        $flashsales = $this->flash_sales->find()
            ->where(['flash_sales.start_date <=' => $time, 'flash_sales.end_date >=' => $time]);
        $this->set('flashsales',$flashsales);

        $flashsales1 = $this->flash_sales->find()
            ->where(['flash_sales.start_date <=' => $time, 'flash_sales.end_date >=' => $time])
            ->first();
        $this->set('firstFS',$flashsales1);


    }

    public function wholesaleHome()
    {
        $searchCollection = $this->Collections->find()
            ->where(['menu_item'=>1]);
        $this->set('searchCategories',$searchCollection);
    }
}
