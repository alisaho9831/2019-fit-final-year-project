<?php
namespace App\Controller;

use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Date;
use Cake\ORM\TableRegistry;
use App\Controller\AppController;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Cake\Mailer\TransportFactory;
use Cake\ORM\Table;
use App\Model\Entity\Role;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @property \App\Model\Table\CustomersTable $Customers
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Customers']
        ]);

        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //Retail and wholesale customer login
    public function login()
    {
        if ($this->request->is('post')) {
            if ($this->Auth->user()) {
                $this->Flash->error('You are already logged in');
                return null;
                // TODO: Remember to put the already logged in user to another page
            } else {
                $user = $this->Auth->identify();
                if ($user) {
                    $this->Auth->setUser($user);
                    $user_id = $this->Auth->user('id');
                    $this->loadModel('Customers');
                    $customer = $this->Customers->find()->where(['user_id' => $user_id])->first();
                    if (isset($customer)) {
                        $customer_is_wholesale = $customer->is_wholesale;
                        if ($customer_is_wholesale == 1) {
                            // Remember to comment here
                                return $this->redirect(['controller' => 'Articles', 'action' => 'wholesale_home']);
                        } else {
                            // Remember to comment here
                            return $this->redirect(['controller' => 'Articles', 'action' => 'Home']);
                        }
                    } else {
                        $this->Flash->error('Invalid username or password, try again');
                        return null;
                    }
                }
            }
        }
    }


    public function logout(){
        $this->Flash->success(__('You are logged out.'));
        return $this->redirect($this->Auth->logout());
    }


    //register as a customer
    public function signup()
    {
        $user = $this->Users->newEntity();
        $customersTable = TableRegistry::getTableLocator()->get('customers');
        $customer = $customersTable->newEntity();
        $addressesTable = TableRegistry::getTableLocator()->get('addresses');
        $address = $addressesTable->newEntity();
        $ordersTable = TableRegistry::getTableLocator()->get('orders');
        $order = $ordersTable->newEntity();


        if ($this->request->is('post')) {

            //load data from sign up form
            $enteredEmail = $this->request->getData('username');
            $enteredEmail2 = $this->request->getData('username2');
            $pwd1= $this->request->getData('password');
            $pwd2= $this->request->getData('pwd2');
            $firstName= $this->request->getData('first_name');
            $lastName= $this->request->getData('last_name');


            //check if account with this email has already been created
            $condition = ['username' => $enteredEmail];
            $email_exists = $this->Users->exists($condition);

            if ($email_exists == true) {
                $this->Flash->error(__('An account with this email has already been created'));
            } elseif ($enteredEmail!=$enteredEmail2){
                $this->Flash->error(__('Emails do no match.'));
            } elseif ($pwd1!=$pwd2){
                $this->Flash->error(__('Passwords do no match.'));
            }
            else {
                //if the email has not been used before, create account
                $user = $this->Users->newEntity();
                $user->username = $enteredEmail;
                $user->password =$pwd1; //security
                $customer->verify_code = Security::hash(Security::randomBytes(32));
                $user->role = 'Retail Customer';
                $user->created = date('Y-m-d H:i:s');
                $user->modified = date('Y-m-d H:i:s');

                if ($this->Users->save($user)) {

                    $userid = $user->id;
                    $verifycode = substr(str_shuffle('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'),0, 1).substr(str_shuffle('aBcEeFgHiJkLmNoPqRstUvWxYz0123456789'),0, 9);

                    $customer->first_name = $firstName;
                    $customer->last_name = $lastName;
                    $customer->email = $enteredEmail;
                    $customer->company = '';
                    $customer->phone1 = '';
                    $customer->phone2 = '';
                    $customer->accepts_marketing = 0;
                    $customer->total_spent = 0;
                    $customer->total_orders = 0;
                    $customer->is_wholesale = 0;
                    $customer->note = '';
                    $customer->user_id = $userid;
                    $customer->abn = '';
                    $customer->accepts_marketing = 1;
                    $customer->is_verified = 0;
                    $customer->verify_code = $verifycode;
                    $customer->wholesale_discount = 0;
                    $customer->is_deactivated = 0;



                    if ($customersTable->save($customer)) {
                        // The $customer entity contains the id now
                        $customerid = $customer->id;

                        $address->customer_id = $customerid;
                        $address->address1 = '';
                        $address->address2 = '';
                        $address->city = '';
                        $address->state = '';
                        $address->country = '';
                        $address->post_code = 0;
                        $address->first_name = $firstName;
                        $address->last_name = $lastName;
                        $address->email = $enteredEmail;
                        $address->is_deactivated = 0;

                        if($addressesTable->save($address)){
                            $addressid= $address->id;

                            $order->email = $enteredEmail;
                            $order->financial_status = '';
                            $order->paid_date = new Date('1990-01-01');
                            $order->fulfillment_status = 'Not fulfilled';
                            $order->fulfillment_date = new Date('1990-01-01');
                            $order->subtotal = 0;
                            $order->taxes_cost = 0;
                            $order->shipping_cost = 0;
                            $order->total_cost = 0;
                            $order->discount_code = '';
                            $order->discount_amount = 0;
                            $order->shipping_method = '';
                            $order->order_date = new Date('1990-01-01');
                            $order->shipping_address_id = $addressid;
                            $order->billing_address_id = $addressid;
                            $order->promo_code_id = 1;
                            $order->payment_method = '';
                            $order->order_status = 'Not an order';
                            $order->notes = '';
                            $order->customer_id = $customerid;
                            $order->is_shopping_cart = 1;
                            $order->is_deactivated = 0;
                            $order->shipping_company = "";
                            $order->tracking_number = "";

                            if($ordersTable->save($order)){
                                $this->Flash->set('Registration successful, your confirmation email has been sent to ' . $this->request->getData('username'), ['element' => 'success']);

                                TransportFactory::setConfig('gmail', [
                                    'host' => 'smtp.gmail.com',
                                    'port' => 587,
                                    'username' => 'smoothsales2019@gmail.com',
                                    'password' => 'IE2019team8',
                                    'className' => 'Smtp',
                                    'tls' => true
                                ]);

                                //send a verification email to customer
                                $email = new Email('default');//
                                $email->setProfile(['from' => 'smoothsales2019@gmail.com', 'transport'=>'gmail']);
                                //$email->setTransport('mailtrap');
                                $email->setEmailFormat('html');//
                                $email->setFrom('smoothsales2019@gmail.com', 'Test Smooth Sales');//
                                $email->setSubject('Please confirm your email to activate your account');//
                                $email->setTo($this->request->getData('username'));//
                                $email->setTemplate('register');
                                $email->setViewVars([
                                    'name' => $this->request->getData('first_name'),
                                    'token' => $customer->verify_code
                                ]);
                                $email->send();

                            } else( debug('error'));


                        }
                    }
                }
            }

        }
        $this->set(compact('user','customer','address','order'));
    }



//for wholesale customers enquiry and request for access to wholesale products page
    public function request()
    {
        if ($this->request->is('post')) {

            //get user data input from form
            $whloesaleEmail = $this->request->getData('Email');
            $whloesaleName = $this->request->getData('username');
            $whloesalePhone = $this->request->getData('Phone');
            $whloesaleABN = $this->request->getData('ABN');
            $whloesaleDescription = $this->request->getData('Description');

            $this->Flash->set('Your enquiry information has been sent', ['element' => 'success']);

            //email set up configuration
            TransportFactory::setConfig('gmail', [
                'host' => 'smtp.gmail.com',
                'port' => 587,
                'timeout' => 30,
                'username' => 'smoothsales2019@gmail.com',
                'password' => 'IE2019team8',
                'className' => 'Smtp',
                'tls'=>true
            ]);

            //send wholesale enquiry detail through email to business owner
            $email = new Email();
            $email->setProfile(['from' => 'smoothsales2019@gmail.com', 'transport'=>'gmail']);
            //$email->setTransport('mailtrap');
            $email->setEmailFormat('html');
            $email->setFrom('smoothsales2019@gmail.com', 'Test Smooth Sales');
            $email->setSubject('Wholesale Enquiry');
            $email->setTo('zjzhang0616@gmail.com');
            $email->send('A new wholesale enquiry has been received from: '. $whloesaleName . ' <br><br>Phone Number: ' . $whloesalePhone . '<br><br> Email: ' . $whloesaleEmail. '<br><br>ABN: ' . $whloesaleABN . '<br><br>Description: ' . $whloesaleDescription . '');

            // $this->redirect(['controller' => 'Articles', 'action' => 'home']);
        }
    }

    public function forgetpwd(){
        $this->loadModel('customers');

        if ($this->request->is('post')) {
            $account_email = $this->request->getData('username');

            $forgotpwd_customer = $this->Users->find()->where(['username' => $account_email])
                ->contain(['Customers'])->first();

//                    debug($forgotpwd_customer);
//                    //This is verify code
//                    debug($forgotpwd_customer->customers[0]->verify_code);
//                    //This is their last name
//                    debug($forgotpwd_customer->customers[0]->last_name);
//                    exit;

            $forgotpwd_customer_VC = $forgotpwd_customer->customers[0]->verify_code;
            $forgotpwd_customer_Fname = $forgotpwd_customer->customers[0]->first_name;

            if (!empty($forgotpwd_customer_Fname) && !empty($forgotpwd_customer_VC)){

                $this->redirect('/users/resetpwd');
                $this->Flash->set('Reset password email has been sent.', ['element' => 'success']);

                // TODO: You might want to setup mailtransport configuration in app.php instead of here.
                TransportFactory::setConfig('gmail', [
                    'host' => 'smtp.gmail.com',
                    'port' => 587,
                    'timeout' => 30,
                    'username' => 'smoothsales2019@gmail.com',
                    'password' => 'IE2019team8',
                    'className' => 'Smtp',
                    'tls'=>true
                ]);

                $email = new Email();
                $email->setProfile(['from' => 'smoothsales2019@gmail.com', 'transport'=>'gmail']);
                $email->setEmailFormat('html');
                $email->setFrom('smoothsales2019@gmail.com', 'Test Smooth Sales');
                $email->setSubject('Reset Password');
                $email->setTo($account_email);
                $email->setTemplate('forgetpwd');
                $email->setViewVars([
                    'name' => $forgotpwd_customer_Fname,
                    'token' =>$forgotpwd_customer_VC
                ]);
                $email->send();





            }

            else {
                $this->Flash->error(__('We could not find an account with this email address.'));

            }

        }
    }



    public function resetpwd(){
        $this->loadModel('customers');

        if ($this->request->is('post')) {
            $customeremail = $this->request->getData('email');
            $customer_vc = $this->request->getData('verify_code');
            $newpassword = $this->request->getData('password');
//            $confirmpwd = $this->request->getData('password_confirm');

            $resetpwd_customer = $this->Users->find()->where(['username' => $customeremail])
                ->contain(['Customers'])->first();

            $resetpwd_user_email = $resetpwd_customer->customers[0]->email;
            $resetpwd_customer_vc = $resetpwd_customer->customers[0]->verify_code;


            if (($resetpwd_user_email == $customeremail) && ( $resetpwd_customer_vc== $customer_vc)) {
//                    if($newpassword == $confirmpwd ) {
                $resetpwd_customer->verify_code = Security::hash(Security::randomBytes(32));
                $resetpwd_customer->password = $newpassword;
                $resetpwd_customer->modified = date('Y-m-d H:i:s');


                //if token and email match then update the record
                if ($this->Users->save($resetpwd_customer) ) {
                    $this->Flash->set('Your password has been reset', ['element' => 'success']); //show success message
                    $this->redirect('/users/login'); //redirect to the login page
                }
//                    }
//                    else {
//                            $this->Flash->error(__('Password does not match. Please try again.'));
//                        }

            } else {
                //otherwise return error for token and email not matching
                $this->Flash->error(__('Could not reset password, your email or reset code were incorrect'));
            }
        }
    }


    public function verification($verify_code){
        $customersTable = TableRegistry::getTableLocator()->get('customers');
        $verify = $customersTable->find('all')->where(['verify_code'=>$verify_code])->first();
        $verify->is_verified = '1';
        $customersTable->save($verify);
    }


}





