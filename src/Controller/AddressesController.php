<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Addresses Controller
 *
 * @property \App\Model\Table\AddressesTable $Addresses
 *
 * @method \App\Model\Entity\Address[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddressesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Customers']
        ];
        $addresses = $this->paginate($this->Addresses);

        $this->set(compact('addresses'));
    }

    /**
     * View method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => ['Customers']
        ]);

        $this->set('address', $address);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $address = $this->Addresses->newEntity();
        if ($this->request->is('post')) {
            $address = $this->Addresses->patchEntity($address, $this->request->getData());
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('The address has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The address could not be saved. Please, try again.'));
        }
        $customers = $this->Addresses->Customers->find('list', ['limit' => 200]);
        $this->set(compact('address', 'customers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $address = $this->Addresses->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $address = $this->Addresses->patchEntity($address, $this->request->getData());
            if ($this->Addresses->save($address)) {
                $this->Flash->success(__('The address has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The address could not be saved. Please, try again.'));
        }
        $customers = $this->Addresses->Customers->find('list', ['limit' => 200]);
        $this->set(compact('address', 'customers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Address id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $address = $this->Addresses->get($id);
        if ($this->Addresses->delete($address)) {
            $this->Flash->success(__('The address has been deleted.'));
        } else {
            $this->Flash->error(__('The address could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addBillingAddress()
    {

        if ($this->request->is(['patch', 'post', 'put'])) {
            $ordersTable = TableRegistry::getTableLocator()->get('orders');
            $customersTable = TableRegistry::getTableLocator()->get('customers');
            $this->loadModel('customers');

            $order_id = $this->request->getData('order_id');
            $first_name = $this->request->getData('first_name');
            $last_name = $this->request->getData('last_name');
            $address1 = $this->request->getData('address1');
            $address2 = $this->request->getData('address2');
            if(!isset($address2)){
                $address2 = 'N/A';
            }

            $state = $this->request->getData('state');
            $city = $this->request->getData('city');
            $postcode = $this->request->getData('postcode');
            $country = $this->request->getData('country');
            $phone = $this->request->getData('phone');
            $email = $this->request->getData('email');


            $customer_id= $ordersTable->find()
                ->select([
                    'customer_id',
                ])
                ->where(['id'=>$order_id]);
            $this->set('customer_id',$customer_id);

            $customer = $customersTable->get([$customer_id] , [
                'contain' => []
            ]);
            $customer->phone2 = $phone;

            $address = $this->Addresses->newEntity();

            $address->customer_id = $customer_id;
            $address->address1 = $address1;
            $address->address2 = $address2;
            $address->city = $city;
            $address->state = $state;
            $address->country = $country;
            $address->post_code = $postcode ;
            $address->first_name = $first_name;
            $address->last_name = $last_name;
            $address->email = $email;

            if($customersTable->save($customer)&&$this->Addresses->save($address)){
                $addressid= $address->id;

                $order = $ordersTable->get($order_id, [
                    'contain' => []
                ]);

                $order->billing_address_id = $addressid;

                if($ordersTable->save($order)){
                    return $this->redirect(['controller'=>'orders','action'=>'payment']);
                }

            }
        }
    }

}
