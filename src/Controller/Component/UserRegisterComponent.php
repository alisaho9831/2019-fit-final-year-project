<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;

class UserRegisterComponent extends Component
{
    public $components = [
        "Flash"
    ];

    public function register($entity){
        $controller = $this->_registry->getController();
        $table = TableRegistry::get($controller->name);

        if ($this->save($table, $entity)) {
            //Successfully send email verification
            $entity->user->sendEmail("Users/verification", "[Working Wellness] Account Verification", [
                "userName" => $entity->user->fname,
                "url" => Router::url(['controller' => 'users','action' =>'verify',$entity->user->id,$entity->user->token], true)
            ]);

            $this->Flash->success("Please check your email for a verification link.");
            return $controller->redirect(["controller" => "Users", "action" => "publicResendVerification"]);
        }
    }



    /**
     * Logs in the user and saves the entity to the session variable User.entity.
     */
    public function login($user) {
        $controller = $this->_registry->getController();
        $controller->Auth->setUser($user);
        $controller->request->getSession()->write([
            "User.entity" => $user,
        ]);

        return $user;
    }


}

