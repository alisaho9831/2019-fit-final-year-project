<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\Mailer\Email;
use Cake\Utility\Security;
use Cake\Mailer\TransportFactory;

/**
 * Orderlines Controller
 *
 * @property \App\Model\Table\OrderLinesTable $Orderlines
 *
 * @method \App\Model\Entity\OrderLine[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrderlinesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Orders', 'Items']
        ];
        $orderLines = $this->paginate($this->Orderlines);

        $this->set(compact('orderLines'));
    }

    /**
     * View method
     *
     * @param string|null $id Order Line id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $orderLine = $this->OrderLines->get($id, [
            'contain' => ['Orders', 'Items']
        ]);

        $this->set('orderLine', $orderLine);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $orderLine = $this->OrderLines->newEntity();
        if ($this->request->is('post')) {
            $orderLine = $this->OrderLines->patchEntity($orderLine, $this->request->getData());
            if ($this->OrderLines->save($orderLine)) {
                $this->Flash->success(__('The order line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order line could not be saved. Please, try again.'));
        }
        $orders = $this->OrderLines->Orders->find('list', ['limit' => 200]);
        $items = $this->OrderLines->Items->find('list', ['limit' => 200]);
        $this->set(compact('orderLine', 'orders', 'items'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Order Line id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $orderLine = $this->OrderLines->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $orderLine = $this->OrderLines->patchEntity($orderLine, $this->request->getData());
            if ($this->OrderLines->save($orderLine)) {
                $this->Flash->success(__('The order line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The order line could not be saved. Please, try again.'));
        }
        $orders = $this->OrderLines->Orders->find('list', ['limit' => 200]);
        $items = $this->OrderLines->Items->find('list', ['limit' => 200]);
        $this->set(compact('orderLine', 'orders', 'items'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Order Line id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $orderLine = $this->OrderLines->get($id);
        if ($this->OrderLines->delete($orderLine)) {
            $this->Flash->success(__('The order line has been deleted.'));
        } else {
            $this->Flash->error(__('The order line could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function addToCart()
    {
        $time = Time::now();
        $time->addMonth(3);

        $orderLine = $this->OrderLines->newEntity();
        if ($this->request->is('post')) {

            $order_id = $this->request->getData('order_id');
            $item_id = $this->request->getData('item_id');

            $condition = ['order_id'=>$order_id,'item_id' => $item_id];
            $item_exists = $this->OrderLines->exists($condition);

            if($item_exists == true){
                $this->Flash->error(__('Item is already exist in the shopping cart. If you wish to adjust the amount of purchase, please click EDIT.'));
                return $this->redirect(['controller'=>'orders','action'=>'cart']);
            }
            else{
                $amount = $this->request->getData('amount');

                $retail_price = $this->request->getData('retail_price');
                $check_flash_sales = $this->request->getData('is_flash_sales');
                $total_retail= $amount*$retail_price;

                $orderLine->order_id = $order_id;
                $orderLine->item_id = $item_id;
                $orderLine->fulfillment_status = 'Not fulfilled';
                $orderLine->is_inner_carton = 0;
                $orderLine->carton_amount = 0;

                $orderLine->amount = $amount;
                $orderLine->price = $total_retail;


                if($check_flash_sales==0){
                    $orderLine->expiry_date = $time;
                }elseif ($check_flash_sales==1){
                    $expiry_date = $this->request->getData('expiry_date');
                    $orderLine->expiry_date = $expiry_date;
                }



                $itemsTable = TableRegistry::getTableLocator()->get('items');
                $items = $itemsTable->get([$item_id] , [
                    'contain' => []
                ]);

                $items->retail_quantity -= $amount;


                if ($this->OrderLines->save($orderLine)&&$itemsTable->save($items)) {
                    return $this->redirect(['controller'=>'orders','action'=>'cart']);
                }
                debug($this->request);
                $this->Flash->error(__('The order line could not be saved. Please, try again.'));
            }

        }
        $orders = $this->OrderLines->Orders->find('list', ['limit' => 200]);
        $items = $this->OrderLines->Items->find('list', ['limit' => 200]);
        $this->set(compact('orderLine', 'orders', 'items'));
    }

    public function addToCartWholesale()
    {
        $time = Time::now();
        $time->addMonth(3);

        $orderLine = $this->OrderLines->newEntity();
        if ($this->request->is('post')) {

            $order_id = $this->request->getData('order_id');
            $item_id = $this->request->getData('item_id');

            $condition = ['order_id'=>$order_id,'item_id' => $item_id];
            $item_exists = $this->OrderLines->exists($condition);

            if($item_exists == true){
                $this->Flash->error(__('Item is already exist in the shopping cart. If you wish to adjust the amount of purchase, please click EDIT.'));
                return $this->redirect(['controller'=>'orders','action'=>'wholesale_cart']);
            }
            else{
                $carton_amount = $this->request->getData('carton_amount');//
                $in_out= $this->request->getData('is_inner_carton');
                $wholesale_price = $this->request->getData('wholesale_price');
                $check_flash_sales = $this->request->getData('is_flash_sales');
                $inner =  $this->request->getData('inner');
                $outer =  $this->request->getData('outer');
               // $total_wholesale= $amount*$wholesale_price;//

                $orderLine->order_id = $order_id;
                $orderLine->item_id = $item_id;
                $orderLine->fulfillment_status = 'Not fulfilled';
                $orderLine->is_inner_carton = $in_out;
                $orderLine->carton_amount = $carton_amount;

               // $orderLine->price = $total_wholesale;//
//-------------------------------------------------------------
                $itemsTable = TableRegistry::getTableLocator()->get('Items');
                $items = $itemsTable->get([$item_id] , [
                    'contain' => []
                ]);
//                $set_box = $this->Orderlines->find()->where(['item_id' => $item_id])
//                    ->contain(['items'])->first();
//                $inner_carton_size = $set_box->items->inner_carton_size;
//                $outer_carton_size = $set_box->items[0]->outer_carton_size;

                if ($in_out == 1) {
                    $amount = $carton_amount*($items->inner_carton_size);//
                    $orderLine->amount = $amount;
                    $items->wholesale_quantity -= $amount;

                    $total_wholesale= $carton_amount*$inner*$wholesale_price;//

                    $orderLine->price = $total_wholesale;
                }
                elseif($in_out == 0){
                    $amount = $carton_amount*$items->outer_carton_size;//
                    $orderLine->amount = $amount;
                    $items->wholesale_quantity -= $amount;
                    $total_wholesale= $carton_amount*$outer*$wholesale_price;//
                    $orderLine->price = $total_wholesale;
                }
                else{
                    echo "error";
                }

                if($check_flash_sales==0){
                    $orderLine->expiry_date = $time;
                }elseif ($check_flash_sales==1){
                    $expiry_date = $this->request->getData('expiry_date');
                    $orderLine->expiry_date = $expiry_date;
                }



                if ($this->OrderLines->save($orderLine)&&$itemsTable->save($items)) {
                    return $this->redirect(['controller'=>'orders','action'=>'wholesale_cart']);
                }
                debug($this->request);
                $this->Flash->error(__('The order line could not be saved. Please, try again.'));
            }

        }
        $orders = $this->OrderLines->Orders->find('list', ['limit' => 200]);
        $items = $this->OrderLines->Items->find('list', ['limit' => 200]);
        $this->set(compact('orderLine', 'orders', 'items'));
    }

    public function updateCart(){

        //debug($this->request);


        if($this->request->is(['patch', 'post', 'put'])){
            $orderlinesTable = TableRegistry::getTableLocator()->get('order_lines');

            $order_id=$this->request->getData('order_id');
            $item_id=$this->request->getData('item_id');
            $newAmount = $this->request->getData('amount');
            $oldAmount = $this->request->getData('oldAmount');
            $retailPrice = $this->request->getData('retail_price');
            $newPrice = $newAmount*$retailPrice;

            $orderlines = $orderlinesTable->get([$order_id,$item_id] , [
                'contain' => []
            ]);

            $orderlines->amount = $newAmount;
            $orderlines->price = $newPrice;

            $difference = $newAmount - $oldAmount;

            $itemsTable = TableRegistry::getTableLocator()->get('items');
            $items = $itemsTable->get([$item_id] , [
                'contain' => []
            ]);

            $items->retail_quantity -= $difference;

            if($orderlinesTable->save($orderlines)&&$itemsTable->save($items)){
                $this->Flash->success(__('Shopping Cart has been Updated.'));
            } else {
                $this->Flash->error(__('Update Failed.'));
            }

            return $this->redirect(['controller'=>'orders','action'=>'cart']);
        }

    }

    public function updateCartWholesale(){

        //debug($this->request);


        if($this->request->is(['patch', 'post', 'put'])){
            $orderlinesTable = TableRegistry::getTableLocator()->get('order_lines');

            $order_id=$this->request->getData('order_id');
            $item_id=$this->request->getData('item_id');
            if($this->request->getData('is_inner_carton')==1){
                $newAmount = $this->request->getData('carton_amount')*$this->request->getData('inner_carton_size');
            }
            else{
                $newAmount = $this->request->getData('carton_amount')*$this->request->getData('outer_carton_size');
            }
            //$newAmount = $this->request->getData('carton_amount');
            $oldAmount = $this->request->getData('oldAmount');
            $wholesalePrice = $this->request->getData('wholesale_price');
            $newPrice = $newAmount*$wholesalePrice;

            $orderlines = $orderlinesTable->get([$order_id,$item_id] , [
                'contain' => []
            ]);

            $orderlines->amount = $newAmount;
            $orderlines->price = $newPrice;
            $orderlines->is_inner_carton=$this->request->getData('is_inner_carton');
            $orderlines->carton_amount=$this->request->getData('carton_amount');

            $difference = $newAmount - $oldAmount;

            $itemsTable = TableRegistry::getTableLocator()->get('items');
            $items = $itemsTable->get([$item_id] , [
                'contain' => []
            ]);

            $items->wholesale_quantity -= $difference;

            if($orderlinesTable->save($orderlines)&&$itemsTable->save($items)){
                $this->Flash->success(__('Shopping Cart has been Updated.'));
            } else {
                $this->Flash->error(__('Update Failed.'));
            }

            return $this->redirect(['controller'=>'orders','action'=>'wholesale_cart']);
        }

    }

    public function deleteItem()
    {
        $this->request->allowMethod(['post', 'delete']);
        $order_id = $this->request->getData('order_id');
        $item_id = $this->request->getData('item_id');
        $amount = $this->request->getData('amount');

        $itemsTable = TableRegistry::getTableLocator()->get('items');
        $items = $itemsTable->get([$item_id] , [
            'contain' => []
        ]);

        $items->retail_quantity += $amount;

        $orderLine = $this->OrderLines->get(['order_id' => $order_id,'item_id'=>$item_id]);
        if ($this->OrderLines->delete($orderLine)&&$itemsTable->save($items)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller' => 'orders', 'action' => 'cart']);

    }

    public function deleteItemWholesale()
    {
        $this->request->allowMethod(['post', 'delete']);
        $order_id = $this->request->getData('order_id');
        $item_id = $this->request->getData('item_id');
        $amount = $this->request->getData('amount');

        $itemsTable = TableRegistry::getTableLocator()->get('items');
        $items = $itemsTable->get([$item_id] , [
            'contain' => []
        ]);

        $items->retail_quantity += $amount;

        $orderLine = $this->OrderLines->get(['order_id' => $order_id,'item_id'=>$item_id]);
        if ($this->OrderLines->delete($orderLine)&&$itemsTable->save($items)) {
            $this->Flash->success(__('The item has been deleted.'));
        } else {
            $this->Flash->error(__('The item could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller' => 'orders', 'action' => 'wholesale_cart']);

    }

    public function checkout()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('Orders');

            $addressesTable = TableRegistry::getTableLocator()->get('addresses');
            $customersTable = TableRegistry::getTableLocator()->get('customers');
            $ordersTable = TableRegistry::getTableLocator()->get('orders');
            $this->loadModel('customers');
            $this->loadModel('orders');


            $order_id = $this->request->getData('order_id');
            $first_name = $this->request->getData('first_name');
            $postcode = $this->request->getData('postcode');
            $last_name = $this->request->getData('last_name');
            $email = $this->request->getData('email');
            $address1 = $this->request->getData('address1');
            $address2 = $this->request->getData('address2');
            if ($address2=='') {
                $address2 = '';
            }

            $state = $this->request->getData('state');
            $city = $this->request->getData('city');
            $country = $this->request->getData('country');
            $phone = $this->request->getData('phone');
            $notes = $this->request->getData('notes');
            if ($notes=='') {
                $notes = '';
            }

            $total_cost = $this->request->getData('total_cost');
            $shipping_cost = $this->request->getData('shipping_cost');

            $order = $ordersTable->get([$order_id], [
                'contain' => []
            ]);

            $order->notes = $notes;
            $order->total_cost = $total_cost;
            $order->shipping_cost = $shipping_cost;
            $order->is_shopping_cart = 0;
            $order->order_status = 'Payment require';

            //$this->Orders->save($order);

            $customer_id = $this->orders->find()
                ->select([
                    'customer_id',
                ])
                ->where(['id' => $order_id]);
            $this->set('customer_id', $customer_id);

            $customer = $customersTable->get([$customer_id], [
                'contain' => []
            ]);
            $customer->phone1 = $phone;

            $customer_id = $customer->id;
            $customer_first_name = $customer->first_name;
            $customer_last_name = $customer->last_name;
            $customer_email = $customer->email;

            $shipping_address_id = $this->orders->find()
                ->select([
                    'shipping_address_id',
                ])
                ->where(['id' => $order_id]);
            $this->set('shipping_address_id', $shipping_address_id);

            $address = $addressesTable->get([$shipping_address_id], [
                'contain' => []
            ]);
            $address->address1 = $address1;
            $address->address2 = $address2;
            $address->city = $city;
            $address->state = $state;
            $address->country = $country;
            $address->email = $email;
            $address->first_name = $first_name;
            $address->last_name = $last_name;
            $address->post_code = $postcode;
            $address->customer_id = $customer_id;

            $address_id=$address->id;

            $same = $this->request->getData('sameForBillingAddress');
            if ($same == 0) {
                $b_first_name = $this->request->getData('b_first_name');
                $b_last_name = $this->request->getData('b_last_name');
                $b_email = $this->request->getData('b_email');
                $b_address1 = $this->request->getData('b_address1');
                $b_address2 = $this->request->getData('b_address2');
                if (!isset($b_address2)) {
                    $b_address2 = '';
                }

                $b_state = $this->request->getData('b_state');
                $b_city = $this->request->getData('b_city');
                $b_postcode = $this->request->getData('b_postcode');
                $b_country = $this->request->getData('b_country');

                $b_address = $addressesTable->newEntity();

                $b_address->address1 = $b_address1;
                $b_address->address2 = $b_address2;
                $b_address->city = $b_city;
                $b_address->state = $b_state;
                $b_address->country = $b_country;
                $b_address->post_code = $b_postcode;
                $b_address->email = $b_email;
                $b_address->first_name = $b_first_name;
                $b_address->last_name = $b_last_name;
                $b_address->customer_id = $customer_id;
                $b_address->is_deactivated = 0;

                if ($addressesTable->save($b_address)) {
                    $b_addressid = $b_address->id;
                    $order->billing_address_id = $b_addressid;
                }

            } else{
                $order->billing_address_id = $address_id;
            }

            if ($customersTable->save($customer)&&$ordersTable->save($order)&&$addressesTable->save($address)) {

                $new_address = $addressesTable->newEntity();


                $new_address->customer_id = $customer_id;
                $new_address->address1 = '';
                $new_address->address2 = '';
                $new_address->city = '';
                $new_address->state = '';
                $new_address->country = '';
                $new_address->post_code = 0;
                $new_address->first_name = $customer_first_name;
                $new_address->last_name = $customer_last_name;
                $new_address->email = $customer_email;
                $new_address->is_deactivated = 0;

                if($addressesTable->save($new_address)) {
                    $new_addressid = $new_address->id;


                    $new_order = $ordersTable->newEntity();

                    $new_order->email = $customer_email;
                    $new_order->financial_status = '';
                    $new_order->paid_date = new Date('1990-01-01');
                    $new_order->fulfillment_status = '';
                    $new_order->fulfillment_date = new Date('1990-01-01');
                    $new_order->subtotal = 0;
                    $new_order->taxes_cost = 0;
                    $new_order->shipping_cost = 0;
                    $new_order->total_cost = 0;
                    $new_order->discount_code = '';
                    $new_order->discount_amount = 0;
                    $new_order->shipping_method = '';
                    $new_order->order_date = new Date('1990-01-01');
                    $new_order->shipping_address_id = $new_addressid;
                    $new_order->billing_address_id = $new_addressid;
                    $new_order->promo_code_id = 1;
                    $new_order->payment_method = '';
                    $new_order->order_status = 'Not an order';
                    $new_order->notes = '';
                    $new_order->customer_id = $customer_id;
                    $new_order->is_shopping_cart = 1;
                    $new_order->is_deactivated = 0;
                    $new_order->shipping_company = "";
                    $new_order->tracking_number = "";

                    if ($ordersTable->save($new_order)) {
                        return $this->redirect(['controller' => 'orders', 'action' => 'payment', '?' => ['order' => $order_id]]);
                    }
                }
            }
            return $this->redirect(['controller' => 'orders', 'action' => 'complete_payment']);
        }
    }
    public function wholesaleCheckout()
    {
        if ($this->request->is(['patch', 'post', 'put'])) {
            $this->loadModel('Orders');
            $orderId = $this->request->getData('order_id');
            $orderDetails=$this->Orders->find('all',['conditions'=>['Orders.id'=>$orderId]])->contain(['Customers'])->toList();
            $orederLine=TableRegistry::getTableLocator()->get('Order_lines')->find('all',['conditions'=>['order_id'=>$orderId]])->contain(['items'=>'products'])->toList();
            //email set up configuration
            TransportFactory::setConfig('gmail', [
                'host' => 'smtp.gmail.com',
                'port' => 587,
                'timeout' => 30,
                'username' => 'smoothsales2019@gmail.com',
                'password' => 'IE2019team8',
                'className' => 'Smtp',
                'tls'=>true
            ]);

            //send wholesale enquiry detail through email to business owner
            $email = new Email();
            $email->setProfile(['from' => 'smoothsales2019@gmail.com', 'transport'=>'gmail']);
            //$email->setTransport('mailtrap');
            $email->setEmailFormat('html');
            $email->setTemplate('order');
            $email->setViewVars(['orderdetail'=>$orderDetails,'orderLinedetails' => $orederLine]);
            $email->setFrom('smoothsales2019@gmail.com', 'Test Smooth Sales');
            $email->setSubject('New Order');
            $email->setTo('smoothsales2019@gmail.com');
            $email->send();

            $addressesTable = TableRegistry::getTableLocator()->get('addresses');
            $customersTable = TableRegistry::getTableLocator()->get('customers');
            $ordersTable = TableRegistry::getTableLocator()->get('orders');
            $this->loadModel('customers');
            $this->loadModel('orders');

            $order_id = $this->request->getData('order_id');
            $first_name = $this->request->getData('first_name');
            $postcode = $this->request->getData('postcode');
            $last_name = $this->request->getData('last_name');
            $email = $this->request->getData('email');
            $address1 = $this->request->getData('address1');
            $address2 = $this->request->getData('address2');
            if ($address2=='') {
                $address2 = '';
            }

            $state = $this->request->getData('state');
            $city = $this->request->getData('city');
            $country = $this->request->getData('country');
            $phone = $this->request->getData('phone');
            $notes = $this->request->getData('notes');
            if ($notes=='') {
                $notes = '';
            }

            $total_cost = $this->request->getData('total_cost');
            $shipping_cost = $this->request->getData('shipping_cost');

            $order = $ordersTable->get([$order_id], [
                'contain' => []
            ]);

            $order->notes = $notes;
            $order->total_cost = $total_cost;
            $order->shipping_cost = $shipping_cost;

            //$this->Orders->save($order);

            $customer_id = $this->orders->find()
                ->select([
                    'customer_id',
                ])
                ->where(['id' => $order_id]);
            $this->set('customer_id', $customer_id);

            $customer = $customersTable->get([$customer_id], [
                'contain' => []
            ]);
            $customer->phone1 = $phone;

            $customer_id = $customer->id;

            $shipping_address_id = $this->orders->find()
                ->select([
                    'shipping_address_id',
                ])
                ->where(['id' => $order_id]);
            $this->set('shipping_address_id', $shipping_address_id);

            $address = $addressesTable->get([$shipping_address_id], [
                'contain' => []
            ]);
            $address->address1 = $address1;
            $address->address2 = $address2;
            $address->city = $city;
            $address->state = $state;
            $address->country = $country;
            $address->email = $email;
            $address->first_name = $first_name;
            $address->last_name = $last_name;
            $address->post_code = $postcode;
            $address->customer_id = $customer_id;

            $address_id=$address->id;

            $same = $this->request->getData('sameForBillingAddress');
            if ($same == 0) {
                $b_first_name = $this->request->getData('b_first_name');
                $b_last_name = $this->request->getData('b_last_name');
                $b_email = $this->request->getData('b_email');
                $b_address1 = $this->request->getData('b_address1');
                $b_address2 = $this->request->getData('b_address2');
                if (!isset($b_address2)) {
                    $b_address2 = '';
                }

                $b_state = $this->request->getData('b_state');
                $b_city = $this->request->getData('b_city');
                $b_postcode = $this->request->getData('b_postcode');
                $b_country = $this->request->getData('b_country');

                $b_address = $addressesTable->newEntity();

                $b_address->address1 = $b_address1;
                $b_address->address2 = $b_address2;
                $b_address->city = $b_city;
                $b_address->state = $b_state;
                $b_address->country = $b_country;
                $b_address->post_code = $b_postcode;
                $b_address->email = $b_email;
                $b_address->first_name = $b_first_name;
                $b_address->last_name = $b_last_name;
                $b_address->customer_id = $customer_id;

                if ($addressesTable->save($b_address)) {
                    $b_addressid = $b_address->id;
                    $order->billing_address_id = $b_addressid;
                }

            } else{
                $order->billing_address_id = $address_id;
            }

            if ($customersTable->save($customer)&&$ordersTable->save($order)) {
                if($addressesTable->save($address)){
                    return $this->redirect(['controller' => 'orders', 'action' => 'wholesale_payment','?' => ['order' => $order_id]]);
                }else{debug($address);}
            }
            return $this->redirect(['controller' => 'orders', 'action' => 'complete_payment']);

        }
    }
}
