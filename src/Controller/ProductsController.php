<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Paginator');
    }

    public function index()
    {
        $products = $this->paginate($this->Products);

        $this->set(compact('products'));
    }

    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
            $product = $this->Products->get($id, [
            'contain' => ['Customers', 'Items', 'ProductCollections', 'ProductImages', 'ProductTags']
        ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $product = $this->Products->newEntity();
        if ($this->request->is('post')) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $customers = $this->Products->Customers->find('list', ['limit' => 200]);
        $this->set(compact('product', 'customers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => ['Customers']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $product = $this->Products->patchEntity($product, $this->request->getData());
            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }
        $customers = $this->Products->Customers->find('list', ['limit' => 200]);
        $this->set(compact('product', 'customers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //the product details page for each product
    public function detail(){
        $this->loadModel('products');
        $this->loadModel('orders');
        $this->loadModel('flash_sales');
        $this->loadModel('Customers');
        if($this->Auth->user()){
            $userVerified=$this->Customers->find('all',['conditions'=>['user_id'=>$this->Auth->user('id')]])->toList()[0]['is_verified'];
            $this->set('userVerified',$userVerified);
            $this->set('log','yes');
        }
        else{
            $this->set('log','no');
        }


        $id=$this->request->query['product'];
        $collectionid=$this->request->query['collection'];

        $time = Time::now();

        $flashSales = $this->flash_sales->find()
            ->contain(['collections'])
            ->where(['flash_sales.start_date <=' => $time, 'flash_sales.end_date >=' => $time, 'flash_sales.collection_id' => $collectionid]);
        $this->set('flashSales',$flashSales);

        $list_Products = $this ->Products->find('all',['contain' =>['items','productimages']])
            ->where(['id'=> $id]);
        $this->set('listProducts', $list_Products);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);

        $collection_name = $this->Collections->find()
            ->where(['id' => $collectionid]);
        $this->set('collection',$collection_name);

        $relatedProducts = $this->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_retail' => 1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->join([
                'table' => 'collections_products',
                'type' => 'LEFT',
                'conditions' => ['Products.id=collections_products.product_id']
            ])
            ->where(['collections_products.collection_id'=>$collectionid,'Products.id !='=>$id])
            ->order(['RAND()'])
            ->limit(3);
        //$relatedProducts->select(['collection_id'=>'collections_products.collection_id']);
        $this->set('relatedProducts',$relatedProducts);

        $this->set('collectionID',$collectionid);

    }

    public function wholesaleDetail(){
        $this->loadModel('products');
        $this->loadModel('orders');
        $this->loadModel('Customers');
        if($this->Auth->user()){
        $userVerified=$this->Customers->find('all',['conditions'=>['user_id'=>$this->Auth->user('id')]])->toList()[0]['is_verified'];
        $this->set('userVerified',$userVerified);
            $this->set('log','yes');
        }
        else{
            $this->set('log','no');
        }
        $id=$this->request->query['product'];
        $collectionid=$this->request->query['collection'];


        $list_Products = $this ->Products->find('all',['contain' =>['items','productimages']])
            ->where(['id'=> $id]);
        $this->set('listProducts', $list_Products);
        //var_dump($list_Products->toList()[0]['items']);
        $this->set('itemInfo', $list_Products->toList()[0]['items']);


        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);

        $collection_name = $this->Collections->find()
            ->where(['id' => $collectionid]);
        $this->set('collection',$collection_name);

    }

//listing all the products in a selected category
    public function listing($id){
        $this->loadModel('collections_products');
        $this->loadModel('orders');
        $this->loadModel('collections');
        $this->loadModel('flash_sales');
        $this->loadModel('Customers');
        if($this->Auth->user()){
            $userVerified=$this->Customers->find('all',['conditions'=>['user_id'=>$this->Auth->user('id')]])->toList()[0]['is_verified'];
            $this->set('userVerified',$userVerified);
            $this->set('log','yes');
        }
        else{
            $this->set('log','no');
        }


        $products = $this->paginate($this->Products,['limit'=>15]);
        $this->set(compact('products'));


        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);

        $collections = $this->collections_products->find()
            ->where(['collection_id'=>$id]);
        $this->set('productCollection', $collections);

        $time = Time::now();

        $flashSales = $this->flash_sales->find()
            ->contain(['collections'])
            ->where(['flash_sales.start_date <=' => $time, 'flash_sales.end_date >=' => $time, 'flash_sales.collection_id' => $id]);
        $this->set('flashSales',$flashSales);


        $productlisting = $this->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_retail' => 1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->join([
                'table' => 'collections_products',
                'type' => 'LEFT',
                'conditions' => ['Products.id=collections_products.product_id']
            ])
            ->where(['collections_products.collection_id'=>$id])
            ->order('Products.brand_name ASC');
        $this->set('productlistings', $productlisting);

        $collection_name = $this->Collections->find()
            ->where(['id' => $id]);
        $this->set('collection',$collection_name);

        $this->set('items_collection_id',$id);



    }
    public function wholesaleListing($id){
        $this->loadModel('collections_products');
        $this->loadModel('orders');
        $this->loadModel('collections');
        $this->loadModel('Customers');
        if($this->Auth->user()) {
            $userVerified = $this->Customers->find('all', ['conditions' => ['user_id' => $this->Auth->user('id')]])->toList()[0]['is_verified'];
            $this->set('userVerified', $userVerified);
            $this->set('log', 'yes');
        }
        else{
            $this->set('log','no');
        }
        $products = $this->paginate($this->Products,['limit'=>15]);
        $this->set('products',$products);



        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);

        $collections = $this->collections_products->find()
            ->where(['collection_id'=>$id]);
        $this->set('productCollection', $collections);


        $productlisting = $this->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_wholesale' => 1 , 'Items.is_wholesale' =>1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->join([
                'table' => 'collections_products',
                'type' => 'LEFT',
                'conditions' => ['Products.id=collections_products.product_id']
            ])
            ->where(['collections_products.collection_id'=>$id]);
        $this->set('productlistings', $productlisting);

        $collection_name = $this->Collections->find()
            ->where(['id' => $id]);
        $this->set('collection',$collection_name);

        $this->set('items_collection_id',$id);
    }

//filter products by products attributes such as brand
    public function filter($id,$filterBrand = null,$filterPriceLow = null,$filterPriceHigh = null){
        $this->loadModel('products');
        $this->loadModel('collections_products');
        $this->loadModel('orders');


        $collections = $this->collections_products->find()
            ->where(['collection_id'=>$id]);
        $this->set('productCollection', $collections);

        $conditionJoin = ['collections_products.collection_id'=>$id];
        $listProductJoin = [];
        $itemConditionJoin = ['Items.published_retail' => 1, 'Items.is_deactivated'=>0];

        if(isset($filterBrand) && $filterBrand != "all"){
            array_push($conditionJoin, ['products.brand_name'=>$filterBrand]);
            array_push($listProductJoin,['brand_name'=>$filterBrand]);
        }

        if($this->request->getData('minPrice')){
            $filterPriceLow = $this->request->getData('minPrice');
        }

        if($this->request->getData('maxPrice')){
            $filterPriceHigh = $this->request->getData('maxPrice');
        }

        if(isset($filterPriceLow)){
            array_push($itemConditionJoin,['Items.retail_price >=' => $filterPriceLow]);
        } else if(isset($filterPriceHigh)) {
            $filterPriceLow = 0;
            array_push($itemConditionJoin,['Items.retail_price >=' => $filterPriceLow]);
        }

        if(isset($filterPriceHigh)){
            array_push($itemConditionJoin,['Items.retail_price <=' => $filterPriceHigh]);
        }

        $list_Product = $this -> products -> find()
            -> where($listProductJoin);

        $this->set('listProduct', $list_Product);
        $this->set('currentFilters',[$filterBrand,$filterPriceLow,$filterPriceHigh]);

        $productlisting = $this->products->find()
            ->contain(['items' => function($q) use ($itemConditionJoin) {
                return $q->where($itemConditionJoin);
            },'productimages'])
            ->join([
                'table' => 'collections_products',
                'type' => 'INNER',
                'conditions' => ['products.id=collections_products.product_id']
            ])
            ->where($conditionJoin)
            ->order('products.brand_name ASC');
        $this->set('productlistings', $productlisting);

        $collection_name = $this->Collections->find()
            ->where(['id' => $id]);
        $this->set('collection',$collection_name);

        $this->set('items_collection_id',$id);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);
    }

//wholesale filter
    public function filterWholesale($id,$filterBrand = null,$filterPriceLow = null,$filterPriceHigh = null){
        $this->loadModel('products');
        $this->loadModel('collections_products');
        $this->loadModel('orders');


        $collections = $this->collections_products->find()
            ->where(['collection_id'=>$id]);
        $this->set('productCollection', $collections);

        $conditionJoin = ['collections_products.collection_id'=>$id];
        $listProductJoin = [];
        $itemConditionJoin = ['Items.published_wholesale' => 1, 'Items.is_deactivated'=>0];

        if(isset($filterBrand) && $filterBrand != "all"){
            array_push($conditionJoin, ['products.brand_name'=>$filterBrand]);
            array_push($listProductJoin,['brand_name'=>$filterBrand]);
        }

        if($this->request->getData('minPrice')){
            $filterPriceLow = $this->request->getData('minPrice');
        }

        if($this->request->getData('maxPrice')){
            $filterPriceHigh = $this->request->getData('maxPrice');
        }

        if(isset($filterPriceLow)){
            array_push($itemConditionJoin,['Items.wholesale_price >=' => $filterPriceLow]);
        } else if(isset($filterPriceHigh)) {
            $filterPriceLow = 0;
            array_push($itemConditionJoin,['Items.wholesale_price >=' => $filterPriceLow]);
        }

        if(isset($filterPriceHigh)){
            array_push($itemConditionJoin,['Items.wholesale_price <=' => $filterPriceHigh]);
        }

        $list_Product = $this -> products -> find()
            -> where($listProductJoin);

        $this->set('listProduct', $list_Product);
        $this->set('currentFilters',[$filterBrand,$filterPriceLow,$filterPriceHigh]);

        $productlisting = $this->products->find()
            ->contain(['items' => function($q) use ($itemConditionJoin) {
                return $q->where($itemConditionJoin);
            },'productimages'])
            ->join([
                'table' => 'collections_products',
                'type' => 'INNER',
                'conditions' => ['products.id=collections_products.product_id']
            ])
            ->where($conditionJoin)
            ->order('products.brand_name ASC');
        $this->set('productlistings', $productlisting);

        $collection_name = $this->Collections->find()
            ->where(['id' => $id]);
        $this->set('collection',$collection_name);

        $this->set('items_collection_id',$id);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);
    }

//search for a certain product
    public function search($keyword = null,$filterBrand = null,$filterPriceLow = null,$filterPriceHigh = null){
        $this->loadModel('products');
        $this->loadModel('collections_products');
        $this->loadModel('orders');
        $this->loadModel('tags');

        if(isset($_POST['keyword'])) {
            $keyword=strtolower($_POST['keyword']);
        }

        $tagsearch =  $this->tags->find()
            ->select(['id'])
            ->where(['tag_name'=>$keyword]);

        $condition = ['products.title LIKE'=>'%'.$keyword.'%'];
        $itemConditionJoin = ['Items.published_retail' => 1, 'Items.is_deactivated'=>0];

        if(isset($filterBrand) && $filterBrand != "all"){
            array_push($condition,['products.brand_name' => $filterBrand]);
        }

        if($this->request->getData('minPrice')){
            $filterPriceLow = $this->request->getData('minPrice');
        }

        if($this->request->getData('maxPrice')){
            $filterPriceHigh = $this->request->getData('maxPrice');
        }

        if(isset($filterPriceLow)){
            array_push($itemConditionJoin,['Items.retail_price >=' => $filterPriceLow]);
        } else if(isset($filterPriceHigh)) {
            $filterPriceLow = 0;
            array_push($itemConditionJoin,['Items.retail_price >=' => $filterPriceLow]);
        }

        if(isset($filterPriceHigh)){
            array_push($itemConditionJoin,['Items.retail_price <=' => $filterPriceHigh]);
        }

        $this->set('currentFilters',[$filterBrand,$filterPriceLow,$filterPriceHigh]);

        if(($tagsearch->isEmpty())){
            $resultset=$this->Products->find()
                ->contain(['items' => function($q) use ($itemConditionJoin) {
                    return $q->where(['Items.published_retail' => 1, 'Items.is_deactivated'=>0]);
                },'productimages'])
                ->where(['Products.title LIKE'=>'%'.$keyword.'%'])
                ->order('Products.brand_name ASC');

        }else{

            $resultset=$this->Products->find()
                ->contain(['items' => function($q) use ($itemConditionJoin) {
                    return $q->where(['Items.published_retail' => 1, 'Items.is_deactivated'=>0]);
                },'productimages'
                ])
                ->join([
                    'table' => 'products_tags',
                    'type' => 'INNER',
                    'conditions' => ['Products.id=products_tags.product_id']
                ])
                ->join([
                    'table' => 'tags',
                    'type' => 'INNER',
                    'conditions' => ['tags.id=products_tags.tag_id']
                ])
                ->where(['tags.tag_name'=>$keyword])
                ->order('Products.brand_name ASC');
        }

        $this->set('searchresults',$resultset);

        $this->set('keywords',$keyword);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);


    }

//wholesale search bar
    public function searchWholesale($keyword = null,$filterBrand = null,$filterPriceLow = null,$filterPriceHigh = null){
        $this->loadModel('products');
        $this->loadModel('collections_products');
        $this->loadModel('orders');

        if(isset($_POST['keyword'])) {
            $keyword=$_POST['keyword'];
        }

        $condition = ['Products.title LIKE'=>'%'.$keyword.'%'];
        $itemConditionJoin = ['Items.published_wholesale' => 1, 'Items.is_deactivated'=>0];

        if(isset($filterBrand) && $filterBrand != "all"){
            array_push($condition,['Products.brand_name' => $filterBrand]);
        }

        if($this->request->getData('minPrice')){
            $filterPriceLow = $this->request->getData('minPrice');
        }

        if($this->request->getData('maxPrice')){
            $filterPriceHigh = $this->request->getData('maxPrice');
        }

        if(isset($filterPriceLow)){
            array_push($itemConditionJoin,['Items.wholesale_price >=' => $filterPriceLow]);
        } else if(isset($filterPriceHigh)) {
            $filterPriceLow = 0;
            array_push($itemConditionJoin,['Items.wholesale_price >=' => $filterPriceLow]);
        }

        if(isset($filterPriceHigh)){
            array_push($itemConditionJoin,['Items.wholesale_price <=' => $filterPriceHigh]);
        }

        $this->set('currentFilters',[$filterBrand,$filterPriceLow,$filterPriceHigh]);

        $resultset=$this->Products->find()
            ->contain(['items' => function($q) use ($itemConditionJoin) {
                return $q->where($itemConditionJoin);
            },'productimages'])
            ->where($condition)
            ->order('Products.brand_name ASC');
        $this->set('searchresults',$resultset);

        $this->set('keywords',$keyword);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);


    }

//retail details page
    public function normalDetail(){
        $this->loadModel('products');
        $this->loadModel('orders');
        $this->loadModel('flash_sales');

        $id=$this->request->query['product'];

        $list_Products = $this ->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_retail' => 1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->where(['id'=> $id]);
        $this->set('listProducts', $list_Products);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);


        $relatedProducts = $this->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_retail' => 1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->where(['Products.id !='=>$id])
            ->order(['RAND()'])
            ->limit(3);
        //$relatedProducts->select(['collection_id'=>'collections_products.collection_id']);
        $this->set('relatedProducts',$relatedProducts);
    }

//wholesale details page
    public function normalDetailWholesale(){
        $this->loadModel('products');
        $this->loadModel('orders');
        $this->loadModel('flash_sales');

        $id=$this->request->query['product'];

        $list_Products = $this ->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_wholesale' => 1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->where(['id'=> $id]);
        $this->set('listProducts', $list_Products);

        $findshoppingcart = $this->orders->find()
            ->select(['order_id' => 'id'])
            ->where(['orders.email'=> $this->Auth->user('username') , 'is_shopping_cart'=> 1,]);
        $this->set('shoppingCartId', $findshoppingcart);


        $relatedProducts = $this->Products->find()
            ->contain(['items' => function($q) {
                return $q->where(['Items.published_wholesale' => 1, 'Items.is_deactivated'=>0]);
            },'productimages'
            ])
            ->where(['Products.id !='=>$id])
            ->order(['RAND()'])
            ->limit(3);
        //$relatedProducts->select(['collection_id'=>'collections_products.collection_id']);
        $this->set('relatedProducts',$relatedProducts);
    }

}
