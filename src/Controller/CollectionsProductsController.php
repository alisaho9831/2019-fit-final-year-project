<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * CollectionsProducts Controller
 *
 * @property \App\Model\Table\CollectionsProductsTable $CollectionsProducts
 *
 * @method \App\Model\Entity\CollectionsProduct[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CollectionsProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Products', 'Collections']
        ];
        $collectionsProducts = $this->paginate($this->CollectionsProducts);

        $this->set(compact('collectionsProducts'));
    }

    /**
     * View method
     *
     * @param string|null $id Collections Product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $collectionsProduct = $this->CollectionsProducts->get($id, [
            'contain' => ['Products', 'Collections']
        ]);

        $this->set('collectionsProduct', $collectionsProduct);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $collectionsProduct = $this->CollectionsProducts->newEntity();
        if ($this->request->is('post')) {
            $collectionsProduct = $this->CollectionsProducts->patchEntity($collectionsProduct, $this->request->getData());
            if ($this->CollectionsProducts->save($collectionsProduct)) {
                $this->Flash->success(__('The collections product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The collections product could not be saved. Please, try again.'));
        }
        $products = $this->CollectionsProducts->Products->find('list', ['limit' => 200]);
        $collections = $this->CollectionsProducts->Collections->find('list', ['limit' => 200]);
        $this->set(compact('collectionsProduct', 'products', 'collections'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Collections Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $collectionsProduct = $this->CollectionsProducts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $collectionsProduct = $this->CollectionsProducts->patchEntity($collectionsProduct, $this->request->getData());
            if ($this->CollectionsProducts->save($collectionsProduct)) {
                $this->Flash->success(__('The collections product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The collections product could not be saved. Please, try again.'));
        }
        $products = $this->CollectionsProducts->Products->find('list', ['limit' => 200]);
        $collections = $this->CollectionsProducts->Collections->find('list', ['limit' => 200]);
        $this->set(compact('collectionsProduct', 'products', 'collections'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Collections Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $collectionsProduct = $this->CollectionsProducts->get($id);
        if ($this->CollectionsProducts->delete($collectionsProduct)) {
            $this->Flash->success(__('The collections product has been deleted.'));
        } else {
            $this->Flash->error(__('The collections product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
