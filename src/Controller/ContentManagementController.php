<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ContentManagement Controller
 *
 * @property \App\Model\Table\ContentManagementTable $ContentManagement
 *
 * @method \App\Model\Entity\ContentManagement[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentManagementController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'));
    }

    /**
     * View method
     *
     * @param string|null $id Content Management id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contentManagement = $this->ContentManagement->get($id, [
            'contain' => []
        ]);

        $this->set('contentManagement', $contentManagement);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contentManagement = $this->ContentManagement->newEntity();
        if ($this->request->is('post')) {
            $contentManagement = $this->ContentManagement->patchEntity($contentManagement, $this->request->getData());
            if ($this->ContentManagement->save($contentManagement)) {
                $this->Flash->success(__('The content management has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content management could not be saved. Please, try again.'));
        }
        $this->set(compact('contentManagement'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Content Management id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contentManagement = $this->ContentManagement->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contentManagement = $this->ContentManagement->patchEntity($contentManagement, $this->request->getData());
            if ($this->ContentManagement->save($contentManagement)) {
                $this->Flash->success(__('The content management has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content management could not be saved. Please, try again.'));
        }
        $this->set(compact('contentManagement'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Content Management id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contentManagement = $this->ContentManagement->get($id);
        if ($this->ContentManagement->delete($contentManagement)) {
            $this->Flash->success(__('The content management has been deleted.'));
        } else {
            $this->Flash->error(__('The content management could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    //Retail Footer pages

    public function PrivacyPolicy(){

        $conditions = array('ContentManagement.id' => 2);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());

    }

    public function RefundPolicy(){

        $conditions = array('ContentManagement.id' => 3);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());


    }
    public function sellThroughUs(){

        $conditions = array('ContentManagement.id' => 4);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());


    }
    public function TermsOfService(){

        $conditions = array('ContentManagement.id' => 1);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());

    }
    public function fAQ()

    {
        $conditions = array('ContentManagement.id' => 5);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());

    }

    // wholesale footer

    public function PrivacyPolicyWholesale(){

        $conditions = array('ContentManagement.id' => 2);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());

    }

    public function RefundPolicyWholesale(){

        $conditions = array('ContentManagement.id' => 3);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());


    }
    public function sellThroughUsWholesale(){

        $conditions = array('ContentManagement.id' => 4);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());


    }
    public function TermsOfServiceWholesale(){

        $conditions = array('ContentManagement.id' => 1);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());

    }
    public function fAQWholesale()

    {
        $conditions = array('ContentManagement.id' => 5);

        $this->paginate = array('conditions' => $conditions);

        $contentManagement = $this->paginate($this->ContentManagement);

        $this->set(compact('contentManagement'), $this->paginate());

    }
}
