<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;

/**
 * CustomersProducts Controller
 *
 * @property \App\Model\Table\CustomersProductsTable $CustomersProducts
 *
 * @method \App\Model\Entity\CustomersProduct[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersProductsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Customers', 'Products']
        ];
        $customersProducts = $this->paginate($this->CustomersProducts);

        $this->set(compact('customersProducts'));
    }

    /**
     * View method
     *
     * @param string|null $id Customers Product id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customersProduct = $this->CustomersProducts->get($id, [
            'contain' => ['Customers', 'Products']
        ]);

        $this->set('customersProduct', $customersProduct);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customersProduct = $this->CustomersProducts->newEntity();
        if ($this->request->is('post')) {
            $customersProduct = $this->CustomersProducts->patchEntity($customersProduct, $this->request->getData());
            if ($this->CustomersProducts->save($customersProduct)) {
                $this->Flash->success(__('The customers product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customers product could not be saved. Please, try again.'));
        }
        $customers = $this->CustomersProducts->Customers->find('list', ['limit' => 200]);
        $products = $this->CustomersProducts->Products->find('list', ['limit' => 200]);
        $this->set(compact('customersProduct', 'customers', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Customers Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customersProduct = $this->CustomersProducts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customersProduct = $this->CustomersProducts->patchEntity($customersProduct, $this->request->getData());
            if ($this->CustomersProducts->save($customersProduct)) {
                $this->Flash->success(__('The customers product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customers product could not be saved. Please, try again.'));
        }
        $customers = $this->CustomersProducts->Customers->find('list', ['limit' => 200]);
        $products = $this->CustomersProducts->Products->find('list', ['limit' => 200]);
        $this->set(compact('customersProduct', 'customers', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customers Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customersProduct = $this->CustomersProducts->get($id);
        if ($this->CustomersProducts->delete($customersProduct)) {
            $this->Flash->success(__('The customers product has been deleted.'));
        } else {
            $this->Flash->error(__('The customers product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function wishlist(){
        $this->loadModel('customers');

        $email=$this->Auth->user('username');

        $customerid = $this->customers->find()
            ->select(['id'=>'id'])
            ->where(['email'=>$email]);

        $wishlist = $this->CustomersProducts->find()
            ->select([
                'products_id' => 'products.id',
                'retail_price' => 'items.retail_price',
                'title' => 'products.title',
                'body' => 'products.body',
                'image' => 'productimages.image'
            ])
            ->where(['customer_id'=>$customerid])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = CustomersProducts.product_id',
                ]
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.product_id = CustomersProducts.product_id',
                ]
            ])
            ->join([
                'table' => 'productimages',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = productimages.product_id',
                ]
            ]);
        $this->set('wishlist_products', $wishlist);

        $time = Time::now();

        $checkFS = $this->CustomersProducts->find()
            ->select([
                'collection_id' => 'flash_sales.collection_id',
            ])
            ->where(['customer_id'=>$customerid])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = CustomersProducts.product_id',
                ]
            ])
            ->join([
                'table' => 'collections_products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = collections_products.product_id',
                ]
            ])
            ->join([
                'table' => 'flash_sales',
                'type' => 'INNER',
                'conditions' => [
                    'flash_sales.collection_id = collections_products.collection_id','flash_sales.start_date <=' => $time, 'flash_sales.end_date >=' => $time,
                ]
            ]);
        $this->set('check_flash_sales', $checkFS);

    }
    public function wholesaleWishlist(){
        $this->loadModel('customers');

        $email=$this->Auth->user('username');

        $customerid = $this->customers->find()
            ->select(['id'=>'id'])
            ->where(['email'=>$email]);

        $wishlist = $this->CustomersProducts->find()

            ->select([
                'products_id' => 'products.id',
                'wholesale_price' => 'items.wholesale_price',
                'title' => 'products.title',
                'body' => 'products.body',
                'image' => 'productimages.image'
            ])
            ->where(['customer_id'=>$customerid])
            ->join([
                'table' => 'products',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = CustomersProducts.product_id',
                ]
            ])
            ->join([
                'table' => 'items',
                'type' => 'INNER',
                'conditions' => [
                    'items.product_id = CustomersProducts.product_id',
                ]
            ])
            ->join([
                'table' => 'productimages',
                'type' => 'INNER',
                'conditions' => [
                    'products.id = productimages.product_id',
                ]
            ]);;
        $this->set('wishlist_products', $wishlist);

    }

    public function addToWishlist()
    {
        $this->loadModel('customers');

        $email=$this->Auth->user('username');
        $customerid = $this->customers->find()
            ->select(['id'=>'id'])
            ->where(['email'=>$email]);

        $CustomersProducts = $this->CustomersProducts->newEntity();
        if ($this->request->is('post')) {

            $product_id = $this->request->getData('product_id');

            $condition = ['customer_id'=>$customerid,'product_id' => $product_id];
            $product_exists = $this->CustomersProducts->exists($condition);

            if($product_exists == true){
                $this->Flash->error(__('Item is already exist in the shopping cart. If you wish to adjust the amount of purchase, please click EDIT.'));
                return $this->redirect(['controller'=>'customers_products','action'=>'wishlist']);
            }
            else{
                $CustomersProducts->customer_id = $customerid;
                $CustomersProducts->product_id = $product_id;

                if ($this->CustomersProducts->save($CustomersProducts)) {
                    return $this->redirect(['controller'=>'customers_products','action'=>'wishlist']);
                }
                debug($this->request);
                $this->Flash->error(__('The order line could not be saved. Please, try again.'));
            }

        }
        $customers = $this->CustomersProducts->Customers->find('list', ['limit' => 200]);
        $products = $this->CustomersProducts->Products->find('list', ['limit' => 200]);
        $this->set(compact('customersProduct', 'customers', 'products'));
    }

    public function addToWishlistWholesale()
    {
        $this->loadModel('customers');

        $email=$this->Auth->user('username');
        $customerid = $this->customers->find()
            ->select(['id'=>'id'])
            ->where(['email'=>$email]);

        $CustomersProducts = $this->CustomersProducts->newEntity();
        if ($this->request->is('post')) {

            $product_id = $this->request->getData('product_id');

            $condition = ['customer_id'=>$customerid,'product_id' => $product_id];
            $product_exists = $this->CustomersProducts->exists($condition);

            if($product_exists == true){
                $this->Flash->error(__('Item is already exist in the shopping cart. If you wish to adjust the amount of purchase, please click EDIT.'));
                return $this->redirect(['controller'=>'customers_products','action'=>'wholesale_wishlist']);
            }
            else{
                $CustomersProducts->customer_id = $customerid;
                $CustomersProducts->product_id = $product_id;

                if ($this->CustomersProducts->save($CustomersProducts)) {
                    return $this->redirect(['controller'=>'customers_products','action'=>'wholesale_wishlist']);
                }
                debug($this->request);
                $this->Flash->error(__('The order line could not be saved. Please, try again.'));
            }

        }
        $customers = $this->CustomersProducts->Customers->find('list', ['limit' => 200]);
        $products = $this->CustomersProducts->Products->find('list', ['limit' => 200]);
        $this->set(compact('customersProduct', 'customers', 'products'));
    }

    public function deleteProduct()
    {
        $this->loadModel('customers');

        $email=$this->Auth->user('username');

        $customerid = $this->customers->find()
            ->select(['id'=>'id'])
            ->where(['email'=>$email]);

        $this->request->allowMethod(['post', 'delete']);
        $product_id = $this->request->getData('product_id');

        $CustomersProducts = $this->CustomersProducts->get(['customer_id'=>$customerid,'product_id' => $product_id]);
        if ($this->CustomersProducts->delete($CustomersProducts)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller'=>'customers_products','action'=>'wishlist']);

    }

    public function deleteProductWholesale()
    {
        $this->loadModel('customers');

        $email=$this->Auth->user('username');

        $customerid = $this->customers->find()
            ->select(['id'=>'id'])
            ->where(['email'=>$email]);

        $this->request->allowMethod(['post', 'delete']);
        $product_id = $this->request->getData('product_id');

        $CustomersProducts = $this->CustomersProducts->get(['customer_id'=>$customerid,'product_id' => $product_id]);
        if ($this->CustomersProducts->delete($CustomersProducts)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }
        return $this->redirect(['controller'=>'customers_products','action'=>'wholesale_wishlist']);

    }
}
