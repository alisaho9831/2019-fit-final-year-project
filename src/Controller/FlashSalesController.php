<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * FlashSales Controller
 *
 * @property \App\Model\Table\FlashSalesTable $FlashSales
 *
 * @method \App\Model\Entity\FlashSale[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FlashSalesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Collections']
        ];
        $flashSales = $this->paginate($this->FlashSales);

        $this->set(compact('flashSales'));
    }

    /**
     * View method
     *
     * @param string|null $id Flash Sale id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $flashSale = $this->FlashSales->get($id, [
            'contain' => ['Collections']
        ]);

        $this->set('flashSale', $flashSale);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $flashSale = $this->FlashSales->newEntity();
        if ($this->request->is('post')) {
            $flashSale = $this->FlashSales->patchEntity($flashSale, $this->request->getData());
            if ($this->FlashSales->save($flashSale)) {
                $this->Flash->success(__('The flash sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The flash sale could not be saved. Please, try again.'));
        }
        $collections = $this->FlashSales->Collections->find('list', ['limit' => 200]);
        $this->set(compact('flashSale', 'collections'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Flash Sale id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $flashSale = $this->FlashSales->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $flashSale = $this->FlashSales->patchEntity($flashSale, $this->request->getData());
            if ($this->FlashSales->save($flashSale)) {
                $this->Flash->success(__('The flash sale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The flash sale could not be saved. Please, try again.'));
        }
        $collections = $this->FlashSales->Collections->find('list', ['limit' => 200]);
        $this->set(compact('flashSale', 'collections'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Flash Sale id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $flashSale = $this->FlashSales->get($id);
        if ($this->FlashSales->delete($flashSale)) {
            $this->Flash->success(__('The flash sale has been deleted.'));
        } else {
            $this->Flash->error(__('The flash sale could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }



}
