<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 *
 * @method \App\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $customers = $this->paginate($this->Customers);

        $this->set(compact('customers'));
    }

    /**
     * View method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => ['Users', 'Products', 'Addresses', 'Orders']
        ]);

        $this->set('customer', $customer);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $customer = $this->Customers->newEntity();
        if ($this->request->is('post')) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData());
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $users = $this->Customers->Users->find('list', ['limit' => 200]);
        $products = $this->Customers->Products->find('list', ['limit' => 200]);
        $this->set(compact('customer', 'users', 'products'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $customer = $this->Customers->get($id, [
            'contain' => ['Products']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $customer = $this->Customers->patchEntity($customer, $this->request->getData());
            if ($this->Customers->save($customer)) {
                $this->Flash->success(__('The customer has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The customer could not be saved. Please, try again.'));
        }
        $users = $this->Customers->Users->find('list', ['limit' => 200]);
        $products = $this->Customers->Products->find('list', ['limit' => 200]);
        $this->set(compact('customer', 'users', 'products'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Customer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $customer = $this->Customers->get($id);
        if ($this->Customers->delete($customer)) {
            $this->Flash->success(__('The customer has been deleted.'));
        } else {
            $this->Flash->error(__('The customer could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function myaccount()
    {$this->loadModel('users');
//        $user_email = $this->Auth->users('username');
//        $user = $this->Users->get($user_email);
//        $userid = $user->id;
//
//        $customer_userid = $this->Customers->get(user_id);
//
//        $customer = $this->Users->find()->where([$userid => $customer_userid])
//            ->contain(['Customers'])->first();
//        $this->set('customer', $customer);


        $email=$this->Auth->user('username');
        $this->set('email',$this->Auth->user('username'));

        $customer_id= $this->Customers->find()
            ->select([
                'id',
            ])
            ->where(['email'=>$email]);
        $this->set('customer_id',$customer_id);

        $myaccount_cus = $this->Customers->find()
            ->select([
                'id'=>'users.id',
                'user_id'=>'Customers.user_id',
                'username' =>'users.username',
                'first_name' => 'Customers.first_name',
                'last_name' => 'Customers.last_name',
                'company' => 'Customers.company',
                'phone1'=>'Customers.phone1',
                'phone2'=>'Customers.phone2',

            ])
            ->join([
                'table' => 'users',
                'type' => 'INNER',
                'conditions' => ['Customers.user_id=users.id'],

            ])
            ->where([
                'Customers.email'=>$email
            ]);
        $this->set('myaccount_customers',$myaccount_cus);
    }
    public function wholesaleMyaccount()
    {$this->loadModel('users');
//        $user_email = $this->Auth->users('username');
//        $user = $this->Users->get($user_email);
//        $userid = $user->id;
//
//        $customer_userid = $this->Customers->get(user_id);
//
//        $customer = $this->Users->find()->where([$userid => $customer_userid])
//            ->contain(['Customers'])->first();
//        $this->set('customer', $customer);


        $email=$this->Auth->user('username');
        $this->set('email',$this->Auth->user('username'));

        $customer_id= $this->Customers->find()
            ->select([
                'id',
            ])
            ->where(['email'=>$email]);
        $this->set('customer_id',$customer_id);

        $myaccount_cus = $this->Customers->find()
            ->select([
                'id'=>'users.id',
                'user_id'=>'Customers.user_id',
                'username' =>'users.username',
                'first_name' => 'Customers.first_name',
                'last_name' => 'Customers.last_name',
                'company' => 'Customers.company',
                'phone1'=>'Customers.phone1',
                'phone2'=>'Customers.phone2',

            ])
            ->join([
                'table' => 'users',
                'type' => 'INNER',
                'conditions' => ['Customers.user_id=users.id'],

            ])
            ->where([
                'Customers.email'=>$email
            ]);
        $this->set('myaccount_customers',$myaccount_cus);
    }


    public function changepw()
    {
        $this->loadModel('Users');
        $email = $this->Auth->user('username');
        $this->set('email', $this->Auth->user('username'));

//        $changepw_cus = $this->Customers->find()
//            ->select([
//                'password' => 'users.password',
//            ])
//            ->join([
//                'table' => 'users',
//                'type' => 'INNER',
//                'conditions' => ['users.id=customers.user_id'],
//
//            ])
//            ->where([
//                'customers.email' => $email
//            ]);;
//        $this->set('changepw_cus', $changepw_cus);


        $changepw_cus = $this->Customers->find()->where(['email' => $email])
            ->contain(['Users'])->first();


        if ($this->request->is(['patch', 'post', 'put'])) {
            $passwordDetails = $this->request->getData();
            $value = $passwordDetails['old_password'];
            $hash = $changepw_cus->user->password;
//            debug(password_verify());
            $oldPasswordHash = (new DefaultPasswordHasher)->hash($passwordDetails['old_password']);
//            debug($oldPasswordHash);
//            exit;
            if ((new DefaultPasswordHasher)->check($value, $hash)) {
                if ($passwordDetails['New_password'] == $passwordDetails['confPassword']) {
                    $userWithNewPassword = $changepw_cus['user'];
                    $userWithNewPassword['password']= $passwordDetails['New_password'];
                    if ($this->Users->save($userWithNewPassword)) {
                        $this->Flash->success(__('Your password has been updated.'));
//                        return $this->redirect(['action' => 'myaccount']);
                    }
                } else {
                    $this->Flash->error('Confirm password does not match with new password, please try again.');
                }
            } else {
                $this->Flash->error('Old password is incorrect, please try again.');
            }
        }
        $this->set(compact('changepw_cus'));
    }

}
