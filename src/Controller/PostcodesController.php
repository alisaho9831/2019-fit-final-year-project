<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Postcodes Controller
 *
 * @property \App\Model\Table\PostcodesTable $Postcodes
 *
 * @method \App\Model\Entity\Postcode[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostcodesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $postcodes = $this->paginate($this->Postcodes);

        $this->set(compact('postcodes'));
    }

    /**
     * View method
     *
     * @param string|null $id Postcode id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $postcode = $this->Postcodes->get($id, [
            'contain' => []
        ]);

        $this->set('postcode', $postcode);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $postcode = $this->Postcodes->newEntity();
        if ($this->request->is('post')) {
            $postcode = $this->Postcodes->patchEntity($postcode, $this->request->getData());
            if ($this->Postcodes->save($postcode)) {
                $this->Flash->success(__('The postcode has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The postcode could not be saved. Please, try again.'));
        }
        $this->set(compact('postcode'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Postcode id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $postcode = $this->Postcodes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $postcode = $this->Postcodes->patchEntity($postcode, $this->request->getData());
            if ($this->Postcodes->save($postcode)) {
                $this->Flash->success(__('The postcode has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The postcode could not be saved. Please, try again.'));
        }
        $this->set(compact('postcode'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Postcode id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $postcode = $this->Postcodes->get($id);
        if ($this->Postcodes->delete($postcode)) {
            $this->Flash->success(__('The postcode has been deleted.'));
        } else {
            $this->Flash->error(__('The postcode could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
